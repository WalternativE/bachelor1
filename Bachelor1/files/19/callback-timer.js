/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("widget-position-constrain", function (e, t) {
    function m(e) {
    }

    var n = "constrain", r = "constrain|xyChange", i = "constrainChange", s = "preventOverlap", o = "align", u = "", a = "bindUI", f = "xy", l = "x", c = "y", h = e.Node, p = "viewportRegion", d = "region", v;
    m.ATTRS = {constrain: {value: null, setter: "_setConstrain"}, preventOverlap: {value: !1}}, v = m._PREVENT_OVERLAP = {x: {tltr: 1, blbr: 1, brbl: 1, trtl: 1}, y: {trbr: 1, tlbl: 1, bltl: 1, brtr: 1}}, m.prototype = {initializer: function () {
        this._posNode || e.error("WidgetPosition needs to be added to the Widget, before WidgetPositionConstrain is added"), e.after(this._bindUIPosConstrained, this, a)
    }, getConstrainedXY: function (e, t) {
        t = t || this.get(n);
        var r = this._getRegion(t === !0 ? null : t), i = this._posNode.get(d);
        return[this._constrain(e[0], l, i, r), this._constrain(e[1], c, i, r)]
    }, constrain: function (e, t) {
        var r, i, s = t || this.get(n);
        s && (r = e || this.get(f), i = this.getConstrainedXY(r, s), (i[0] !== r[0] || i[1] !== r[1]) && this.set(f, i, {constrained: !0}))
    }, _setConstrain: function (e) {
        return e === !0 ? e : h.one(e)
    }, _constrain: function (e, t, n, r) {
        if (r) {
            this.get(s) && (e = this._preventOverlap(e, t, n, r));
            var i = t == l, o = i ? r.width : r.height, u = i ? n.width : n.height, a = i ? r.left : r.top, f = i ? r.right - u : r.bottom - u;
            if (e < a || e > f)u < o ? e < a ? e = a : e > f && (e = f) : e = a
        }
        return e
    }, _preventOverlap: function (e, t, n, r) {
        var i = this.get(o), s = t === l, a, f, c, h, p, d;
        return i && i.points && v[t][i.points.join(u)] && (f = this._getRegion(i.node), f && (a = s ? n.width : n.height, c = s ? f.left : f.top, h = s ? f.right : f.bottom, p = s ? f.left - r.left : f.top - r.top, d = s ? r.right - f.right : r.bottom - f.bottom), e > c ? d < a && p > a && (e = c - a) : p < a && d > a && (e = h)), e
    }, _bindUIPosConstrained: function () {
        this.after(i, this._afterConstrainChange), this._enableConstraints(this.get(n))
    }, _afterConstrainChange: function (e) {
        this._enableConstraints(e.newVal)
    }, _enableConstraints: function (e) {
        e ? (this.constrain(), this._cxyHandle = this._cxyHandle || this.on(r, this._constrainOnXYChange)) : this._cxyHandle && (this._cxyHandle.detach(), this._cxyHandle = null)
    }, _constrainOnXYChange: function (e) {
        e.constrained || (e.newVal = this.getConstrainedXY(e.newVal))
    }, _getRegion: function (e) {
        var t;
        return e ? (e = h.one(e), e && (t = e.get(d))) : t = this._posNode.get(p), t
    }}, e.WidgetPositionConstrain = m
}, "3.14.1", {requires: ["widget-position"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("overlay",function(e,t){e.Overlay=e.Base.create("overlay",e.Widget,[e.WidgetStdMod,e.WidgetPosition,e.WidgetStack,e.WidgetPositionAlign,e.WidgetPositionConstrain])},"3.14.1",{requires:["widget","widget-stdmod","widget-position","widget-position-align","widget-stack","widget-position-constrain"],skinnable:!0});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("transition",function(e,t){var n="",r="",i=e.config.doc,s="documentElement",o=i[s].style,u="transition",a="transitionProperty",f,l,c,h,p,d,v={},m=["Webkit","Moz"],g={Webkit:"webkitTransitionEnd"},y=function(){this.init.apply(this,arguments)};y._TRANSFORM="transform",y._toCamel=function(e){return e=e.replace(/-([a-z])/gi,function(e,t){return t.toUpperCase()}),e},y._toHyphen=function(e){return e=e.replace(/([A-Z]?)([a-z]+)([A-Z]?)/g,function(e,t,n,r){var i=(t?"-"+t.toLowerCase():"")+n;return r&&(i+="-"+r.toLowerCase()),i}),e},y.SHOW_TRANSITION="fadeIn",y.HIDE_TRANSITION="fadeOut",y.useNative=!1,"transition"in o&&"transitionProperty"in o&&"transitionDuration"in o&&"transitionTimingFunction"in o&&"transitionDelay"in o?(y.useNative=!0,y.supported=!0):e.Array.each(m,function(e){var t=e+"Transition";t in i[s].style&&(n=e,r=y._toHyphen(e)+"-",y.useNative=!0,y.supported=!0,y._VENDOR_PREFIX=e)}),typeof o.transform=="undefined"&&e.Array.each(m,function(e){var t=e+"Transform";typeof o[t]!="undefined"&&(y._TRANSFORM=t)}),n&&(u=n+"Transition",a=n+"TransitionProperty"),f=r+"transition-property",l=r+"transition-duration",c=r+"transition-timing-function",h=r+"transition-delay",p="transitionend",d="on"+n.toLowerCase()+"transitionend",p=g[n]||p,y.fx={},y.toggles={},y._hasEnd={},y._reKeywords=/^(?:node|duration|iterations|easing|delay|on|onstart|onend)$/i,e.Node.DOM_EVENTS[p]=1,y.NAME="transition",y.DEFAULT_EASING="ease",y.DEFAULT_DURATION=.5,y.DEFAULT_DELAY=0,y._nodeAttrs={},y.prototype={constructor:y,init:function(e,t){var n=this;return n._node=e,!n._running&&t&&(n._config=t,e._transition=n,n._duration="duration"in t?t.duration:n.constructor.DEFAULT_DURATION,n._delay="delay"in t?t.delay:n.constructor.DEFAULT_DELAY,n._easing=t.easing||n.constructor.DEFAULT_EASING,n._count=0,n._running=!1),n},addProperty:function(t,n){var r=this,i=this._node,s=e.stamp(i),o=e.one(i),u=y._nodeAttrs[s],a,f,l,c,h;u||(u=y._nodeAttrs[s]={}),c=u[t],n&&n.value!==undefined?h=n.value:n!==undefined&&(h=n,n=v),typeof h=="function"&&(h=h.call(o,o)),c&&c.transition&&c.transition!==r&&c.transition._count--,r._count++,l=(typeof n.duration!="undefined"?n.duration:r._duration)||1e-4,u[t]={value:h,duration:l,delay:typeof n.delay!="undefined"?n.delay:r._delay,easing:n.easing||r._easing,transition:r},a=e.DOM.getComputedStyle(i,t),f=typeof h=="string"?a:parseFloat(a),y.useNative&&f===h&&setTimeout(function(){r._onNativeEnd.call(i,{propertyName:t,elapsedTime:l})},l*1e3)},removeProperty:function(t){var n=this,r=y._nodeAttrs[e.stamp(n._node)];r&&r[t]&&(delete r[t],n._count--)},initAttrs:function(t){var n,r=this._node;t.transform&&!t[y._TRANSFORM]&&(t[y._TRANSFORM]=t.transform,delete t.transform);for(n in t)t.hasOwnProperty(n)&&!y._reKeywords.test(n)&&(this.addProperty(n,t[n]),r.style[n]===""&&e.DOM.setStyle(r,n,e.DOM.getComputedStyle(r,n)))},run:function(t){var n=this,r=n._node,i=n._config,s={type:"transition:start",config:i};return n._running||(n._running=!0,i.on&&i.on.start&&i.on.start.call(e.one(r),s),n.initAttrs(n._config),n._callback=t,n._start()),n},_start:function(){this._runNative()},_prepDur:function(e){return e=parseFloat(e)*1e3,e+"ms"},_runNative:function(){var t=this,n=t._node,r=e.stamp(n),i=n.style,s=n.ownerDocument.defaultView.getComputedStyle(n),o=y._nodeAttrs[r],u="",a=s[y._toCamel(f)],d=f+": ",v=l+": ",m=c+": ",g=h+": ",b,w,E;a!=="all"&&(d+=a+",",v+=s[y._toCamel(l)]+",",m+=s[y._toCamel(c)]+",",g+=s[y._toCamel(h)]+",");for(E in o)b=y._toHyphen(E),w=o[E],(w=o[E])&&w.transition===t&&(E in n.style?(v+=t._prepDur(w.duration)+",",g+=t._prepDur(w.delay)+",",m+=w.easing+",",d+=b+",",u+=b+": "+w.value+"; "):this.removeProperty(E));d=d.replace(/,$/,";"),v=v.replace(/,$/,";"),m=m.replace(/,$/,";"),g=g.replace(/,$/,";"),y._hasEnd[r]||(n.addEventListener(p,t._onNativeEnd,""),y._hasEnd[r]=!0),i.cssText+=d+v+m+g+u},_end:function(t){var n=this,r=n._node,i=n._callback,s=n._config,o={type:"transition:end",config:s,elapsedTime:t},u=e.one(r);n._running=!1,n._callback=null,r&&(s.on&&s.on.end?setTimeout(function(){s.on.end.call(u,o),i&&i.call(u,o)},1):i&&setTimeout(function(){i.call(u,o)},1))},_endNative:function(e){var t=this._node,n=t.ownerDocument.defaultView.getComputedStyle(t,"")[y._toCamel(f)];e=y._toHyphen(e),typeof n=="string"&&(n=n.replace(new RegExp("(?:^|,\\s)"+e+",?"),","),n=n.replace(/^,|,$/,""),t.style[u]=n)},_onNativeEnd:function(t){var n=this,r=e.stamp(n),i=t,s=y._toCamel(i.propertyName),o=i.elapsedTime,u=y._nodeAttrs[r],f=u[s],l=f?f.transition:null,c,h;l&&(l.removeProperty(s),l._endNative(s),h=l._config[s],c={type:"propertyEnd",propertyName:s,elapsedTime:o,config:h},h&&h.on&&h.on.end&&h.on.end.call(e.one(n),c),l._count<=0&&(l._end(o),n.style[a]=""))},destroy:function(){var e=this,t=e._node;t&&(t.removeEventListener(p,e._onNativeEnd,!1),e._node=null)}},e.Transition=y,e.TransitionNative=y,e.Node.prototype.transition=function(t,n,r){var i=y._nodeAttrs[e.stamp(this._node)],s=i?i.transition||null:null,o,u;if(typeof t=="string"){typeof n=="function"&&(r=n,n=null),o=y.fx[t];if(n&&typeof n=="object"){n=e.clone(n);for(u in o)o.hasOwnProperty(u)&&(u in n||(n[u]=o[u]))}else n=o}else r=n,n=t;return s&&!s._running?s.init(this,n):s=new y(this._node,n),s.run(r),this},e.Node.prototype.show=function(t,n,r){return this._show(),t&&e.Transition&&(typeof t!="string"&&!t.push&&(typeof n=="function"&&(r=n,n=t),t=y.SHOW_TRANSITION),this.transition(t,n,r)),this},e.NodeList.prototype.show=function(t,n,r){var i=this._nodes,s=0,o;while(o=i[s++])e.one(o).show(t,n,r);return this};var b=function(e,t,n){return function(){t&&t.call(e),n&&typeof n=="function"&&n.apply(e._node,arguments)}};e.Node.prototype.hide=function(t,n,r){return t&&e.Transition?(typeof n=="function"&&(r=n,n=null),r=b(this,this._hide,r),typeof t!="string"&&!t.push&&(typeof n=="function"&&(r=n,n=t),t=y.HIDE_TRANSITION),this.transition(t,n,r)):this._hide(),this},e.NodeList.prototype.hide=function(t,n,r){var i=this._nodes,s=0,o;while(o=i[s++])e.one(o).hide(t,n,r);return this},e.NodeList.prototype
.transition=function(t,n,r){var i=this._nodes,s=this.size(),o=0,r=r===!0,u;while(u=i[o++])o<s&&r?e.one(u).transition(t):e.one(u).transition(t,n);return this},e.Node.prototype.toggleView=function(t,n,r){this._toggles=this._toggles||[],r=arguments[arguments.length-1];if(typeof t!="string"){n=t,this._toggleView(n,r);return}return typeof n=="function"&&(n=undefined),typeof n=="undefined"&&t in this._toggles&&(n=!this._toggles[t]),n=n?1:0,n?this._show():r=b(this,this._hide,r),this._toggles[t]=n,this.transition(e.Transition.toggles[t][n],r),this},e.NodeList.prototype.toggleView=function(t,n,r){var i=this._nodes,s=0,o;while(o=i[s++])o=e.one(o),o.toggleView.apply(o,arguments);return this},e.mix(y.fx,{fadeOut:{opacity:0,duration:.5,easing:"ease-out"},fadeIn:{opacity:1,duration:.5,easing:"ease-in"},sizeOut:{height:0,width:0,duration:.75,easing:"ease-out"},sizeIn:{height:function(e){return e.get("scrollHeight")+"px"},width:function(e){return e.get("scrollWidth")+"px"},duration:.5,easing:"ease-in",on:{start:function(){var e=this.getStyle("overflow");e!=="hidden"&&(this.setStyle("overflow","hidden"),this._transitionOverflow=e)},end:function(){this._transitionOverflow&&(this.setStyle("overflow",this._transitionOverflow),delete this._transitionOverflow)}}}}),e.mix(y.toggles,{size:["sizeOut","sizeIn"],fade:["fadeOut","fadeIn"]})},"3.14.1",{requires:["node-style"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-base",function(e,t){var n="running",r="startTime",i="elapsedTime",s="start",o="tween",u="end",a="node",f="paused",l="reverse",c="iterationCount",h=Number,p={},d;e.Anim=function(){e.Anim.superclass.constructor.apply(this,arguments),e.Anim._instances[e.stamp(this)]=this},e.Anim.NAME="anim",e.Anim._instances={},e.Anim.RE_DEFAULT_UNIT=/^width|height|top|right|bottom|left|margin.*|padding.*|border.*$/i,e.Anim.DEFAULT_UNIT="px",e.Anim.DEFAULT_EASING=function(e,t,n,r){return n*e/r+t},e.Anim._intervalTime=20,e.Anim.behaviors={left:{get:function(e,t){return e._getOffset(t)}}},e.Anim.behaviors.top=e.Anim.behaviors.left,e.Anim.DEFAULT_SETTER=function(t,n,r,i,s,o,u,a){var f=t._node,l=f._node,c=u(s,h(r),h(i)-h(r),o);l?"style"in l&&(n in l.style||n in e.DOM.CUSTOM_STYLES)?(a=a||"",f.setStyle(n,c+a)):"attributes"in l&&n in l.attributes?f.setAttribute(n,c):n in l&&(l[n]=c):f.set?f.set(n,c):n in f&&(f[n]=c)},e.Anim.DEFAULT_GETTER=function(t,n){var r=t._node,i=r._node,s="";return i?"style"in i&&(n in i.style||n in e.DOM.CUSTOM_STYLES)?s=r.getComputedStyle(n):"attributes"in i&&n in i.attributes?s=r.getAttribute(n):n in i&&(s=i[n]):r.get?s=r.get(n):n in r&&(s=r[n]),s},e.Anim.ATTRS={node:{setter:function(t){return t&&(typeof t=="string"||t.nodeType)&&(t=e.one(t)),this._node=t,!t,t}},duration:{value:1},easing:{value:e.Anim.DEFAULT_EASING,setter:function(t){if(typeof t=="string"&&e.Easing)return e.Easing[t]}},from:{},to:{},startTime:{value:0,readOnly:!0},elapsedTime:{value:0,readOnly:!0},running:{getter:function(){return!!p[e.stamp(this)]},value:!1,readOnly:!0},iterations:{value:1},iterationCount:{value:0,readOnly:!0},direction:{value:"normal"},paused:{readOnly:!0,value:!1},reverse:{value:!1}},e.Anim.run=function(){var t=e.Anim._instances,n;for(n in t)t[n].run&&t[n].run()},e.Anim.pause=function(){for(var t in p)p[t].pause&&p[t].pause();e.Anim._stopTimer()},e.Anim.stop=function(){for(var t in p)p[t].stop&&p[t].stop();e.Anim._stopTimer()},e.Anim._startTimer=function(){d||(d=setInterval(e.Anim._runFrame,e.Anim._intervalTime))},e.Anim._stopTimer=function(){clearInterval(d),d=0},e.Anim._runFrame=function(){var t=!0,n;for(n in p)p[n]._runFrame&&(t=!1,p[n]._runFrame());t&&e.Anim._stopTimer()},e.Anim.RE_UNITS=/^(-?\d*\.?\d*){1}(em|ex|px|in|cm|mm|pt|pc|%)*$/;var v={run:function(){return this.get(f)?this._resume():this.get(n)||this._start(),this},pause:function(){return this.get(n)&&this._pause(),this},stop:function(e){return(this.get(n)||this.get(f))&&this._end(e),this},_added:!1,_start:function(){this._set(r,new Date-this.get(i)),this._actualFrames=0,this.get(f)||this._initAnimAttr(),p[e.stamp(this)]=this,e.Anim._startTimer(),this.fire(s)},_pause:function(){this._set(r,null),this._set(f,!0),delete p[e.stamp(this)],this.fire("pause")},_resume:function(){this._set(f,!1),p[e.stamp(this)]=this,this._set(r,new Date-this.get(i)),e.Anim._startTimer(),this.fire("resume")},_end:function(t){var n=this.get("duration")*1e3;t&&this._runAttrs(n,n,this.get(l)),this._set(r,null),this._set(i,0),this._set(f,!1),delete p[e.stamp(this)],this.fire(u,{elapsed:this.get(i)})},_runFrame:function(){var e=this._runtimeAttr.duration,t=new Date-this.get(r),n=this.get(l),s=t>=e;this._runAttrs(t,e,n),this._actualFrames+=1,this._set(i,t),this.fire(o),s&&this._lastFrame()},_runAttrs:function(t,n,r){var i=this._runtimeAttr,s=e.Anim.behaviors,o=i.easing,u=n,a=!1,f,l,c;t>=n&&(a=!0),r&&(t=n-t,u=0);for(c in i)i[c].to&&(f=i[c],l=c in s&&"set"in s[c]?s[c].set:e.Anim.DEFAULT_SETTER,a?l(this,c,f.from,f.to,u,n,o,f.unit):l(this,c,f.from,f.to,t,n,o,f.unit))},_lastFrame:function(){var e=this.get("iterations"),t=this.get(c);t+=1,e==="infinite"||t<e?(this.get("direction")==="alternate"&&this.set(l,!this.get(l)),this.fire("iteration")):(t=0,this._end()),this._set(r,new Date),this._set(c,t)},_initAnimAttr:function(){var t=this.get("from")||{},n=this.get("to")||{},r={duration:this.get("duration")*1e3,easing:this.get("easing")},i=e.Anim.behaviors,s=this.get(a),o,u,f;e.each(n,function(n,a){typeof n=="function"&&(n=n.call(this,s)),u=t[a],u===undefined?u=a in i&&"get"in i[a]?i[a].get(this,a):e.Anim.DEFAULT_GETTER(this,a):typeof u=="function"&&(u=u.call(this,s));var l=e.Anim.RE_UNITS.exec(u),c=e.Anim.RE_UNITS.exec(n);u=l?l[1]:u,f=c?c[1]:n,o=c?c[2]:l?l[2]:"",!o&&e.Anim.RE_DEFAULT_UNIT.test(a)&&(o=e.Anim.DEFAULT_UNIT);if(!u||!f){e.error('invalid "from" or "to" for "'+a+'"',"Anim");return}r[a]={from:e.Lang.isObject(u)?e.clone(u):u,to:f,unit:o}},this),this._runtimeAttr=r},_getOffset:function(e){var t=this._node,n=t.getComputedStyle(e),r=e==="left"?"getX":"getY",i=e==="left"?"setX":"setY",s;return n==="auto"&&(s=t.getStyle("position"),s==="absolute"||s==="fixed"?(n=t[r](),t[i](n)):n=0),n},destructor:function(){delete e.Anim._instances[e.stamp(this)]}};e.extend(e.Anim,e.Base,v)},"3.14.1",{requires:["base-base","node-style"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("widget-anim", function (e, t) {
    function b(e) {
        b.superclass.constructor.apply(this, arguments)
    }

    var n = "boundingBox", r = "host", i = "node", s = "opacity", o = "", u = "visible", a = "destroy", f = "hidden", l = "rendered", c = "start", h = "end", p = "duration", d = "animShow", v = "animHide", m = "_uiSetVisible", g = "animShowChange", y = "animHideChange";
    b.NS = "anim", b.NAME = "pluginWidgetAnim", b.ANIMATIONS = {fadeIn: function () {
        var t = this.get(r), f = t.get(n), l = new e.Anim({node: f, to: {opacity: 1}, duration: this.get(p)});
        return t.get(u) || f.setStyle(s, 0), l.on(a, function () {
            this.get(i).setStyle(s, e.UA.ie ? 1 : o)
        }), l
    }, fadeOut: function () {
        return new e.Anim({node: this.get(r).get(n), to: {opacity: 0}, duration: this.get(p)})
    }}, b.ATTRS = {duration: {value: .2}, animShow: {valueFn: b.ANIMATIONS.fadeIn}, animHide: {valueFn: b.ANIMATIONS.fadeOut}}, e.extend(b, e.Plugin.Base, {initializer: function (e) {
        this._bindAnimShow(), this._bindAnimHide(), this.after(g, this._bindAnimShow), this.after(y, this._bindAnimHide), this.beforeHostMethod(m, this._uiAnimSetVisible)
    }, destructor: function () {
        this.get(d).destroy(), this.get(v).destroy()
    }, _uiAnimSetVisible: function (t) {
        if (this.get(r).get(l))return t ? (this.get(v).stop(), this.get(d).run()) : (this.get(d).stop(), this.get(v).run()), new e.Do.Prevent
    }, _uiSetVisible: function (e) {
        var t = this.get(r), i = t.getClassName(f);
        t.get(n).toggleClass(i, !e)
    }, _bindAnimShow: function () {
        this.get(d).on(c, e.bind(function () {
            this._uiSetVisible(!0)
        }, this))
    }, _bindAnimHide: function () {
        this.get(v).after(h, e.bind(function () {
            this._uiSetVisible(!1)
        }, this))
    }}), e.namespace("Plugin").WidgetAnim = b
}, "3.14.1", {requires: ["anim-base", "plugin", "widget"]});

YUI.add("gallery-outside-events", function (b) {
    var a = ["blur", "change", "click", "dblclick", "focus", "keydown", "keypress", "keyup", "mousedown", "mousemove", "mouseout", "mouseover", "mouseup", "select", "submit"];
    b.Event.defineOutside = function (d, c) {
        c = c || d + "outside";
        b.Event.define(c, {on: function (g, e, f) {
            e.onHandle = b.one("doc").on(d, function (h) {
                if (this.isOutside(g, h.target)) {
                    f.fire(h)
                }
            }, this)
        }, detach: function (g, e, f) {
            e.onHandle.detach()
        }, delegate: function (h, f, g, e) {
            f.delegateHandle = b.one("doc").delegate(d, function (i) {
                if (this.isOutside(h, i.target)) {
                    g.fire(i)
                }
            }, e, this)
        }, detachDelegate: function (h, f, g, e) {
            f.delegateHandle.detach()
        }, isOutside: function (e, f) {
            return f !== e && !f.ancestor(function (g) {
                return g === e
            })
        }})
    };
    b.each(a, function (c) {
        b.Event.defineOutside(c)
    })
}, "1.1.0", {requires: ["event-focus", "event-synthetic"]});

YUI.add("gallery-overlay-extras", function (d) {
    /*!
     * Overlay Extras
     *
     * Oddnut Software
     * Copyright (c) 2009-2011 Eric Ferraiuolo - http://oddnut.com
     * YUI BSD License - http://developer.yahoo.com/yui/license.html
     */
    var j = "overlay", r = "host", p = "renderUI", g = "bindUI", m = "syncUI", b = "rendered", t = "boundingBox", q = "visible", f = "zIndex", i = "align", n = "Change", k = d.Lang.isBoolean, s = d.ClassNameManager.getClassName, c = d.one("doc"), e = (function () {
        /*! IS_POSITION_FIXED_SUPPORTED - Juriy Zaytsev (kangax) - http://yura.thinkweb2.com/cft/ */
        var v = null, w, u;
        if (document.createElement) {
            w = document.createElement("div");
            if (w && w.style) {
                w.style.position = "fixed";
                w.style.top = "10px";
                u = document.body;
                if (u && u.appendChild && u.removeChild) {
                    u.appendChild(w);
                    v = (w.offsetTop === 10);
                    u.removeChild(w)
                }
            }
        }
        return v
    }()), a, l, o, h;
    (function () {
        var x = "overlayModal", w = "modal", v = "mask", u = {modal: s(j, w), mask: s(j, v)};
        a = d.Base.create(x, d.Plugin.Base, [], {_maskNode: null, _uiHandles: null, initializer: function (y) {
            this.afterHostMethod(p, this.renderUI);
            this.afterHostMethod(g, this.bindUI);
            this.afterHostMethod(m, this.syncUI);
            if (this.get(r).get(b)) {
                this.renderUI();
                this.bindUI();
                this.syncUI()
            }
        }, destructor: function () {
            if (this._maskNode) {
                this._maskNode.remove(true)
            }
            this._detachUIHandles();
            this.get(r).get(t).removeClass(u.modal)
        }, renderUI: function () {
            var z = this.get(r).get(t), y = d.one("body");
            this._maskNode = d.Node.create("<div></div>");
            this._maskNode.addClass(u.mask);
            this._maskNode.setStyles({position: e ? "fixed" : "absolute", width: "100%", height: "100%", top: "0", left: "0", display: "none"});
            y.insert(this._maskNode, y.get("firstChild"));
            z.addClass(u.modal)
        }, bindUI: function () {
            this.afterHostEvent(q + n, this._afterHostVisibleChange);
            this.afterHostEvent(f + n, this._afterHostZIndexChange)
        }, syncUI: function () {
            var y = this.get(r);
            this._uiSetHostVisible(y.get(q));
            this._uiSetHostZIndex(y.get(f))
        }, _focus: function () {
            var z = this.get(r), A = z.get(t), y = A.get("tabIndex");
            A.set("tabIndex", y >= 0 ? y : 0);
            z.focus();
            A.set("tabIndex", y)
        }, _blur: function () {
            this.get(r).blur()
        }, _getMaskNode: function () {
            return this._maskNode
        }, _uiSetHostVisible: function (y) {
            if (y) {
                d.later(1, this, "_attachUIHandles");
                this._maskNode.setStyle("display", "block");
                this._focus()
            } else {
                this._detachUIHandles();
                this._maskNode.setStyle("display", "none");
                this._blur()
            }
        }, _uiSetHostZIndex: function (y) {
            this._maskNode.setStyle(f, y || 0)
        }, _attachUIHandles: function (z) {
            if (this._uiHandles) {
                return
            }
            var y = this.get(r), A = y.get(t);
            this._uiHandles = [A.on("clickoutside", d.bind(this._focus, this)), A.on("focusoutside", d.bind(this._focus, this))];
            if (!e) {
                this._uiHandles.push(d.one("win").on("scroll", d.bind(function (C) {
                    var B = this._maskNode;
                    B.setStyle("top", B.get("docScrollY"))
                }, this)))
            }
        }, _detachUIHandles: function () {
            d.each(this._uiHandles, function (y) {
                y.detach()
            });
            this._uiHandles = null
        }, _afterHostVisibleChange: function (y) {
            this._uiSetHostVisible(y.newVal)
        }, _afterHostZIndexChange: function (y) {
            this._uiSetHostZIndex(y.newVal)
        }}, {NS: w, ATTRS: {maskNode: {getter: "_getMaskNode", readOnly: true}}, CLASSES: u})
    }());
    (function () {
        var v = "overlayKeepaligned", u = "keepaligned";
        l = d.Base.create(v, d.Plugin.Base, [], {_uiHandles: null, initializer: function (w) {
            this.afterHostMethod(g, this.bindUI);
            this.afterHostMethod(m, this.syncUI);
            if (this.get(r).get(b)) {
                this.bindUI();
                this.syncUI()
            }
        }, destructor: function () {
            this._detachUIHandles()
        }, bindUI: function () {
            this.afterHostEvent(q + n, this._afterHostVisibleChange)
        }, syncUI: function () {
            this._uiSetHostVisible(this.get(r).get(q))
        }, syncAlign: function () {
            this.get(r)._syncUIPosAlign()
        }, _uiSetHostVisible: function (w) {
            if (w) {
                this._attachUIHandles()
            } else {
                this._detachUIHandles()
            }
        }, _attachUIHandles: function () {
            if (this._uiHandles) {
                return
            }
            var w = d.bind(this.syncAlign, this);
            this._uiHandles = [d.on("windowresize", w), d.on("scroll", w)]
        }, _detachUIHandles: function () {
            d.each(this._uiHandles, function (w) {
                w.detach()
            });
            this._uiHandles = null
        }, _afterHostVisibleChange: function (w) {
            this._uiSetHostVisible(w.newVal)
        }}, {NS: u})
    }());
    (function () {
        var x = "overlayAutohide", v = "autohide", w = "clickedOutside", y = "focusedOutside", u = "pressedEscape";
        o = d.Base.create(x, d.Plugin.Base, [], {_uiHandles: null, initializer: function (z) {
            this.afterHostMethod(g, this.bindUI);
            this.afterHostMethod(m, this.syncUI);
            if (this.get(r).get(b)) {
                this.bindUI();
                this.syncUI()
            }
        }, destructor: function () {
            this._detachUIHandles()
        }, bindUI: function () {
            this.afterHostEvent(q + n, this._afterHostVisibleChange)
        }, syncUI: function () {
            this._uiSetHostVisible(this.get(r).get(q))
        }, _uiSetHostVisible: function (z) {
            if (z) {
                d.later(1, this, "_attachUIHandles")
            } else {
                this._detachUIHandles()
            }
        }, _attachUIHandles: function () {
            if (this._uiHandles) {
                return
            }
            var B = this.get(r), C = B.get(t), A = d.bind(B.hide, B), z = [];
            if (this.get(w)) {
                z.push(C.on("clickoutside", A))
            }
            if (this.get(y)) {
                z.push(C.on("focusoutside", A))
            }
            if (this.get(u)) {
                z.push(c.on("key", A, "esc"))
            }
            this._uiHandles = z
        }, _detachUIHandles: function () {
            d.each(this._uiHandles, function (z) {
                z.detach()
            });
            this._uiHandles = null
        }, _afterHostVisibleChange: function (z) {
            this._uiSetHostVisible(z.newVal)
        }}, {NS: v, ATTRS: {clickedOutside: {value: true, validator: k}, focusedOutside: {value: true, validator: k}, pressedEscape: {value: true, validator: k}}})
    }());
    (function () {
        var x = "overlayPointer", v = "pointer", w = "pointing", u = {pointer: s(j, v), pointing: s(j, w)};
        h = d.Base.create(x, d.Plugin.Base, [], {_pointerNode: null, initializer: function (y) {
            this.afterHostMethod(p, this.renderUI);
            this.afterHostMethod(g, this.bindUI);
            this.afterHostMethod(m, this.syncUI);
            if (this.get(r).get(b)) {
                this.renderUI();
                this.bindUI();
                this.syncUI()
            }
        }, destructor: function () {
            var z = this.get(r), A = z.get(t), B = z.get(i), y = this._pointerNode;
            A.removeClass(u.pointing);
            if (B && B.points) {
                A.removeClass(s(j, w, B.points[0]))
            }
            if (y) {
                y.remove(true)
            }
        }, renderUI: function () {
            this._pointerNode = d.Node.create("<span></span>").addClass(u.pointer);
            this.get(r).get(t).append(this._pointerNode)
        }, bindUI: function () {
            this.afterHostEvent(i + n, this._afterHostAlignChange)
        }, syncUI: function () {
            this._uiSetHostAlign(this.get(r).get(i))
        }, _getPointerNode: function () {
            return this._pointerNode
        }, _uiSetHostAlign: function (z, y) {
            var B = this.get(r), C = B.get(t), A = this._pointerNode;
            if (y && y.points) {
                C.removeClass(s(j, w, y.points[0]));
                C.removeClass(s(j, w, y.points[0], y.points[1]))
            }
            if (z && z.node && z.points[0] !== d.WidgetPositionAlign.CC) {
                C.addClass(u.pointing);
                C.addClass(s(j, w, z.points[0]));
                C.addClass(s(j, w, z.points[0], z.points[1]));
                A.show()
            } else {
                A.hide();
                C.removeClass(u.pointing)
            }
            B._syncUIPosAlign()
        }, _afterHostAlignChange: function (y) {
            this._uiSetHostAlign(y.newVal, y.prevVal)
        }}, {NS: v, ATTRS: {pointerNode: {getter: "_getPointerNode", readOnly: true}}, CLASSES: u})
    }());
    d.Plugin.OverlayModal = a;
    d.Plugin.OverlayKeepaligned = l;
    d.Plugin.OverlayAutohide = o;
    d.Plugin.OverlayPointer = h
}, "gallery-2011.05.04-20-03", {requires: ["base", "widget-anim", "gallery-outside-events"]});

YUI.add("rg-tooltip",function(a){a.namespace("rg.widget");var b=a.Base.create("alignable",a.Widget,[a.WidgetPosition,a.WidgetPositionAlign]),c=a.Base.create("TooltipPlugin",a.Plugin.Base,[],{initializer:function(){if("mouseenter"!==this.get("type")||!a.UA.touchEnabled){var c=this.get("host"),d=new b({render:!1,visible:!0,zIndex:100}),e=new b({render:!1,visible:!1,zIndex:100});this.getAttrsFromData(),c.ancestor(".y-popup-container")&&e.get("boundingBox").addClass("dialog-tooltip"),e.get("boundingBox").addClass("yui3-tooltip"),e.get("contentBox").addClass("tt-container"),e.get("contentBox").addClass("tt-container-"+this.get("align")),e.get("boundingBox").addClass(this.get("additionalClasses")),d.get("contentBox").addClass("tt-arrow"),d.get("contentBox").addClass("tt-arrow-"+this.get("align")),this.innerContainer=a.Node.create("<div></div>"),this.innerContainer.addClass("tooltip-inner"),e.get("contentBox").append(this.innerContainer),this.get("width")&&(this.innerContainer.setStyle("width",this.get("width")),this.innerContainer.setStyle("maxWidth","none")),this.eventHandlers=[],"mouseenter"===this.get("type")?this.eventHandlers.push(c.on("mouseover",this.show,this),c.on("mouseout",this._hide,this)):"click"===this.get("type")?(this.get("closeOnOutsideClick")&&e.plug(a.Plugin.OverlayAutohide),this.eventHandlers.push(c.on("click",this._onClick,this))):"focus"===this.get("type")?this.eventHandlers.push(c.on("focus",this.hide,this)):"togglefocus"===this.get("type")&&this.eventHandlers.push(c.on("focus",this.show,this),c.on("blur",this.hide,this)),this.eventHandlers.push(e.get("boundingBox").delegate("click",this.hide,this.get("tooltipCloseSelector"),this)),this.get("persistWhenMouseover")&&(e.on("hover",function(){this._hover||(this._hover=!0,this.show())},this),e.on("mouseleave",function(){this._hover&&(this._hover=!1,this._hide())},this));var f=this.get("overlayOffset"),g=this.get("arrowOffset");(f[0]||f[1])&&e.on("xyChange",function(a){a.newVal[0]=a.newVal[0]+f[0],a.newVal[1]=a.newVal[1]+f[1]}),(g[0]||g[1])&&d.on("xyChange",function(a){a.newVal[0]=a.newVal[0]+g[0],a.newVal[1]=a.newVal[1]+g[1]}),this.set("overlay",e),this.set("arrow",d)}},destructor:function(){this.get("overlay")&&(this.hide(),a.Array.each(this.eventHandlers,function(a){a.detach()}),this.get("overlay").destroy(!0))},getAttrsFromData:function(){var b=this.get("host"),c=["align","renderTarget","width"];a.Array.each(c,function(a){var c=b.getData("tt-"+a);c&&this.set(a,c)},this)},_onClick:function(){this.get("overlay").get("visible")?this.hide():this.show()},show:function(){this._showTooltip=!0;var b=this.get("delay"),c=this;b>0?this._timer||(this._timer=a.later(b,this,function(){c._showTooltip&&c._show(),c._timer=null})):this._show()},_show:function(){var b=this,c=a.Lang,d=this.get("host"),e=this.get("contentFnCtx"),f=this.get("contentFn"),g=this.get("overlay"),h=this.get("arrow"),i=!1;g.get("rendered")||(g.render(this.get("renderTarget")),h.render(g.get("contentBox")),i=!0,this.eventHandlers.push(this.after("contentChange",this.show,this),a.before("windowresize",function(){g.get("visible")&&(b.reposition(),g.show())}))),d.hasClass("tooltip-disabled")||((this.get("updateContentOnShow")||i)&&(c.isUndefined(e)||c.isUndefined(f)?c.isObject(this.get("content"))&&this.get("content").NAME&&"node"==this.get("content").NAME?(this.innerContainer.empty(),this.innerContainer.append(this.get("content"))):this.innerContainer.setContent(this.get("content")):this.innerContainer.setContent(f.call(e,this))),this.reposition(),g.show())},reposition:function(){var a=this.get("overlay");a.get("boundingBox").removeClass("tt-align-top"),a.get("boundingBox").removeClass("tt-align-bottom"),a.get("boundingBox").removeClass("tt-align-right"),a.get("boundingBox").removeClass("tt-align-left"),this.get("position").call(this)},_hide:function(b){var c=this.get("delay"),d=this;this._showTooltip=!1,c>0?a.later(200,this,function(){d._showTooltip||d.hide(b),d._timer=null}):this.hide(b)},hide:function(b){var c=this.get("host").generateID(),d=null;b&&b.relatedTarget&&b.relatedTarget.ancestor&&(d=b.relatedTarget.ancestor("#"+c,!0)),b&&null!==d||this.get("persistWhenMouseover")&&this._hover||(this._showTooltip=!1,this._timer&&this._timer.cancel&&a.Lang.isFunction(this._timer.cancel)&&(this._timer.cancel(),this._timer=null),this.get("overlay").hide())}},{NS:"tooltip",ATTRS:{updateContentOnShow:{value:!0},renderTarget:{value:"body"},align:{value:"top"},additionalClasses:{value:""},tooltipCloseSelector:{value:".js-tooltip-close"},width:{value:null},content:{value:""},type:{value:"mouseenter"},overlay:{value:null},arrow:{value:null},contentFnCtx:{validator:function(b){return a.Lang.isObject(b)}},contentFn:{validator:function(b){return a.Lang.isFunction(b)}},closeOnOutsideClick:{value:!0},arrowOffset:{value:[0,0]},overlayOffset:{value:[0,0]},delay:{value:0},persistWhenMouseover:{value:!1},position:{value:function(){var a=this.get("host"),b=this.get("overlay"),c=this.get("arrow");switch(this.get("align")){case"bottom":b.get("boundingBox").addClass("tt-align-bottom"),b.align(a,["tc","bc"]),c.align(a,["tc","bc"]);break;case"left":b.get("boundingBox").addClass("tt-align-left"),b.align(a,["rc","lc"]),c.align(a,["rc","lc"]);break;case"right":b.get("boundingBox").addClass("tt-align-right"),b.align(a,["lc","rc"]),c.align(a,["lc","rc"]);break;default:b.get("boundingBox").addClass("tt-align-top"),b.align(a,["bc","tc"]),c.align(a,["bc","tc"])}}}}});a.rg.widget.Alignable=b,a.rg.widget.TooltipPlugin=c},"0.0.1",{requires:["overlay","transition","gallery-overlay-extras"]});
YUI.add("rg.core.form.ErrorPopoverPlugin",function(a){a.namespace("rg.core.form"),a.rg.core.form.ErrorPopoverPlugin=a.Base.create("ErrorPopoverPlugin",a.rg.widget.TooltipPlugin,[],{initializer:function(){var b=this.get("host"),c=this.get("overlay"),d=c.get("boundingBox");a.Lang.isUndefined(b.tooltip)||b.tooltip.get("overlay").get("boundingBox").addClass("popover-hidden"),d.addClass("error-popover"),this.get("fieldName")&&d.addClass("error-popover-"+this.get("fieldName"))},destructor:function(){var b=this.get("host");if(!a.Lang.isUndefined(b.tooltip)){var c=b.tooltip.get("overlay");c.get("boundingBox").removeClass("popover-hidden"),this.get("overlay").destroy(!0)}}},{NS:"errorPopover",ATTRS:{align:{value:"right"},type:{value:"manual"},fieldName:{}}})},"0.0.1",{requires:["rg-tooltip"]});
/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-color",function(e,t){var n=Number;e.Anim.getUpdatedColorValue=function(t,r,i,s,o){return t=e.Color.re_RGB.exec(e.Color.toRGB(t)),r=e.Color.re_RGB.exec(e.Color.toRGB(r)),(!t||t.length<3||!r||r.length<3)&&e.error("invalid from or to passed to color behavior"),"rgb("+[Math.floor(o(i,n(t[1]),n(r[1])-n(t[1]),s)),Math.floor(o(i,n(t[2]),n(r[2])-n(t[2]),s)),Math.floor(o(i,n(t[3]),n(r[3])-n(t[3]),s))].join(", ")+")"},e.Anim.behaviors.color={set:function(t,n,r,i,s,o,u){t._node.setStyle(n,e.Anim.getUpdatedColorValue(r,i,s,o,u))},get:function(e,t){var n=e._node.getComputedStyle(t);return n=n==="transparent"?"rgb(255, 255, 255)":n,n}},e.each(["backgroundColor","borderColor","borderTopColor","borderRightColor","borderBottomColor","borderLeftColor"],function(t){e.Anim.behaviors[t]=e.Anim.behaviors.color})},"3.14.1",{requires:["anim-base"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-xy",function(e,t){var n=Number;e.Anim.behaviors.xy={set:function(e,t,r,i,s,o,u){e._node.setXY([u(s,n(r[0]),n(i[0])-n(r[0]),o),u(s,n(r[1]),n(i[1])-n(r[1]),o)])},get:function(e){return e._node.getXY()}}},"3.14.1",{requires:["anim-base","node-screen"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-curve",function(e,t){e.Anim.behaviors.curve={set:function(t,n,r,i,s,o,u){r=r.slice.call(r),i=i.slice.call(i);var a=u(s,0,100,o)/100;i.unshift(r),t._node.setXY(e.Anim.getBezier(i,a))},get:function(e){return e._node.getXY()}},e.Anim.getBezier=function(e,t){var n=e.length,r=[],i,s;for(i=0;i<n;++i)r[i]=[e[i][0],e[i][1]];for(s=1;s<n;++s)for(i=0;i<n-s;++i)r[i][0]=(1-t)*r[i][0]+t*r[parseInt(i+1,10)][0],r[i][1]=(1-t)*r[i][1]+t*r[parseInt(i+1,10)][1];return[r[0][0],r[0][1]]}},"3.14.1",{requires:["anim-xy"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-easing",function(e,t){var n={easeNone:function(e,t,n,r){return n*e/r+t},easeIn:function(e,t,n,r){return n*(e/=r)*e+t},easeOut:function(e,t,n,r){return-n*(e/=r)*(e-2)+t},easeBoth:function(e,t,n,r){return(e/=r/2)<1?n/2*e*e+t:-n/2*(--e*(e-2)-1)+t},easeInStrong:function(e,t,n,r){return n*(e/=r)*e*e*e+t},easeOutStrong:function(e,t,n,r){return-n*((e=e/r-1)*e*e*e-1)+t},easeBothStrong:function(e,t,n,r){return(e/=r/2)<1?n/2*e*e*e*e+t:-n/2*((e-=2)*e*e*e-2)+t},elasticIn:function(e,t,n,r,i,s){var o;return e===0?t:(e/=r)===1?t+n:(s||(s=r*.3),!i||i<Math.abs(n)?(i=n,o=s/4):o=s/(2*Math.PI)*Math.asin(n/i),-(i*Math.pow(2,10*(e-=1))*Math.sin((e*r-o)*2*Math.PI/s))+t)},elasticOut:function(e,t,n,r,i,s){var o;return e===0?t:(e/=r)===1?t+n:(s||(s=r*.3),!i||i<Math.abs(n)?(i=n,o=s/4):o=s/(2*Math.PI)*Math.asin(n/i),i*Math.pow(2,-10*e)*Math.sin((e*r-o)*2*Math.PI/s)+n+t)},elasticBoth:function(e,t,n,r,i,s){var o;return e===0?t:(e/=r/2)===2?t+n:(s||(s=r*.3*1.5),!i||i<Math.abs(n)?(i=n,o=s/4):o=s/(2*Math.PI)*Math.asin(n/i),e<1?-0.5*i*Math.pow(2,10*(e-=1))*Math.sin((e*r-o)*2*Math.PI/s)+t:i*Math.pow(2,-10*(e-=1))*Math.sin((e*r-o)*2*Math.PI/s)*.5+n+t)},backIn:function(e,t,n,r,i){return i===undefined&&(i=1.70158),e===r&&(e-=.001),n*(e/=r)*e*((i+1)*e-i)+t},backOut:function(e,t,n,r,i){return typeof i=="undefined"&&(i=1.70158),n*((e=e/r-1)*e*((i+1)*e+i)+1)+t},backBoth:function(e,t,n,r,i){return typeof i=="undefined"&&(i=1.70158),(e/=r/2)<1?n/2*e*e*(((i*=1.525)+1)*e-i)+t:n/2*((e-=2)*e*(((i*=1.525)+1)*e+i)+2)+t},bounceIn:function(t,n,r,i){return r-e.Easing.bounceOut(i-t,0,r,i)+n},bounceOut:function(e,t,n,r){return(e/=r)<1/2.75?n*7.5625*e*e+t:e<2/2.75?n*(7.5625*(e-=1.5/2.75)*e+.75)+t:e<2.5/2.75?n*(7.5625*(e-=2.25/2.75)*e+.9375)+t:n*(7.5625*(e-=2.625/2.75)*e+.984375)+t},bounceBoth:function(t,n,r,i){return t<i/2?e.Easing.bounceIn(t*2,0,r,i)*.5+n:e.Easing.bounceOut(t*2-i,0,r,i)*.5+r*.5+n}};e.Easing=n},"3.14.1",{requires:["anim-base"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("anim-node-plugin", function (e, t) {
    var n = function (t) {
        t = t ? e.merge(t) : {}, t.node = t.host, n.superclass.constructor.apply(this, arguments)
    };
    n.NAME = "nodefx", n.NS = "fx", e.extend(n, e.Anim), e.namespace("Plugin"), e.Plugin.NodeFX = n
}, "3.14.1", {requires: ["node-pluginhost", "anim-base"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("anim-scroll",function(e,t){var n=Number;e.Anim.behaviors.scroll={set:function(e,t,r,i,s,o,u){var a=e._node,f=[u(s,n(r[0]),n(i[0])-n(r[0]),o),u(s,n(r[1]),n(i[1])-n(r[1]),o)];f[0]&&a.set("scrollLeft",f[0]),f[1]&&a.set("scrollTop",f[1])},get:function(e){var t=e._node;return[t.get("scrollLeft"),t.get("scrollTop")]}}},"3.14.1",{requires:["anim-base"]});

YUI.add("rg-anim",function(a){a.namespace("rg"),a.rg.highlight=function(b,c,d){var e=a.one(b),f=a.merge({duration:1.5},d);if(0===a.UA.ie||a.UA.ie>8)e.setStyle("backgroundColor","#fffed2"),e.transition({easing:"ease-out",duration:f.duration,backgroundColor:"rgba(0,0,0,0)"},function(){e.setStyle("backgroundColor",""),c&&c()});else{var g=new a.Anim({node:e,from:{backgroundColor:"#fffed2"},to:{backgroundColor:"#ffffff"},duration:f.duration});g.set("easing",a.Easing.easeOut),g.on("end",function(){e.setStyle("backgroundColor",""),c&&c()}),g.run()}},a.rg.fadeToggle=function(b,c,d){var e,f,g=a.one(b);return g?(e="none"!=g.getStyle("display"),f=a.merge({duration:.2},d),void(e?g.transition({easing:"linear",duration:f.duration,opacity:"0"},function(){g.hide(),g.setStyle("opacity",""),c&&c()}):(g.setStyle("opacity","0"),g.show(),g.transition({easing:"linear",duration:f.duration,opacity:"1"},function(){g.setStyle("opacity",""),c&&c()})))):!1},a.rg.slideOpen=function(b,c,d){var e=a.one(b),f="none"!=e.getStyle("display"),g=null,h=d&&"duration"in d?d.duration:.3,i=d&&"dimension"in d?d.dimension:"height",j=new a.Anim({node:e,duration:h});if(!f){e.setStyle(i,""),e.setStyle("overflow","hidden"),e.setStyle("visibility","hidden"),e.setStyle("display","block");var k=e.getStyle("marginTop"),l=e.getStyle("marginBottom"),m=e.getStyle("marginLeft"),n=e.getStyle("marginRight"),o=e.getStyle("paddingTop"),p=e.getStyle("paddingBottom"),q=e.getStyle("paddingLeft"),r=e.getStyle("paddingRight");return"height"!=i?(e.setStyle("paddingLeft","0"),e.setStyle("paddingRight","0"),e.setStyle("marginLeft","0"),e.setStyle("marginRight","0"),g=e.get("offsetWidth"),j.set("to",{width:g,marginLeft:m,marginRight:n,paddingLeft:q,paddingRight:r})):(e.setStyle("paddingTop","0"),e.setStyle("paddingBottom","0"),e.setStyle("marginTop","0"),e.setStyle("marginBottom","0"),g=e.get("offsetHeight"),j.set("to",{height:g,marginTop:k,marginBottom:l,paddingTop:o,paddingBottom:p})),e.setStyle("display","none"),e.setStyle("visibility",""),e.setStyle(i,"1px"),j.on("start",function(){e.show(),e.setStyle("display","block")}),j.on("end",function(){e.removeAttribute("style"),c&&c(!0)}),j.run(),j}},a.rg.slideClose=function(b,c,d){var e=a.one(b);if(e){var f="none"!=e.getStyle("display"),g=d&&"duration"in d?d.duration:.2,h=d&&"dimension"in d?d.dimension:"height",i=new a.Anim({node:e,duration:g});if(f)return"height"!=h?i.set("to",{width:"1px",paddingLeft:"0",paddingRight:"0",marginLeft:"0",marginRight:"0"}):i.set("to",{height:"1px",paddingTop:"0",paddingBottom:"0",marginTop:"0",marginBottom:"0"}),i.on("start",function(){e.setStyle("overflow","hidden")}),i.on("end",function(){e.removeAttribute("style"),e.setStyle("display","none"),c&&c(!1)}),i.run(),i}},a.rg.slideToggle=function(b,c,d,e){var f=a.one(b);if(!f)return void(c&&c());var g="none"!=f.getStyle("display");return e||(e={}),e.dimension=d?"width":"height",g?a.rg.slideClose(b,c,e):a.rg.slideOpen(b,c,e)},a.rg.scrollTo=function(b,c,d){b=a.one(b);var e=new a.Anim({node:b,duration:d,to:{scroll:[0,c]},easing:a.Easing.easeInOut});e.run(),e.destroy()}},"0.0.1",{requires:["anim","transition"]});
YUI.add("rg-validation",function(a){a.namespace("rg.validation");var b=/(?:[a-z0-9!#$%&'*+/=?^_`{|}~-]+(?:\.[a-z0-9!#$%&'*+/=?^_`{|}~-]+)*|"(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21\x23-\x5b\x5d-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])*")@(?:(?:[a-z0-9](?:[a-z0-9-]*[a-z0-9])?\.)+[a-z0-9](?:[a-z0-9-]*[a-z0-9])?|\[(?:(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?)\.){3}(?:25[0-5]|2[0-4][0-9]|[01]?[0-9][0-9]?|[a-z0-9-]*[a-z0-9]:(?:[\x01-\x08\x0b\x0c\x0e-\x1f\x21-\x5a\x53-\x7f]|\\[\x01-\x09\x0b\x0c\x0e-\x7f])+)\])/;a.rg.validation.isValidMail=function(a){var c=b.test(a);return c?!0:(a=a.toLocaleLowerCase?a.toLocaleLowerCase():a.toLowerCase(),b.test(a))},a.rg.validation.sanitizeFormFieldName=function(a){return a.replace(/([\[\]])/g,"\\$1")}},"0.0.1");
YUI.add("rg.WidgetView",function(a){a.namespace("rg");var b,c=a.Lang.isNull,d=a.Lang.isUndefined,e=a.config.debug,f=a.rg.validation.sanitizeFormFieldName;if(e)var g={},h=5;var i=function(c){var d,e,f;if(b(c))return c.id;if(a.Lang.isArray(c)){d=[];for(f in c)c.hasOwnProperty(f)&&(e=i(c[f]),null!==e&&d.push(e));return d}if(a.Lang.isObject(c)){d={};for(f in c)c.hasOwnProperty(f)&&(e=i(c[f]),null!==e&&(d[f]=e));return d}return null},j=function(a){for(var b=a.get("offsetTop"),c=a.get("offsetLeft"),d=a.get("offsetWidth"),e=a.get("offsetHeight");a.get("offsetParent");)a=a.get("offsetParent"),b+=a.get("offsetTop"),c+=a.get("offsetLeft");return b>=window.pageYOffset&&c>=window.pageXOffset&&b+e<=window.pageYOffset+window.innerHeight&&c+d<=window.pageXOffset+window.innerWidth},k=a.Base.create("DestroyWidget",a.Plugin.Base,[],{initializer:function(a){this.widget=a.widget},destructor:function(){this.widget.destroy()}},{NS:"DestroyWidget"});a.rg.WidgetView=a.Base.create("WidgetView",a.Base,[],{initializer:function(b){this.id=b.id||"",this.data=b.data||{},this.partials=b.partials,this.viewClass=b.viewClass||null,this.widgetUrl=b.widgetUrl||null,this.template=b.template||null,this.compiledTemplate=b.compiledTemplate||null,this.templateExtensions=b.templateExtensions||[],this.templateName=b.templateName||null,this.yuiModules=b.yuiModules||[],this.events=a.merge(b.events||{},this.events),this._attachedViewEvents=[],this._childWidgets={},this.pageLayout=b.pageLayout,this.pageTitle=b.pageTitle,this.data.widgetId||(this.data.widgetId=this.id),this.on("rendered",function(){var b=this.get("container");if(b){if(this.afterRendered&&a.Lang.isFunction(this.afterRendered))if(a.config.debug)this.afterRendered();else try{this.afterRendered()}catch(c){a.error("Error calling afterRendered of widget "+this.id,c)}}else a.log("widget: "+this.id+" is not properly implemented. widgetId missing?","warn")},this)},success:!0,errors:[],addChild:function(b,c){if(b&&c){var d=c.data.widgetId;this._childWidgets[d]=c;var e=b,f=this.getChildIds();if(b.indexOf(".")>=0){var g=b.split(".");for(e=g.splice(g.length-1,1);g.length>0;){if(!f)return;f=f[g.shift()]}}f&&(f[e]&&a.Lang.isArray(f[e])?f[e].push(d):f[e]=d)}},getChildIds:function(){return this.childIds||(this.childIds=i(this.data)),this.childIds},getChild:function(b){var c;if(b.indexOf(".")>=0){for(var d=b.split("."),e=this.getChildIds();d.length>0;){if(!e)return;e=e[d.shift()]}c=e}else c=this.getChildIds()[b];if(c){if(a.Lang.isArray(c)){var f=[],g=a.clone(c,!0);return a.Array.each(g,function(a){var b=this.getChildById(a);b&&f.push(b)},this),f}return this.getChildById(c)}},getChildById:function(a){var b=this._childWidgets[a];if(b&&b.get("container"))return this._childWidgets[a];var c=this.get("container").one("#"+a);if(!c)return void this.removeChildById(a);var d=c.getData("widget");return this._childWidgets[a]=d,d},removeChildById:function(){var b=function(c,d){for(var e in c)if(c.hasOwnProperty(e)){var f=c[e];f===d?a.Lang.isArray(c)?c.splice(e,1):delete c[e]:a.Lang.isObject(f)&&b(f,d)}};return function(a){delete this._childWidgets[a],b(this.getChildIds(),a)}}(),setContainer:function(a){a||this.get("container")&&this.get("container").purge(!0),this.set("container",a),a&&(a.plug(k,{widget:this}),a.setData("widget",this),a.addClass("js-widgetContainer"),this.attachEvents(this.events))},destructor:function(){var a=this.get("container");this.detachEvents(),a&&(a.empty(),a.purge(),a.remove()),this.set("container",null)},validateResponse:function(b){var c=this.get("container"),d=c.all(".control-group, .js-error-clear");if(d.each(function(b){b.unplug(a.rg.core.form.ErrorPopoverPlugin)}),c.all(".error").removeClass("error"),!b.success){var e=b.errors;a.Object.each(e,function(b,d){if(0===d)a.rg.notify(b,"warning");else{d=f(d);var e=c.one(".error-highlight-"+d);e&&e.addClass("error");var g=c.one(".error-"+d);if(g){var h=this.get("errorPopoverConfig"),i={content:a.Lang.isString(b)?b:b[0],fieldName:d};h&&(i=a.merge(h,i)),g.plug(a.rg.core.form.ErrorPopoverPlugin,i),g.errorPopover.show()}}},this);var g=c.all(".error").shift();if(g&&!j(g)){var h=parseInt(g.getY())-60;h=0>h?0:h,a.rg.scrollTo(a.one("document"),h,.2)}return!1}return!0},validateField:function(b,c){var d=this.get("container"),e=f(b.get("name")),g=d.one(".error-highlight-"+e),h=d.one(".error-"+e);if(g&&g.removeClass("error"),h&&h.unplug(a.rg.core.form.ErrorPopoverPlugin),!c.success){if(g&&g.addClass("error"),h){var i=this.get("errorPopoverConfig"),j={content:c.errors[b.get("name")][0],fieldName:e};i&&(j=a.merge(i,j)),h.plug(a.rg.core.form.ErrorPopoverPlugin,j),h.errorPopover.show()}return!1}return!0},updateData:function(b){this.data=a.merge(this.data,b)},attachEvents:function(b){var f,i,j,k,l=this.get("container");this.detachEvents(),b=b||this.events,a.rg.supportsPushState()&&this._attachedViewEvents.push(l.delegate("click",a.rg.widget.handleAjaxPageLoad,"a.ajax-page-load",this));for(k in b)if(b.hasOwnProperty(k)){i=b[k];for(j in i)if(i.hasOwnProperty(j))if(f=i[j],"string"==typeof f&&(f=this[f]),(d(f)||c(f))&&(a.log("Event handler not found for ["+j+"] on ["+k+"]","warn"),f=function(){}),"global"===k)this._attachedViewEvents.push(a.on(j,f,this));else if("submit"===k)l.one(j)&&this._attachedViewEvents.push(l.one(j).on(k,f,this));else{if(e&&"js-"!=k.substring(1,4)){var m=k+this.name;g[m]={selector:k,widget:this.name,logged:!1}}"click"==j?!function(a,b,c,d,e){e._attachedViewEvents.push(a.delegate(b,function(a){var b=a.currentTarget;return b.hasClass("btn-inactive")||b.hasClass("btn-disabled")||b.getAttribute("disabled")?void a.preventDefault():void c.call(e,a)},d,e))}(l,j,f,k,this):"change"==j&&a.UA.ie>0&&a.UA.ie<9?(this.get("container").all(k).each(function(a){this._attachedViewEvents.push(a.on("click",function(){this.blur()}))},this),this.get("container").all(k).each(function(a){this._attachedViewEvents.push(a.on(j,f,this))},this)):this._attachedViewEvents.push("submit"==j?l.one(k).on(j,f,this):l.delegate(j,f,k,this))}}if(e)for(var n in g)if(g.hasOwnProperty(n)){var o=g[n];o.logged===!1&&h>0&&(a.log('Event handler selector "'+o.selector+'" on '+o.widget+' should begin with ".js-"',"warn"),o.logged=!0,h--)}},detachEvents:function(){a.Array.each(this._attachedViewEvents,function(a){a.detach()}),this._attachedViewEvents=[]},reload:function(b,c,d){var e=this.widgetUrl,f=this.get("container");a.rg.loadWidget(e,b,function(a){try{a.render({replace:f,before:c,after:d})}catch(b){f.remove(!0)}})},getDefaultDialogConfig:function(){return{}},render:function(b){a.rg.widget.prepareWidgetTree(this,a.bind(function(c){this.data=c.data,a.rg.widget.renderWidgetTree(this,b)},this))}},{NAME:"view",ATTRS:{container:{value:null}}}),a.rg.WidgetView.getInstance=function(b){var c,d=b.viewClass;a.Lang.isString(d)||(d="WidgetView"),c=a.Object.getValue(a.rg,d.split(".")),a.Lang.isObject(c)||(c=a.rg.WidgetView);var e=new c(b);return e.setAttrs(b.attrs),e},a.rg.WidgetView.isRenderableWidget=b=function(b){return a.Lang.isObject(b)?!(!b.id||!b.templateName):!1}},"1.0.0",{requires:["rg.core.form.ErrorPopoverPlugin","rg-anim","rg-validation"]});
YUI.add("rg-utils-dom",function(a){a.namespace("rg.utils.dom"),a.rg.utils.dom.canAcceptFocus=function(b){return b=a.one(b),"hidden"==b.getStyle("visibility")||"none"==b.getStyle("display")||b.get("disabled")?!1:!0},a.rg.utils.dom.disableButton=function(b){b=a.one(b),b.setAttribute("disabled","disabled"),b.addClass("btn-disabled")},a.rg.utils.dom.enableButton=function(b){b=a.one(b),b.removeClass("btn-disabled"),b.removeAttribute("disabled")}},"0.0.1",{requires:[]});
YUI.add("rg-overlay",function(a){a.namespace("rg.widget");var b,c,d,e="host",f="renderUI",g="contentBox";!function(){var c="popup",d="y-popup",h="overlayPopup",i="boundingBox",j="y-popup-modal",k=a.UA.ie>5&&a.UA.ie<9,l=function(){var a,b,c;a=this.get(e),c=a.get(g),c.addClass(d),b=a.get(i),b.addClass(j),a.get("visible")&&o.call(this)},m=function(a){a.newVal&&this.get(e)._syncUIPosAlign()},n=function(){var b=this.get(e),c=a.one("win"),d=c.get("winHeight");k&&b.get(i).setStyle("height",d+"px")},o=function(){var b=a.one("body");if(k&&(b=a.one("html")),this.get("host").get("visible")){a.fire("rg.popup:opened"),b.addClass("has-overlay"),this.get(e).get(i).setStyle("top","0"),b.set("scrollTop",0),a.one("#page-container").addClass("popup-fixed");var c=this.get("bodyScrollTop");a.one(".popup-fixed").setStyle("top",-c),this.get("handleResize")&&(this.resizeHandler=a.on("windowresize",n,this),this.contentUpdateHandler=this.get("host").after("contentUpdate",n,this),p.call(this),n.call(this))}else a.fire("rg.popup:closed"),b.removeClass("has-overlay"),a.one("#page-container").removeClass("popup-fixed"),a.one("#page-container").removeAttribute("style"),a.one("win").set("scrollTop",this.get("bodyScrollTop")),this.get("handleResize")&&(this.resizeHandler.detach(),this.contentUpdateHandler.detach(),this.get("timer").cancel());this.get("hideOnClickOutside")?this.get(e).get(i).on("click",r,this):this.get(e).get(i).detach("click",r,this)},p=function(){this.set("timer",a.later(1e3,this,function(){n.call(this)},[],!0))},q=function(){var b=this.get(e),c=a.Node.create('<div class="y-popup-container"></div>');b.get(i).appendChild(c),c.appendChild(b.get(g))},r=function(a){var b=this.get(e);a.target.contains(b.get(g))&&b.hide()};b=a.Base.create(h,a.Plugin.Base,[],{initializer:function(){this.beforeHostMethod(f,q,this),this.afterHostMethod(f,l,this),this.after("hideOnClickOutsideChange",function(){this.resizeHandler&&this.resizeHandler.detach(),o.call(this)},this),this.after("handleResizeChange",function(){this.get(e).get(i).detach("click",r,this),o.call(this)},this),this.get(e).get(i).delegate("click",function(){this.get(e).hide()},".js-close",this),this.doBefore("visibleChange",function(){this.get("host").get("visible")||this.set("bodyScrollTop",a.one("win").get("scrollTop"))},this),this._visibleChangeBeforeHandle=this.doBefore("visibleChange",m,this),this._visibleChangeAfterHandle=this.get("host").after("visibleChange",o,this)},destructor:function(){this._visibleChangeAfterHandle.detach(),this._visibleChangeBeforeHandle.detach()}},{NS:c,ATTRS:{timer:{value:null},handleResize:{value:!0},hideOnClickOutside:{value:!0},bodyScrollTop:{value:null}}})}(),function(){var b="closeable",d="overlayCloseable",h='<div class="close"><a href="javascript:;" onclick="return false;"></a></div>',i=function(){var b,c,d,f;b=this.get(e),c=b.get(g),c.setStyle("position","relative"),d=a.Node.create(h),f=d.one("a"),f.setContent('<span class="ico-x-close-light"></span>'),c.append(d),f.on("click",b.hide,b)};c=a.Base.create(d,a.Plugin.Base,[],{initializer:function(){this.afterHostMethod(f,i,this)},destructor:function(){}},{NS:b})}(),function(){d=a.Base.create("Overlay",a.Overlay,[],{initializer:function(d){d.plugins=a.mix([a.Plugin.OverlayModal,{fn:a.Plugin.OverlayAutohide,cfg:{focusedOutside:!1,clickedOutside:!1,pressedEscape:!0}},{fn:b,cfg:{handleResize:d.handleResize,hideOnClickOutside:d.hideOnClickOutside}},c],d.plugins),this.set("zIndex",d.zIndex&&"auto"!==d.zIndex?d.zIndex:1001),this.on("bodyContentChange",function(){this.get("autoFocusInput")&&a.Lang.later(200,this,function(){var b=this.getStdModNode(a.WidgetStdMod.BODY);if(b){var c=b.all("input[type=text]");if(!c.isEmpty()){var d=c._nodes[0];d&&a.rg.utils.dom.canAcceptFocus(d)&&d.focus()}}})})},scaleToViewport:function(b,c){var d=a.one("body").get("winHeight"),e=this.getStdModNode(a.WidgetStdMod.BODY),f=e.get("offsetHeight"),g=e.getY(),h=a.one(".yui3-overlay-modal"),i=b.get("offsetHeight"),j=f-i,k=150,l=50,m=d-(g+l+j),n=Math.max(m,k);b.setStyle("overflowY","auto"),c===!0&&n>b.get("offsetHeight")&&b.setStyle("height",n),b.setStyle("maxHeight",n);var o=n+g+l+j;return h.setStyle("overflow","hidden"),h.setStyle("height",o-5),n},configure:function(a){"undefined"!=typeof a.hideOnClickOutside&&this.hasPlugin("popup").set("hideOnClickOutside",a.hideOnClickOutside),"undefined"!=typeof a.handleResize&&this.hasPlugin("popup").set("handleResize",a.handleResize),this.render(),this.set("zIndex",a.zIndex&&"auto"!==a.zIndex?a.zIndex:1001)}},{ATTRS:{autoFocusInput:{value:!0}}})}(),a.rg.widget.OverlayPopup=b,a.rg.widget.OverlayCloseable=c,a.rg.widget.Dialog=d},"0.0.1",{requires:["overlay","gallery-overlay-extras","rg-utils-dom"]});
YUI.add("gallery-overlay-transition", function (b) {
    var a = b.Lang;
    b.Plugin.TransitionOverlay = b.Base.create("overlayTransitionPlugin", b.Plugin.Base, [], {_showing: false, _styleCache: {}, initializer: function (c) {
        this.doBefore("_uiSetVisible", this._uiAnimSetVisible);
        this._host = this.get("host");
        this._bb = this._host.get("boundingBox");
        this.publish("start", {preventable: false});
        this.publish("end", {preventable: false});
        if (this.get("styleOverride")) {
            this._host.once("visibleChange", function (d) {
                if (d.newVal && !d.prevVal) {
                    this._applyDefaultStyle()
                }
            }, this)
        }
    }, destructor: function () {
        this._host = this._bb = null
    }, _applyDefaultStyle: function () {
        var c = this.get("hide"), d = this._bb;
        b.each(c, b.bind(function (e, f) {
            this._styleCache[f] = d.getComputedStyle(f)
        }, this));
        if (!this._host.get("visible")) {
            d.setStyles(c)
        }
    }, _uiAnimSetVisible: function (e) {
        var c = this._host, d;
        if (c.get("rendered")) {
            this._showing = e;
            this.fire("start", e);
            if (e) {
                this._uiSetVisible(true)
            }
            this._bb.transition((e) ? this.get("show") : this.get("hide"), b.bind(function () {
                this.fire("end", e);
                if (!e) {
                    this._uiSetVisible(false)
                }
            }, this));
            return new b.Do.Prevent("AnimPlugin prevented default show/hide")
        }
    }, _uiSetVisible: function (c) {
        this._bb.toggleClass(this._host.getClassName("hidden"), !c)
    }}, {NS: "transitionPlugin", ATTRS: {duration: {value: 0.25}, styleOverride: {value: true, validator: a.isBoolean}, hide: {value: {opacity: 0}, setter: function (c) {
        return b.merge({duration: this.get("duration")}, c)
    }}, show: {value: {opacity: 1}, setter: function (c) {
        return b.merge({duration: this.get("duration")}, c)
    }}}})
}, "@VERSION@", {requires: ["base-build", "plugin", "event-custom", "transition"]});

YUI.add("rg-callback-timer",function(a){a.namespace("rg");var b=function(){a.Lang.isNull(this._timer)||this._timer.cancel()},c=function(){this._timer=a.Lang.later(this.get("delay"),this,d)},d=function(){var b=this.get("host"),c=this.get("callback");c.call(b),b.unplug(a.rg.CallbackTimer)};a.rg.CallbackTimer=a.Base.create("CallbackTimer",a.Plugin.Base,[],{_timer:null,initializer:function(){c.call(this)},destructor:function(){b.call(this)}},{NS:"callbackTimer",ATTRS:{delay:{value:5e3},callback:{validator:function(b){return a.Lang.isFunction(b)}}}})},"0.0.1",{requires:[]});
