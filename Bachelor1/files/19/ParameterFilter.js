/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("cookie",function(e,t){function h(e){throw new TypeError(e)}function p(e){(!s(e)||e==="")&&h("Cookie name must be a non-empty string.")}function d(e){(!s(e)||e==="")&&h("Subcookie name must be a non-empty string.")}var n=e.Lang,r=e.Object,i=null,s=n.isString,o=n.isObject,u=n.isUndefined,a=n.isFunction,f=encodeURIComponent,l=decodeURIComponent,c=e.config.doc;e.Cookie={_createCookieString:function(e,t,n,r){r=r||{};var i=f(e)+"="+(n?f(t):t),u=r.expires,a=r.path,l=r.domain;return o(r)&&(u instanceof Date&&(i+="; expires="+u.toUTCString()),s(a)&&a!==""&&(i+="; path="+a),s(l)&&l!==""&&(i+="; domain="+l),r.secure===!0&&(i+="; secure")),i},_createCookieHashString:function(e){o(e)||h("Cookie._createCookieHashString(): Argument must be an object.");var t=[];return r.each(e,function(e,n){!a(e)&&!u(e)&&t.push(f(n)+"="+f(String(e)))}),t.join("&")},_parseCookieHash:function(e){var t=e.split("&"),n=i,r={};if(e.length)for(var s=0,o=t.length;s<o;s++)n=t[s].split("="),r[l(n[0])]=l(n[1]);return r},_parseCookieString:function(e,t,n){var r={};if(s(e)&&e.length>0){var o=t===!1?function(e){return e}:l,a=e.split(/;\s/g),f=i,c=i,h=i;for(var p=0,d=a.length;p<d;p++){h=a[p].match(/([^=]+)=/i);if(h instanceof Array)try{f=l(h[1]),c=o(a[p].substring(h[1].length+1))}catch(v){}else f=l(a[p]),c="";!u(n)&&n.reverseCookieLoading?u(r[f])&&(r[f]=c):r[f]=c}}return r},_setDoc:function(e){c=e},exists:function(e){p(e);var t=this._parseCookieString(c.cookie,!0);return t.hasOwnProperty(e)},get:function(e,t){p(e);var n,r,s;return a(t)?(s=t,t={}):o(t)?s=t.converter:t={},n=this._parseCookieString(c.cookie,!t.raw,t),r=n[e],u(r)?i:a(s)?s(r):r},getSub:function(e,t,n,r){var s=this.getSubs(e,r);return s!==i?(d(t),u(s[t])?i:a(n)?n(s[t]):s[t]):i},getSubs:function(e,t){p(e);var n=this._parseCookieString(c.cookie,!1,t);return s(n[e])?this._parseCookieHash(n[e]):i},remove:function(t,n){return p(t),n=e.merge(n||{},{expires:new Date(0)}),this.set(t,"",n)},removeSub:function(e,t,n){p(e),d(t),n=n||{};var r=this.getSubs(e);if(o(r)&&r.hasOwnProperty(t)){delete r[t];if(!n.removeIfEmpty)return this.setSubs(e,r,n);for(var i in r)if(r.hasOwnProperty(i)&&!a(r[i])&&!u(r[i]))return this.setSubs(e,r,n);return this.remove(e,n)}return""},set:function(e,t,n){p(e),u(t)&&h("Cookie.set(): Value cannot be undefined."),n=n||{};var r=this._createCookieString(e,t,!n.raw,n);return c.cookie=r,r},setSub:function(e,t,n,r){p(e),d(t),u(n)&&h("Cookie.setSub(): Subcookie value cannot be undefined.");var i=this.getSubs(e);return o(i)||(i={}),i[t]=n,this.setSubs(e,i,r)},setSubs:function(e,t,n){p(e),o(t)||h("Cookie.setSubs(): Cookie value must be an object.");var r=this._createCookieString(e,this._createCookieHashString(t),!1,n);return c.cookie=r,r}}},"3.14.1",{requires:["yui-base"]});

YUI.add("rg-uri",function(a){a.namespace("rg");var b=function(){"use strict";function a(a){return null!==a&&""!==a}function b(a){a=a||"";var b=/^(?:(?![^:@]+:[^:@\/]*@)([^:\/?#.]+):)?(?:\/\/)?((?:(([^:@]*)(?::([^:@]*))?)?@)?([^:\/?#]*)(?::(\d*))?)(((\/(?:[^?#](?![^?#\/]*\.[^?#\/.]+(?:[?#]|$)))*\/?)?([^?#\/]*))(?:\?([^#]*))?(?:#(.*))?)/,d=["source","protocol","authority","userInfo","user","password","host","port","relative","path","directory","file","query","anchor"],e={name:"queryKey",parser:/(?:^|&)([^&=]*)=?([^&]*)/g},f=b.exec(a),g=14,h=this;for(this.uriParts={};g--;)this.uriParts[d[g]]=f[g]||"";this.uriParts[e.name]={},this.uriParts[d[12]].replace(e.parser,function(a,b,c){b&&(h.uriParts[e.name][b]=c)}),this.queryObj=new c(this.uriParts.query),this.hasAuthorityPrefixUserPref=null}return b.prototype.protocol=function(a){return"undefined"!=typeof a&&(this.uriParts.protocol=a),this.uriParts.protocol},b.prototype.hasAuthorityPrefix=function(a){return"undefined"!=typeof a&&(this.hasAuthorityPrefixUserPref=a),null===this.hasAuthorityPrefixUserPref?-1!==this.uriParts.source.indexOf("//"):this.hasAuthorityPrefixUserPref},b.prototype.userInfo=function(a){return"undefined"!=typeof a&&(this.uriParts.userInfo=a),this.uriParts.userInfo},b.prototype.host=function(a){return"undefined"!=typeof a&&(this.uriParts.host=a),this.uriParts.host},b.prototype.port=function(a){return"undefined"!=typeof a&&(this.uriParts.port=a),this.uriParts.port},b.prototype.path=function(a){return"undefined"!=typeof a&&(this.uriParts.path=a),this.uriParts.path},b.prototype.query=function(a){return"undefined"!=typeof a&&(this.queryObj=new c(a)),this.queryObj},b.prototype.anchor=function(a){return"undefined"!=typeof a&&(this.uriParts.anchor=a),this.uriParts.anchor},b.prototype.setProtocol=function(a){return this.protocol(a),this},b.prototype.setHasAuthorityPrefix=function(a){return this.hasAuthorityPrefix(a),this},b.prototype.setUserInfo=function(a){return this.userInfo(a),this},b.prototype.setHost=function(a){return this.host(a),this},b.prototype.setPort=function(a){return this.port(a),this},b.prototype.setPath=function(a){return this.path(a),this},b.prototype.setQuery=function(a){return this.query(a),this},b.prototype.setAnchor=function(a){return this.anchor(a),this},b.prototype.getQueryParamValue=function(a){return this.query().getParamValue(a)},b.prototype.getQueryParamValues=function(a){return this.query().getParamValues(a)},b.prototype.deleteQueryParam=function(a,b){return 2===arguments.length?this.query().deleteParam(a,b):this.query().deleteParam(a),this},b.prototype.addQueryParam=function(a,b,c){return 3===arguments.length?this.query().addParam(a,b,c):this.query().addParam(a,b),this},b.prototype.replaceQueryParam=function(a,b,c){return 3===arguments.length?this.query().replaceParam(a,b,c):this.query().replaceParam(a,b),this},b.prototype.scheme=function(){var b="";return a(this.protocol())?(b+=this.protocol(),this.protocol().indexOf(":")!==this.protocol().length-1&&(b+=":"),b+="//"):this.hasAuthorityPrefix()&&a(this.host())&&(b+="//"),b},b.prototype.origin=function(){var b=this.scheme();return a(this.userInfo())&&a(this.host())&&(b+=this.userInfo(),this.userInfo().indexOf("@")!==this.userInfo().length-1&&(b+="@")),a(this.host())&&(b+=this.host(),a(this.port())&&(b+=":"+this.port())),b},b.prototype.toString=function(){var b=this.origin();return a(this.path())&&(b+=this.path()),a(this.query().toString())&&(0!==this.query().toString().indexOf("?")&&(b+="?"),b+=this.query().toString()),a(this.anchor())&&(0!==this.anchor().indexOf("#")&&(b+="#"),b+=this.anchor()),b},b.prototype.clone=function(){return new b(this.toString())},b}(),c=function(){"use strict";function a(a){return a=decodeURIComponent(a),a=a.replace("+"," ")}function b(a){var b,c,d,e,f,g;if(this.params=[],"undefined"!=typeof a&&null!==a&&""!==a)for(0===a.indexOf("?")&&(a=a.substring(1)),c=a.toString().split(/[&;]/),b=0;b<c.length;b++)d=c[b],e=d.split("="),f=e[0],g=-1===d.indexOf("=")?null:null===e[1]?"":e[1],this.params.push([f,g])}return b.prototype.getParamValue=function(b){var c,d;for(d=0;d<this.params.length;d++)if(c=this.params[d],a(b)===a(c[0]))return c[1]},b.prototype.getParamValues=function(b){var c,d,e=[];for(c=0;c<this.params.length;c++)d=this.params[c],a(b)===a(d[0])&&e.push(d[1]);return e},b.prototype.deleteParam=function(b,c){var d,e,f,g,h=[];for(d=0;d<this.params.length;d++)e=this.params[d],f=a(e[0])===a(b),g=a(e[1])===a(c),(1===arguments.length&&!f||2===arguments.length&&!f&&!g)&&h.push(e);return this.params=h,this},b.prototype.addParam=function(a,b,c){return 3===arguments.length&&-1!==c?(c=Math.min(c,this.params.length),this.params.splice(c,0,[a,b])):arguments.length>0&&this.params.push([a,b]),this},b.prototype.replaceParam=function(b,c,d){var e,f,g=-1;if(3===arguments.length){for(e=0;e<this.params.length;e++)if(f=this.params[e],a(f[0])===a(b)&&decodeURIComponent(f[1])===a(d)){g=e;break}this.deleteParam(b,d).addParam(b,c,g)}else{for(e=0;e<this.params.length;e++)if(f=this.params[e],a(f[0])===a(b)){g=e;break}this.deleteParam(b),this.addParam(b,c,g)}return this},b.prototype.toString=function(){var a,b,c="";for(a=0;a<this.params.length;a++)b=this.params[a],c.length>0&&(c+="&"),c+=null===b[1]?b[0]:b.join("=");return c.length>0?"?"+c:c},b}();a.rg.Uri=b,a.rg.Query=c},"0.0.1",{});
YUI.add("rg-ajax",function(a){a.namespace("rg");var b;a.rg.ajaxDbwAware=function(b,c,d,e,f,g,h){return a.Lang.isUndefined(g)&&(g="GET"),g=g.toUpperCase(),a.rg.ajax(b,c,d,e,f,g,h)},a.rg.ajaxEncodePostData=function(b,c){var d=function e(b,d,f){for(var g in d)if(d.hasOwnProperty(g)){var h=b?b+"["+g+"]":g;a.Lang.isObject(d[g])?e(h,d[g],f):void 0!==d[g]&&null!==d[g]&&(f[h]=c?d[g]:encodeURIComponent(d[g]))}return f};return d("",b,{})},a.rg.ajaxEncodePostDataToQueryString=function(b){var c=a.rg.ajaxEncodePostData(b),d=[];for(var e in c)c.hasOwnProperty(e)&&d.push(e+"="+c[e]);return d.length>0?d.join("&"):null},a.rg.performedRequests=[];var c=function(c,d,e,f,g,h,i){var j=a.merge(i,{Accept:"application/json"});a.Lang.isObject(d)||(d={}),a.Lang.isUndefined(h)&&(h="POST"),("POST"===h||"PUT"===h||"DELETE"===h||"PATCH"===h)&&(d.request_token=a.one("#Rg-Request-Token").get("content"),j["Content-Type"]="application/x-www-form-urlencoded"),b=h;var k=function(b,c,d,e){var f={status:d.status,url:b,method:c,correlationId:d.getResponseHeader("X-Correlation-Id"),pageImpression:d.getResponseHeader("X-Rg-Pi")?!0:!1,ok:e};a.rg.performedRequests.push(f),a.fire("performedAjaxRequest",f)};return{headers:j,method:h,data:a.rg.ajaxEncodePostDataToQueryString(d),arguments:{fn:e,context:f,args:g},on:{success:function(b,d,e){var f;try{if(k(c,h,d,!0),f=a.JSON.parse(d.responseText),f.debug&&a.Array.each(f.debug,function(b){a.log(b.message,"info"),a.log(b.context,"info")}),f.requestToken&&a.one("#Rg-Request-Token").set("content",f.requestToken),f.success===!1&&f.result.redirect)return a.log("ajax redirect: "+f.result.redirect),void a.rg.utils.url.forwardTo(f.result.redirect)}catch(g){f={success:!1,result:{},errors:["An error occured while parsing server response."]}}a.Lang.isFunction(e.fn)&&e.fn.call(e.context,f,e.args)},failure:function(b,d,e){var f;try{k(c,h,d,!1),f=a.JSON.parse(d.responseText)}catch(g){f={success:!1,result:{},errors:["A server error occured"]}}f.success=!1,!a.Lang.isFunction(e.fn)||d&&!d.status||e.fn.call(e.context,f,e.args)}}}};a.rg.ajax=function(d,e,f,g,h,i,j){if(b&&"GET"!==b||"GET"!==i){var k=new a.rg.Uri(d);k.addQueryParam("dbw","true"),d=k.toString()}return a.io(d,c(d,e,f,g,h,i,j))},a.rg.queueAjax=function(d,e,f,g,h,i,j){if(b&&"GET"!==b||"GET"!==i){var k=new a.rg.Uri(d);k.addQueryParam("dbw","true"),d=k.toString()}return a.io.queue(d,c(d,e,f,g,h,i,j))}},"0.0.1",{requires:["io-base","io-queue","json","cookie","rg-uri","rg-utils-url"]});
/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("event-mousewheel", function (e, t) {
    var n = "DOMMouseScroll", r = function (t) {
        var r = e.Array(t, 0, !0), i;
        return e.UA.gecko ? (r[0] = n, i = e.config.win) : i = e.config.doc, r.length < 3 ? r[2] = i : r.splice(2, 0, i), r
    };
    e.Env.evt.plugins.mousewheel = {on: function () {
        return e.Event._attach(r(arguments))
    }, detach: function () {
        return e.Event.detach.apply(e.Event, r(arguments))
    }}
}, "3.14.1", {requires: ["node-base"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("event-mouseenter", function (e, t) {
    var n = e.Env.evt.dom_wrappers, r = e.DOM.contains, i = e.Array, s = function () {
    }, o = {proxyType: "mouseover", relProperty: "fromElement", _notify: function (t, i, s) {
        var o = this._node, u = t.relatedTarget || t[i];
        o !== u && !r(o, u) && s.fire(new e.DOMEventFacade(t, o, n["event:" + e.stamp(o) + t.type]))
    }, on: function (t, n, r) {
        var i = e.Node.getDOMNode(t), s = [this.proxyType, this._notify, i, null, this.relProperty, r];
        n.handle = e.Event._attach(s, {facade: !1})
    }, detach: function (e, t) {
        t.handle.detach()
    }, delegate: function (t, n, r, i) {
        var o = e.Node.getDOMNode(t), u = [this.proxyType, s, o, null, r];
        n.handle = e.Event._attach(u, {facade: !1}), n.handle.sub.filter = i, n.handle.sub.relProperty = this.relProperty, n.handle.sub._notify = this._filterNotify
    }, _filterNotify: function (t, n, s) {
        n = n.slice(), this.args && n.push.apply(n, this.args);
        var o = e.delegate._applyFilter(this.filter, n, s), u = n[0].relatedTarget || n[0][this.relProperty], a, f, l, c, h;
        if (o) {
            o = i(o);
            for (f = 0, l = o.length && (!a || !a.stopped); f < l; ++f) {
                h = o[0];
                if (!r(h, u)) {
                    a || (a = new e.DOMEventFacade(n[0], h, s), a.container = e.one(s.el)), a.currentTarget = e.one(h), c = n[1].fire(a);
                    if (c === !1)break
                }
            }
        }
        return c
    }, detachDelegate: function (e, t) {
        t.handle.detach()
    }};
    e.Event.define("mouseenter", o, !0), e.Event.define("mouseleave", e.merge(o, {proxyType: "mouseout", relProperty: "toElement"}), !0)
}, "3.14.1", {requires: ["event-synthetic"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-key",function(e,t){var n="+alt",r="+ctrl",i="+meta",s="+shift",o=e.Lang.trim,u={KEY_MAP:{enter:13,esc:27,backspace:8,tab:9,pageup:33,pagedown:34},_typeRE:/^(up|down|press):/,_keysRE:/^(?:up|down|press):|\+(alt|ctrl|meta|shift)/g,processArgs:function(t){var n=t.splice(3,1)[0],r=e.Array.hash(n.match(/\+(?:alt|ctrl|meta|shift)\b/g)||[]),i={type:this._typeRE.test(n)?RegExp.$1:null,mods:r,keys:null},s=n.replace(this._keysRE,""),u,a,f,l;if(s){s=s.split(","),i.keys={};for(l=s.length-1;l>=0;--l){u=o(s[l]);if(!u)continue;+u==u?i.keys[u]=r:(f=u.toLowerCase(),this.KEY_MAP[f]?(i.keys[this.KEY_MAP[f]]=r,i.type||(i.type="down")):(u=u.charAt(0),a=u.toUpperCase(),r["+shift"]&&(u=a),i.keys[u.charCodeAt(0)]=u===a?e.merge(r,{"+shift":!0}):r))}}return i.type||(i.type="press"),i},on:function(e,t,o,u){var a=t._extra,f="key"+a.type,l=a.keys,c=u?"delegate":"on";t._detach=e[c](f,function(e){var t=l?l[e.which]:a.mods;t&&(!t[n]||t[n]&&e.altKey)&&(!t[r]||t[r]&&e.ctrlKey)&&(!t[i]||t[i]&&e.metaKey)&&(!t[s]||t[s]&&e.shiftKey)&&o.fire(e)},u)},detach:function(e,t,n){t._detach.detach()}};u.delegate=u.on,u.detachDelegate=u.detach,e.Event.define("key",u,!0)},"3.14.1",{requires:["event-synthetic"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("event-resize", function (e, t) {
    e.Event.define("windowresize", {on: e.UA.gecko && e.UA.gecko < 1.91 ? function (t, n, r) {
        n._handle = e.Event.attach("resize", function (e) {
            r.fire(e)
        })
    } : function (t, n, r) {
        var i = e.config.windowResizeDelay || 100;
        n._handle = e.Event.attach("resize", function (t) {
            n._timer && n._timer.cancel(), n._timer = e.later(i, e, function () {
                r.fire(t)
            })
        })
    }, detach: function (e, t) {
        t._timer && t._timer.cancel(), t._handle.detach()
    }})
}, "3.14.1", {requires: ["node-base", "event-synthetic"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-hover",function(e,t){var n=e.Lang.isFunction,r=function(){},i={processArgs:function(e){var t=n(e[2])?2:3;return n(e[t])?e.splice(t,1)[0]:r},on:function(e,t,n,r){var i=t.args?t.args.slice():[];i.unshift(null),t._detach=e[r?"delegate":"on"]({mouseenter:function(e){e.phase="over",n.fire(e)},mouseleave:function(e){var n=t.context||this;i[0]=e,e.type="hover",e.phase="out",t._extra.apply(n,i)}},r)},detach:function(e,t,n){t._detach.detach()}};i.delegate=i.on,i.detachDelegate=i.detach,e.Event.define("hover",i)},"3.14.1",{requires:["event-mouseenter"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("event-outside", function (e, t) {
    var n = ["blur", "change", "click", "dblclick", "focus", "keydown", "keypress", "keyup", "mousedown", "mousemove", "mouseout", "mouseover", "mouseup", "select", "submit"];
    e.Event.defineOutside = function (t, n) {
        n = n || t + "outside";
        var r = {on: function (n, r, i) {
            r.handle = e.one("doc").on(t, function (e) {
                this.isOutside(n, e.target) && (e.currentTarget = n, i.fire(e))
            }, this)
        }, detach: function (e, t, n) {
            t.handle.detach()
        }, delegate: function (n, r, i, s) {
            r.handle = e.one("doc").delegate(t, function (e) {
                this.isOutside(n, e.target) && i.fire(e)
            }, s, this)
        }, isOutside: function (e, t) {
            return t !== e && !t.ancestor(function (t) {
                return t === e
            })
        }};
        r.detachDelegate = r.detach, e.Event.define(n, r)
    }, e.Array.each(n, function (t) {
        e.Event.defineOutside(t)
    })
}, "3.14.1", {requires: ["event-synthetic"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-touch",function(e,t){var n="scale",r="rotation",i="identifier",s=e.config.win,o={};e.DOMEventFacade.prototype._touch=function(t,s,o){var u,a,f,l,c;if(t.touches){this.touches=[],c={};for(u=0,a=t.touches.length;u<a;++u)l=t.touches[u],c[e.stamp(l)]=this.touches[u]=new e.DOMEventFacade(l,s,o)}if(t.targetTouches){this.targetTouches=[];for(u=0,a=t.targetTouches.length;u<a;++u)l=t.targetTouches[u],f=c&&c[e.stamp(l,!0)],this.targetTouches[u]=f||new e.DOMEventFacade(l,s,o)}if(t.changedTouches){this.changedTouches=[];for(u=0,a=t.changedTouches.length;u<a;++u)l=t.changedTouches[u],f=c&&c[e.stamp(l,!0)],this.changedTouches[u]=f||new e.DOMEventFacade(l,s,o)}n in t&&(this[n]=t[n]),r in t&&(this[r]=t[r]),i in t&&(this[i]=t[i])},e.Node.DOM_EVENTS&&e.mix(e.Node.DOM_EVENTS,{touchstart:1,touchmove:1,touchend:1,touchcancel:1,gesturestart:1,gesturechange:1,gestureend:1,MSPointerDown:1,MSPointerUp:1,MSPointerMove:1}),s&&"ontouchstart"in s&&!(e.UA.chrome&&e.UA.chrome<6)?(o.start=["touchstart","mousedown"],o.end=["touchend","mouseup"],o.move=["touchmove","mousemove"],o.cancel=["touchcancel","mousecancel"]):s&&"msPointerEnabled"in s.navigator?(o.start="MSPointerDown",o.end="MSPointerUp",o.move="MSPointerMove",o.cancel="MSPointerCancel"):(o.start="mousedown",o.end="mouseup",o.move="mousemove",o.cancel="mousecancel"),e.Event._GESTURE_MAP=o},"3.14.1",{requires:["node-base"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-move",function(e,t){var n=e.Event._GESTURE_MAP,r={start:n.start,end:n.end,move:n.move},i="start",s="move",o="end",u="gesture"+s,a=u+o,f=u+i,l="_msh",c="_mh",h="_meh",p="_dmsh",d="_dmh",v="_dmeh",m="_ms",g="_m",y="minTime",b="minDistance",w="preventDefault",E="button",S="ownerDocument",x="currentTarget",T="target",N="nodeType",C=e.config.win&&"msPointerEnabled"in e.config.win.navigator,k="msTouchActionCount",L="msInitTouchAction",A=function(t,n,r){var i=r?4:3,s=n.length>i?e.merge(n.splice(i,1)[0]):{};return w in s||(s[w]=t.PREVENT_DEFAULT),s},O=function(e,t){return t._extra.root||e.get(N)===9?e:e.get(S)},M=function(t){var n=t.getDOMNode();return t.compareTo(e.config.doc)&&n.documentElement?n.documentElement:!1},_=function(e,t,n){e.pageX=t.pageX,e.pageY=t.pageY,e.screenX=t.screenX,e.screenY=t.screenY,e.clientX=t.clientX,e.clientY=t.clientY,e[T]=e[T]||t[T],e[x]=e[x]||t[x],e[E]=n&&n[E]||1},D=function(t){var n=M(t)||t.getDOMNode(),r=t.getData(k);C&&(r||(r=0,t.setData(L,n.style.msTouchAction)),n.style.msTouchAction=e.Event._DEFAULT_TOUCH_ACTION,r++,t.setData(k,r))},P=function(e){var t=M(e)||e.getDOMNode(),n=e.getData(k),r=e.getData(L);C&&(n--,e.setData(k,n),n===0&&t.style.msTouchAction!==r&&(t.style.msTouchAction=r))},H=function(e,t){t&&(!t.call||t(e))&&e.preventDefault()},B=e.Event.define;e.Event._DEFAULT_TOUCH_ACTION="none",B(f,{on:function(e,t,n){D(e),t[l]=e.on(r[i],this._onStart,this,e,t,n)},delegate:function(e,t,n,s){var o=this;t[p]=e.delegate(r[i],function(r){o._onStart(r,e,t,n,!0)},s)},detachDelegate:function(e,t,n,r){var i=t[p];i&&(i.detach(),t[p]=null),P(e)},detach:function(e,t,n){var r=t[l];r&&(r.detach(),t[l]=null),P(e)},processArgs:function(e,t){var n=A(this,e,t);return y in n||(n[y]=this.MIN_TIME),b in n||(n[b]=this.MIN_DISTANCE),n},_onStart:function(t,n,i,u,a){a&&(n=t[x]);var f=i._extra,l=!0,c=f[y],h=f[b],p=f.button,d=f[w],v=O(n,i),m;t.touches?t.touches.length===1?_(t,t.touches[0],f):l=!1:l=p===undefined||p===t.button,l&&(H(t,d),c===0||h===0?this._start(t,n,u,f):(m=[t.pageX,t.pageY],c>0&&(f._ht=e.later(c,this,this._start,[t,n,u,f]),f._hme=v.on(r[o],e.bind(function(){this._cancel(f)},this))),h>0&&(f._hm=v.on(r[s],e.bind(function(e){(Math.abs(e.pageX-m[0])>h||Math.abs(e.pageY-m[1])>h)&&this._start(t,n,u,f)},this)))))},_cancel:function(e){e._ht&&(e._ht.cancel(),e._ht=null),e._hme&&(e._hme.detach(),e._hme=null),e._hm&&(e._hm.detach(),e._hm=null)},_start:function(e,t,n,r){r&&this._cancel(r),e.type=f,t.setData(m,e),n.fire(e)},MIN_TIME:0,MIN_DISTANCE:0,PREVENT_DEFAULT:!1}),B(u,{on:function(e,t,n){D(e);var i=O(e,t,r[s]),o=i.on(r[s],this._onMove,this,e,t,n);t[c]=o},delegate:function(e,t,n,i){var o=this;t[d]=e.delegate(r[s],function(r){o._onMove(r,e,t,n,!0)},i)},detach:function(e,t,n){var r=t[c];r&&(r.detach(),t[c]=null),P(e)},detachDelegate:function(e,t,n,r){var i=t[d];i&&(i.detach(),t[d]=null),P(e)},processArgs:function(e,t){return A(this,e,t)},_onMove:function(e,t,n,r,i){i&&(t=e[x]);var s=n._extra.standAlone||t.getData(m),o=n._extra.preventDefault;s&&(e.touches&&(e.touches.length===1?_(e,e.touches[0]):s=!1),s&&(H(e,o),e.type=u,r.fire(e)))},PREVENT_DEFAULT:!1}),B(a,{on:function(e,t,n){D(e);var i=O(e,t),s=i.on(r[o],this._onEnd,this,e,t,n);t[h]=s},delegate:function(e,t,n,i){var s=this;t[v]=e.delegate(r[o],function(r){s._onEnd(r,e,t,n,!0)},i)},detachDelegate:function(e,t,n,r){var i=t[v];i&&(i.detach(),t[v]=null),P(e)},detach:function(e,t,n){var r=t[h];r&&(r.detach(),t[h]=null),P(e)},processArgs:function(e,t){return A(this,e,t)},_onEnd:function(e,t,n,r,i){i&&(t=e[x]);var s=n._extra.standAlone||t.getData(g)||t.getData(m),o=n._extra.preventDefault;s&&(e.changedTouches&&(e.changedTouches.length===1?_(e,e.changedTouches[0]):s=!1),s&&(H(e,o),e.type=a,r.fire(e),t.clearData(m),t.clearData(g)))},PREVENT_DEFAULT:!1})},"3.14.1",{requires:["node-base","event-touch","event-synthetic"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-flick",function(e,t){var n=e.Event._GESTURE_MAP,r={start:n.start,end:n.end,move:n.move},i="start",s="end",o="move",u="ownerDocument",a="minVelocity",f="minDistance",l="preventDefault",c="_fs",h="_fsh",p="_feh",d="_fmh",v="nodeType";e.Event.define("flick",{on:function(e,t,n){var s=e.on(r[i],this._onStart,this,e,t,n);t[h]=s},detach:function(e,t,n){var r=t[h],i=t[p];r&&(r.detach(),t[h]=null),i&&(i.detach(),t[p]=null)},processArgs:function(t){var n=t.length>3?e.merge(t.splice(3,1)[0]):{};return a in n||(n[a]=this.MIN_VELOCITY),f in n||(n[f]=this.MIN_DISTANCE),l in n||(n[l]=this.PREVENT_DEFAULT),n},_onStart:function(t,n,i,a){var f=!0,l,h,m,g=i._extra.preventDefault,y=t;t.touches&&(f=t.touches.length===1,t=t.touches[0]),f&&(g&&(!g.call||g(t))&&y.preventDefault(),t.flick={time:(new Date).getTime()},i[c]=t,l=i[p],m=n.get(v)===9?n:n.get(u),l||(l=m.on(r[s],e.bind(this._onEnd,this),null,n,i,a),i[p]=l),i[d]=m.once(r[o],e.bind(this._onMove,this),null,n,i,a))},_onMove:function(e,t,n,r){var i=n[c];i&&i.flick&&(i.flick.time=(new Date).getTime())},_onEnd:function(e,t,n,r){var i=(new Date).getTime(),s=n[c],o=!!s,u=e,h,p,v,m,g,y,b,w,E=n[d];E&&(E.detach(),delete n[d]),o&&(e.changedTouches&&(e.changedTouches.length===1&&e.touches.length===0?u=e.changedTouches[0]:o=!1),o&&(m=n._extra,v=m[l],v&&(!v.call||v(e))&&e.preventDefault(),h=s.flick.time,i=(new Date).getTime(),p=i-h,g=[u.pageX-s.pageX,u.pageY-s.pageY],m.axis?w=m.axis:w=Math.abs(g[0])>=Math.abs(g[1])?"x":"y",y=g[w==="x"?0:1],b=p!==0?y/p:0,isFinite(b)&&Math.abs(y)>=m[f]&&Math.abs(b)>=m[a]&&(e.type="flick",e.flick={time:p,distance:y,velocity:b,axis:w,start:s},r.fire(e)),n[c]=null))},MIN_VELOCITY:0,MIN_DISTANCE:0,PREVENT_DEFAULT:!1})},"3.14.1",{requires:["node-base","event-touch","event-synthetic"]});

/*
 YUI 3.14.1 (build 63049cb)
 Copyright 2013 Yahoo! Inc. All rights reserved.
 Licensed under the BSD License.
 http://yuilibrary.com/license/
 */

YUI.add("event-valuechange", function (e, t) {
    var n = "_valuechange", r = "value", i = "nodeName", s, o = {POLL_INTERVAL: 50, TIMEOUT: 1e4, _poll: function (t, r) {
        var i = t._node, s = r.e, u = t._data && t._data[n], a = 0, f, l, c, h, p, d;
        if (!i || !u) {
            o._stopPolling(t);
            return
        }
        l = u.prevVal, h = u.nodeName, u.isEditable ? c = i.innerHTML : h === "input" || h === "textarea" ? c = i.value : h === "select" && (p = i.options[i.selectedIndex], c = p.value || p.text), c !== l && (u.prevVal = c, f = {_event: s, currentTarget: s && s.currentTarget || t, newVal: c, prevVal: l, target: s && s.target || t}, e.Object.some(u.notifiers, function (e) {
            var t = e.handle.evt, n;
            a !== 1 ? e.fire(f) : t.el === d && e.fire(f), n = t && t._facade ? t._facade.stopped : 0, n > a && (a = n, a === 1 && (d = t.el));
            if (a === 2)return!0
        }), o._refreshTimeout(t))
    }, _refreshTimeout: function (e, t) {
        if (!e._node)return;
        var r = e.getData(n);
        o._stopTimeout(e), r.timeout = setTimeout(function () {
            o._stopPolling(e, t)
        }, o.TIMEOUT)
    }, _startPolling: function (t, s, u) {
        var a, f;
        if (!t.test("input,textarea,select") && !(f = o._isEditable(t)))return;
        a = t.getData(n), a || (a = {nodeName: t.get(i).toLowerCase(), isEditable: f, prevVal: f ? t.getDOMNode().innerHTML : t.get(r)}, t.setData(n, a)), a.notifiers || (a.notifiers = {});
        if (a.interval) {
            if (!u.force) {
                a.notifiers[e.stamp(s)] = s;
                return
            }
            o._stopPolling(t, s)
        }
        a.notifiers[e.stamp(s)] = s, a.interval = setInterval(function () {
            o._poll(t, u)
        }, o.POLL_INTERVAL), o._refreshTimeout(t, s)
    }, _stopPolling: function (t, r) {
        if (!t._node)return;
        var i = t.getData(n) || {};
        clearInterval(i.interval), delete i.interval, o._stopTimeout(t), r ? i.notifiers && delete i.notifiers[e.stamp(r)] : i.notifiers = {}
    }, _stopTimeout: function (e) {
        var t = e.getData(n) || {};
        clearTimeout(t.timeout), delete t.timeout
    }, _isEditable: function (e) {
        var t = e._node;
        return t.contentEditable === "true" || t.contentEditable === ""
    }, _onBlur: function (e, t) {
        o._stopPolling(e.currentTarget, t)
    }, _onFocus: function (e, t) {
        var s = e.currentTarget, u = s.getData(n);
        u || (u = {isEditable: o._isEditable(s), nodeName: s.get(i).toLowerCase()}, s.setData(n, u)), u.prevVal = u.isEditable ? s.getDOMNode().innerHTML : s.get(r), o._startPolling(s, t, {e: e})
    }, _onKeyDown: function (e, t) {
        o._startPolling(e.currentTarget, t, {e: e})
    }, _onKeyUp: function (e, t) {
        (e.charCode === 229 || e.charCode === 197) && o._startPolling(e.currentTarget, t, {e: e, force: !0})
    }, _onMouseDown: function (e, t) {
        o._startPolling(e.currentTarget, t, {e: e})
    }, _onSubscribe: function (t, s, u, a) {
        var f, l, c, h, p;
        l = {blur: o._onBlur, focus: o._onFocus, keydown: o._onKeyDown, keyup: o._onKeyUp, mousedown: o._onMouseDown}, f = u._valuechange = {};
        if (a)f.delegated = !0, f.getNodes = function () {
            return h = t.all("input,textarea,select").filter(a), p = t.all('[contenteditable="true"],[contenteditable=""]').filter(a), h.concat(p)
        }, f.getNodes().each(function (e) {
            e.getData(n) || e.setData(n, {nodeName: e.get(i).toLowerCase(), isEditable: o._isEditable(e), prevVal: c ? e.getDOMNode().innerHTML : e.get(r)})
        }), u._handles = e.delegate(l, t, a, null, u); else {
            c = o._isEditable(t);
            if (!t.test("input,textarea,select") && !c)return;
            t.getData(n) || t.setData(n, {nodeName: t.get(i).toLowerCase(), isEditable: c, prevVal: c ? t.getDOMNode().innerHTML : t.get(r)}), u._handles = t.on(l, null, null, u)
        }
    }, _onUnsubscribe: function (e, t, n) {
        var r = n._valuechange;
        n._handles && n._handles.detach(), r.delegated ? r.getNodes().each(function (e) {
            o._stopPolling(e, n)
        }) : o._stopPolling(e, n)
    }};
    s = {detach: o._onUnsubscribe, on: o._onSubscribe, delegate: o._onSubscribe, detachDelegate: o._onUnsubscribe, publishConfig: {emitFacade: !0}}, e.Event.define("valuechange", s), e.Event.define("valueChange", s), e.ValueChange = o
}, "3.14.1", {requires: ["event-focus", "event-synthetic"]});

/*
YUI 3.14.1 (build 63049cb)
Copyright 2013 Yahoo! Inc. All rights reserved.
Licensed under the BSD License.
http://yuilibrary.com/license/
*/

YUI.add("event-tap",function(e,t){function u(t,n){n=n||e.Object.values(o),e.Array.each(n,function(e){var n=t[e];n&&(n.detach(),t[e]=null)})}var n=e.config.doc,r=e.Event._GESTURE_MAP,i=r.start,s="tap",o={START:"Y_TAP_ON_START_HANDLE",END:"Y_TAP_ON_END_HANDLE",CANCEL:"Y_TAP_ON_CANCEL_HANDLE"};e.Event.define(s,{publishConfig:{preventedFn:function(e){var t=e.target.once("click",function(e){e.preventDefault()});setTimeout(function(){t.detach()},100)}},processArgs:function(e,t){if(!t){var n=e[3];return e.splice(3,1),n}},on:function(e,t,n){t[o.START]=e.on(i,this._start,this,e,t,n)},detach:function(e,t,n){u(t)},delegate:function(e,t,n,r){t[o.START]=e.delegate(i,function(r){this._start(r,e,t,n,!0)},r,this)},detachDelegate:function(e,t,n){u(t)},_start:function(e,t,n,r,i){var s={canceled:!1,eventType:e.type},u=n.preventMouse||!1;if(e.button&&e.button===3)return;if(e.touches&&e.touches.length!==1)return;s.node=i?e.currentTarget:t,e.touches?s.startXY=[e.touches[0].pageX,e.touches[0].pageY]:s.startXY=[e.pageX,e.pageY],e.touches?(n[o.END]=t.once("touchend",this._end,this,t,n,r,i,s),n[o.CANCEL]=t.once("touchcancel",this.detach,this,t,n,r,i,s),n.preventMouse=!0):s.eventType.indexOf("mouse")!==-1&&!u?(n[o.END]=t.once("mouseup",this._end,this,t,n,r,i,s),n[o.CANCEL]=t.once("mousecancel",this.detach,this,t,n,r,i,s)):s.eventType.indexOf("mouse")!==-1&&u?n.preventMouse=!1:s.eventType.indexOf("MSPointer")!==-1&&(n[o.END]=t.once("MSPointerUp",this._end,this,t,n,r,i,s),n[o.CANCEL]=t.once("MSPointerCancel",this.detach,this,t,n,r,i,s))},_end:function(e,t,n,r,i,a){var f=a.startXY,l,c,h=15;n._extra&&n._extra.sensitivity>=0&&(h=n._extra.sensitivity),e.changedTouches?(l=[e.changedTouches[0].pageX,e.changedTouches[0].pageY],c=[e.changedTouches[0].clientX,e.changedTouches[0].clientY]):(l=[e.pageX,e.pageY],c=[e.clientX,e.clientY]),Math.abs(l[0]-f[0])<=h&&Math.abs(l[1]-f[1])<=h&&(e.type=s,e.pageX=l[0],e.pageY=l[1],e.clientX=c[0],e.clientY=c[1],e.currentTarget=a.node,r.fire(e)),u(n,[o.END,o.CANCEL])}})},"3.14.1",{requires:["node-base","event-base","event-touch","event-synthetic"]});

YUI.add("rg.core.util.ParameterFilter",function(a){a.namespace("rg.core.util");var b=window;a.rg.core.util.ParameterFilter={filter:function(c){b.history&&b.history.replaceState&&a.on("load",function(){var d,e,f,g,h,i,j,k,l,m,n=b.location.href,o=!1;if(d=n.indexOf("#"),-1===d?e=n:(e=n.substr(0,d),f=n.substr(d+1)),g=e.indexOf("?"),-1!==g){h=e.substr(0,g),i=e.substr(g+1),j=a.QueryString.parse(i);for(k in c)c.hasOwnProperty(k)&&(l=c[k],j[l]&&(o=!0,delete j[l]));for(k in j)j.hasOwnProperty(k)&&"_"===k.substr(0,1)&&(o=!0,delete j[k]);o&&(i=a.QueryString.stringify(j),m=h,i&&(m=m+"?"+i),f&&(m=m+"#"+f),b.history.replaceState(null,null,m))}})}}},"1.0.0",{requires:["querystring"]});
