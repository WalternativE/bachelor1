/*
	AIAA.org membership tracking script
	Jay Cominoli -- jcominoli@nexusdirect.com
	http://www.nexusdirect.com
	Written: 2013.02.28
	ga('create', 'UA-27759346-1',{'name':'aiaa.org'});

    ga('send','pageview', '/netforum-store-complete/?samTest=samTestWithNoChange');
	
	-- HOW THIS WORKS --
	When a site visitor clicks on a google adwords URL to the aiaa.org 
	site the  URL will have a query parameter of "gawid" with a value to 
	categorize and identify the ad.  This value will be cookied and 
	allowed across subdomains.  Once that site visitor visits the store 
	and/or membership purchase and completes a transaction, the script 
	will collect information on the thank you page (email address and 
	products purcahsed) and pass this information along to google analytics
	as an "event".  Statistics and metric will be available in the 
	Google Analytics account for AIAA.org
	
	
	-- INSTALLATION --
	The following code should be installed on:
		> www.aiaa.org
		> netforum.aiaa.org
	
		::NOTE::
		The code might possibly want to be installed on additional aiaa.org 
		subdomains (if they are not already integrated in to the main aiaa.org CMS) 
		if the adwords campaigns points to a landing page on that subdomain
	
		ex: an adword campaign landing on http://arc.aiaa.org/
		but wanting to track membership signups
	
		This may not be applicable, but the code will work all the same as long as 
		it is available on the other subdomains.
	
	To avoid several rounds or back and forth and sending new versions of this 
	script to your third party vendor (netforum), we recommend that you allow us
	to host the javascript.  This will allow Nexus Direct to make any changes 
	immediately to the script while we are testing.  You are free to host this 
	code yourselves if you wish.
	
	To use the Nexus Direct hosted script, please simply use:	
	<script type="text/javascript" charset="utf-8" src="https://www.nexusdirect.com/clients/aiaa/aiaa-tracking.js"></script>
	somewhere after your declaration of jquery.
	
	
	-- REQUIREMENTS -- 
	This script uses jquery to run.  Your site already uses jquery, but if you 
	remove it, this script will not function.
	
	This script also relies on specific CSS classes on the netforum's cart thank you 
	page.  If those change, this script will not function correctly.
	
	
*/

if (!window.jQuery) {
  var jq = document.createElement('script'); jq.type = 'text/javascript';
  // Path to jquery.js file, eg. Google hosted version
  jq.src = 'https://ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js';
  document.getElementsByTagName('head')[0].appendChild(jq);
}

$(document).ready(function() {
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){ 
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o), 
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m) 
    })(window,document,'script','//www.google-analytics.com/analytics.js','ga'); 

   ga('create', 'UA-27759346-1','aiaa.org');
   
	var pageID = getQueryVar("gawid");
	if( pageID!=""){
		$.cookie("pageID", pageID, {
			expires : 30,
			path    : '/',
			domain  : 'aiaa.org',
			secure  : false
		});
	}

	products = "";
	var wc = getQueryVar("webcode");
	if( wc=="storecomplete"){
		
		$("span").each(function(){
			if( $(this).attr("id").indexOf("lblEmailTo") > 0 ){
				email = $(this).text().replace(/^\s+|\s+$/g, '');
			}
		});

		$('.ShoppingCart tr').each(function(){
			if( $(this).find("td").length > 0 ){
				product = "";
				tdOBJ = $(this).find("td");
				tdOBJ.each(function(){
					if($(this).hasClass('StoreCartQtyColumn'))
					{
						product += $(this).find('input[type=text]').val().replace(/^\s+|\s+$/g, '') + "__";
					}
					else
					{
						product += $(this).text().replace(/^\s+|\s+$/g, '') + "__";	
					}			
				});
			
				
				
				if (typeof _gaq != "undefined"){
					_gaq.push(['_trackPageview', '/netforum-store-complete/?trackingID=' +  $.cookie("pageID") + '&email=' + escape(email) 
					+ '&products=' + escape(product) ]);

				}
				//this new bit of code added by Sam...should deal with the new GA method as opposed to the old one above
				else if(typeof (ga) !== "undefined")
				{	
					ga('send','pageview', '/netforum-store-complete/?trackingID=' +  $.cookie("pageID") + '&email=' + escape(email) 
					+ '&products=' + escape(product));
				}
				// products += product + "||";
			}
		});
		
	}
	
});

function getQueryVar(name){
	theVar = name.replace(/[\[]/, "\\\[").replace(/[\]]/, "\\\]");
	rexp = new RegExp("[\\?&]" + theVar.toLowerCase() + "=([^&#]*)");
	result = rexp.exec(window.location.search.toLowerCase());
	
	if(result == null){
		return "";
	} else{
		return result[1].toLowerCase();
	}
}



/* 
	THE CODE BELOW WAS NOT WRITTEN BY NEXUS DIRECT
	BUT USED AS AN AID TO DEVELOP THE ABOVE CODE.
*/

/*!
* jQuery Cookie Plugin v1.3.1
* https://github.com/carhartl/jquery-cookie
*
* Copyright 2013 Klaus Hartl
* Released under the MIT license
*/

(function(factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function($) {

	var pluses = /\+/g;

	function raw(s) {
		return s;
	}

	function decoded(s) {
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	function converted(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}
		try {
			return config.json ? JSON.parse(s) : s;
		} catch (er) {}
	}

	var config = $.cookie = function(key, value, options) {

			// write
			if (value !== undefined) {
				options = $.extend({}, config.defaults, options);

				if (typeof options.expires === 'number') {
					var days = options.expires,
						t = options.expires = new Date();
					t.setDate(t.getDate() + days);
				}

				value = config.json ? JSON.stringify(value) : String(value);

				return (document.cookie = [
				config.raw ? key : encodeURIComponent(key), '=', config.raw ? value : encodeURIComponent(value), options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path ? '; path=' + options.path : '', options.domain ? '; domain=' + options.domain : '', options.secure ? '; secure' : ''].join(''));
			}

			// read
			var decode = config.raw ? raw : decoded;
			var cookies = document.cookie.split('; ');
			var result = key ? undefined : {};
			for (var i = 0, l = cookies.length; i < l; i++) {
				var parts = cookies[i].split('=');
				var name = decode(parts.shift());
				var cookie = decode(parts.join('='));

				if (key && key === name) {
					result = converted(cookie);
					break;
				}

				if (!key) {
					result[name] = converted(cookie);
				}
			}

			return result;
		};

	config.defaults = {};

	$.removeCookie = function(key, options) {
		if ($.cookie(key) !== undefined) {
			$.cookie(key, '', $.extend(options, {
				expires: -1
			}));
			return true;
		}
		return false;
	};

}));

