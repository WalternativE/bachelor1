/*!
 * jQuery blockUI plugin
 * Version 2.31 (06-JAN-2010)
 * @requires jQuery v1.2.3 or later
 *
 * Examples at: http://malsup.com/jquery/block/
 * Copyright (c) 2007-2008 M. Alsup
 * Dual licensed under the MIT and GPL licenses:
 * http://www.opensource.org/licenses/mit-license.php
 * http://www.gnu.org/licenses/gpl.html
 *
 * Thanks to Amir-Hossein Sobhi for some excellent contributions!
 */

;(function($) {

$.fn._fadeIn = $.fn.fadeIn;

var noOp = function() {};

// this bit is to ensure we don't call setExpression when we shouldn't (with extra muscle to handle
// retarded userAgent strings on Vista)
var mode = document.documentMode || 0;
var setExpr = $.browser.msie && (($.browser.version < 8 && !mode) || mode < 8);
var ie6 = $.browser.msie && /MSIE 6.0/.test(navigator.userAgent) && !mode;

// global $ methods for blocking/unblocking the entire page
$.blockUI   = function(opts) { install(window, opts); };
$.unblockUI = function(opts) { remove(window, opts); };

// convenience method for quick growl-like notifications  (http://www.google.com/search?q=growl)
$.growlUI = function(title, message, timeout, onClose) {
	var $m = $('<div class="growlUI"></div>');
	if (title) $m.append('<h1>'+title+'</h1>');
	if (message) $m.append('<h2>'+message+'</h2>');
	if (timeout == undefined) timeout = 3000;
	$.blockUI({
		message: $m, fadeIn: 700, fadeOut: 1000, centerY: false,
		timeout: timeout, showOverlay: false,
		onUnblock: onClose, 
		css: $.blockUI.defaults.growlCSS
	});
};

// plugin method for blocking element content
$.fn.block = function(opts) {
	return this.unblock({ fadeOut: 0 }).each(function() {
		if ($.css(this,'position') == 'static')
			this.style.position = 'relative';
		if ($.browser.msie)
			this.style.zoom = 1; // force 'hasLayout'
		install(this, opts);
	});
};

// plugin method for unblocking element content
$.fn.unblock = function(opts) {
	return this.each(function() {
		remove(this, opts);
	});
};

$.blockUI.version = 2.31; // 2nd generation blocking at no extra cost!

// override these in your code to change the default behavior and style
$.blockUI.defaults = {
	// message displayed when blocking (use null for no message)
	message:  '<h1>Please wait...</h1>',

	title: null,	  // title string; only used when theme == true
	draggable: true,  // only used when theme == true (requires jquery-ui.js to be loaded)
	
	theme: false, // set to true to use with jQuery UI themes
	
	// styles for the message when blocking; if you wish to disable
	// these and use an external stylesheet then do this in your code:
	// $.blockUI.defaults.css = {};
	css: {
		padding:	0,
		margin:		0,
		width:		'30%',
		top:		'40%',
		left:		'35%',
		textAlign:	'center',
		color:		'#000',
		border:		'3px solid #aaa',
		backgroundColor:'#fff',
		cursor:		'wait'
	},
	
	// minimal style set used when themes are used
	themedCSS: {
		width:	'30%',
		top:	'40%',
		left:	'35%'
	},

	// styles for the overlay
	overlayCSS:  {
		backgroundColor: '#000',
		opacity:	  	 0.6,
		cursor:		  	 'wait'
	},

	// styles applied when using $.growlUI
	growlCSS: {
		width:  	'350px',
		top:		'10px',
		left:   	'',
		right:  	'10px',
		border: 	'none',
		padding:	'5px',
		opacity:	0.6,
		cursor: 	'default',
		color:		'#fff',
		backgroundColor: '#000',
		'-webkit-border-radius': '10px',
		'-moz-border-radius':	 '10px'
	},
	
	// IE issues: 'about:blank' fails on HTTPS and javascript:false is s-l-o-w
	// (hat tip to Jorge H. N. de Vasconcelos)
	iframeSrc: /^https/i.test(window.location.href || '') ? 'javascript:false' : 'about:blank',

	// force usage of iframe in non-IE browsers (handy for blocking applets)
	forceIframe: false,

	// z-index for the blocking overlay
	baseZ: 1000,

	// set these to true to have the message automatically centered
	centerX: true, // <-- only effects element blocking (page block controlled via css above)
	centerY: true,

	// allow body element to be stetched in ie6; this makes blocking look better
	// on "short" pages.  disable if you wish to prevent changes to the body height
	allowBodyStretch: true,

	// enable if you want key and mouse events to be disabled for content that is blocked
	bindEvents: true,

	// be default blockUI will supress tab navigation from leaving blocking content
	// (if bindEvents is true)
	constrainTabKey: true,

	// fadeIn time in millis; set to 0 to disable fadeIn on block
	fadeIn:  200,

	// fadeOut time in millis; set to 0 to disable fadeOut on unblock
	fadeOut:  400,

	// time in millis to wait before auto-unblocking; set to 0 to disable auto-unblock
	timeout: 0,

	// disable if you don't want to show the overlay
	showOverlay: true,

	// if true, focus will be placed in the first available input field when
	// page blocking
	focusInput: true,

	// suppresses the use of overlay styles on FF/Linux (due to performance issues with opacity)
	applyPlatformOpacityRules: true,
	
	// callback method invoked when fadeIn has completed and blocking message is visible
	onBlock: null,

	// callback method invoked when unblocking has completed; the callback is
	// passed the element that has been unblocked (which is the window object for page
	// blocks) and the options that were passed to the unblock call:
	//	 onUnblock(element, options)
	onUnblock: null,

	// don't ask; if you really must know: http://groups.google.com/group/jquery-en/browse_thread/thread/36640a8730503595/2f6a79a77a78e493#2f6a79a77a78e493
	quirksmodeOffsetHack: 4
};

// private data and functions follow...

var pageBlock = null;
var pageBlockEls = [];

function install(el, opts) {
	var full = (el == window);
	var msg = opts && opts.message !== undefined ? opts.message : undefined;
	opts = $.extend({}, $.blockUI.defaults, opts || {});
	opts.overlayCSS = $.extend({}, $.blockUI.defaults.overlayCSS, opts.overlayCSS || {});
	var css = $.extend({}, $.blockUI.defaults.css, opts.css || {});
	var themedCSS = $.extend({}, $.blockUI.defaults.themedCSS, opts.themedCSS || {});
	msg = msg === undefined ? opts.message : msg;

	// remove the current block (if there is one)
	if (full && pageBlock)
		remove(window, {fadeOut:0});

	// if an existing element is being used as the blocking content then we capture
	// its current place in the DOM (and current display style) so we can restore
	// it when we unblock
	if (msg && typeof msg != 'string' && (msg.parentNode || msg.jquery)) {
		var node = msg.jquery ? msg[0] : msg;
		var data = {};
		$(el).data('blockUI.history', data);
		data.el = node;
		data.parent = node.parentNode;
		data.display = node.style.display;
		data.position = node.style.position;
		if (data.parent)
			data.parent.removeChild(node);
	}

	var z = opts.baseZ;

	// blockUI uses 3 layers for blocking, for simplicity they are all used on every platform;
	// layer1 is the iframe layer which is used to supress bleed through of underlying content
	// layer2 is the overlay layer which has opacity and a wait cursor (by default)
	// layer3 is the message content that is displayed while blocking

	var lyr1 = ($.browser.msie || opts.forceIframe) 
		? $('<iframe class="blockUI" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;position:absolute;width:100%;height:100%;top:0;left:0" src="'+opts.iframeSrc+'"></iframe>')
		: $('<div class="blockUI" style="display:none"></div>');
	var lyr2 = $('<div class="blockUI blockOverlay" style="z-index:'+ (z++) +';display:none;border:none;margin:0;padding:0;width:100%;height:100%;top:0;left:0"></div>');
	
	var lyr3;
	if (opts.theme && full) {
		var s = '<div class="blockUI blockMsg blockPage ui-dialog ui-widget ui-corner-all" style="z-index:'+z+';display:none;position:fixed">' +
					'<div class="ui-widget-header ui-dialog-titlebar blockTitle">'+(opts.title || '&nbsp;')+'</div>' +
					'<div class="ui-widget-content ui-dialog-content"></div>' +
				'</div>';
		lyr3 = $(s);
	}
	else {
		lyr3 = full ? $('<div class="blockUI blockMsg blockPage" style="z-index:'+z+';display:none;position:fixed"></div>')
					: $('<div class="blockUI blockMsg blockElement" style="z-index:'+z+';display:none;position:absolute"></div>');
	}						   

	// if we have a message, style it
	if (msg) {
		if (opts.theme) {
			lyr3.css(themedCSS);
			lyr3.addClass('ui-widget-content');
		}
		else 
			lyr3.css(css);
	}

	// style the overlay
	if (!opts.applyPlatformOpacityRules || !($.browser.mozilla && /Linux/.test(navigator.platform)))
		lyr2.css(opts.overlayCSS);
	lyr2.css('position', full ? 'fixed' : 'absolute');

	// make iframe layer transparent in IE
	if ($.browser.msie || opts.forceIframe)
		lyr1.css('opacity',0.0);

	//$([lyr1[0],lyr2[0],lyr3[0]]).appendTo(full ? 'body' : el);
	var layers = [lyr1,lyr2,lyr3], $par = full ? $('body') : $(el);
	$.each(layers, function() {
		this.appendTo($par);
	});
	
	if (opts.theme && opts.draggable && $.fn.draggable) {
		lyr3.draggable({
			handle: '.ui-dialog-titlebar',
			cancel: 'li'
		});
	}

	// ie7 must use absolute positioning in quirks mode and to account for activex issues (when scrolling)
	var expr = setExpr && (!$.boxModel || $('object,embed', full ? null : el).length > 0);
	if (ie6 || expr) {
		// give body 100% height
		if (full && opts.allowBodyStretch && $.boxModel)
			$('html,body').css('height','100%');

		// fix ie6 issue when blocked element has a border width
		if ((ie6 || !$.boxModel) && !full) {
			var t = sz(el,'borderTopWidth'), l = sz(el,'borderLeftWidth');
			var fixT = t ? '(0 - '+t+')' : 0;
			var fixL = l ? '(0 - '+l+')' : 0;
		}

		// simulate fixed position
		$.each([lyr1,lyr2,lyr3], function(i,o) {
			var s = o[0].style;
			s.position = 'absolute';
			if (i < 2) {
				full ? s.setExpression('height','Math.max(document.body.scrollHeight, document.body.offsetHeight) - (jQuery.boxModel?0:'+opts.quirksmodeOffsetHack+') + "px"')
					 : s.setExpression('height','this.parentNode.offsetHeight + "px"');
				full ? s.setExpression('width','jQuery.boxModel && document.documentElement.clientWidth || document.body.clientWidth + "px"')
					 : s.setExpression('width','this.parentNode.offsetWidth + "px"');
				if (fixL) s.setExpression('left', fixL);
				if (fixT) s.setExpression('top', fixT);
			}
			else if (opts.centerY) {
				if (full) s.setExpression('top','(document.documentElement.clientHeight || document.body.clientHeight) / 2 - (this.offsetHeight / 2) + (blah = document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + "px"');
				s.marginTop = 0;
			}
			else if (!opts.centerY && full) {
				var top = (opts.css && opts.css.top) ? parseInt(opts.css.top) : 0;
				var expression = '((document.documentElement.scrollTop ? document.documentElement.scrollTop : document.body.scrollTop) + '+top+') + "px"';
				s.setExpression('top',expression);
			}
		});
	}

	// show the message
	if (msg) {
		if (opts.theme)
			lyr3.find('.ui-widget-content').append(msg);
		else
			lyr3.append(msg);
		if (msg.jquery || msg.nodeType)
			$(msg).show();
	}

	if (($.browser.msie || opts.forceIframe) && opts.showOverlay)
		lyr1.show(); // opacity is zero
	if (opts.fadeIn) {
		var cb = opts.onBlock ? opts.onBlock : noOp;
		var cb1 = (opts.showOverlay && !msg) ? cb : noOp;
		var cb2 = msg ? cb : noOp;
		if (opts.showOverlay)
			lyr2._fadeIn(opts.fadeIn, cb1);
		if (msg)
			lyr3._fadeIn(opts.fadeIn, cb2);
	}
	else {
		if (opts.showOverlay)
			lyr2.show();
		if (msg)
			lyr3.show();
		if (opts.onBlock)
			opts.onBlock();
	}

	// bind key and mouse events
	bind(1, el, opts);

	if (full) {
		pageBlock = lyr3[0];
		pageBlockEls = $(':input:enabled:visible',pageBlock);
		if (opts.focusInput)
			setTimeout(focus, 20);
	}
	else
		center(lyr3[0], opts.centerX, opts.centerY);

	if (opts.timeout) {
		// auto-unblock
		var to = setTimeout(function() {
			full ? $.unblockUI(opts) : $(el).unblock(opts);
		}, opts.timeout);
		$(el).data('blockUI.timeout', to);
	}
};

// remove the block
function remove(el, opts) {
	var full = (el == window);
	var $el = $(el);
	var data = $el.data('blockUI.history');
	var to = $el.data('blockUI.timeout');
	if (to) {
		clearTimeout(to);
		$el.removeData('blockUI.timeout');
	}
	opts = $.extend({}, $.blockUI.defaults, opts || {});
	bind(0, el, opts); // unbind events
	
	var els;
	if (full) // crazy selector to handle odd field errors in ie6/7
		els = $('body').children().filter('.blockUI').add('body > .blockUI');
	else
		els = $('.blockUI', el);

	if (full)
		pageBlock = pageBlockEls = null;

	if (opts.fadeOut) {
		els.fadeOut(opts.fadeOut);
		setTimeout(function() { reset(els,data,opts,el); }, opts.fadeOut);
	}
	else
		reset(els, data, opts, el);
};

// move blocking element back into the DOM where it started
function reset(els,data,opts,el) {
	els.each(function(i,o) {
		// remove via DOM calls so we don't lose event handlers
		if (this.parentNode)
			this.parentNode.removeChild(this);
	});

	if (data && data.el) {
		data.el.style.display = data.display;
		data.el.style.position = data.position;
		if (data.parent)
			data.parent.appendChild(data.el);
		$(el).removeData('blockUI.history');
	}

	if (typeof opts.onUnblock == 'function')
		opts.onUnblock(el,opts);
};

// bind/unbind the handler
function bind(b, el, opts) {
	var full = el == window, $el = $(el);

	// don't bother unbinding if there is nothing to unbind
	if (!b && (full && !pageBlock || !full && !$el.data('blockUI.isBlocked')))
		return;
	if (!full)
		$el.data('blockUI.isBlocked', b);

	// don't bind events when overlay is not in use or if bindEvents is false
	if (!opts.bindEvents || (b && !opts.showOverlay)) 
		return;

	// bind anchors and inputs for mouse and key events
	var events = 'mousedown mouseup keydown keypress';
	b ? $(document).bind(events, opts, handler) : $(document).unbind(events, handler);

// former impl...
//	   var $e = $('a,:input');
//	   b ? $e.bind(events, opts, handler) : $e.unbind(events, handler);
};

// event handler to suppress keyboard/mouse events when blocking
function handler(e) {
	// allow tab navigation (conditionally)
	if (e.keyCode && e.keyCode == 9) {
		if (pageBlock && e.data.constrainTabKey) {
			var els = pageBlockEls;
			var fwd = !e.shiftKey && e.target == els[els.length-1];
			var back = e.shiftKey && e.target == els[0];
			if (fwd || back) {
				setTimeout(function(){focus(back)},10);
				return false;
			}
		}
	}
	// allow events within the message content
	if ($(e.target).parents('div.blockMsg').length > 0)
		return true;

	// allow events for content that is not being blocked
	return $(e.target).parents().children().filter('div.blockUI').length == 0;
};

function focus(back) {
	if (!pageBlockEls)
		return;
	var e = pageBlockEls[back===true ? pageBlockEls.length-1 : 0];
	if (e)
		e.focus();
};

function center(el, x, y) {
	var p = el.parentNode, s = el.style;
	var l = ((p.offsetWidth - el.offsetWidth)/2) - sz(p,'borderLeftWidth');
	var t = ((p.offsetHeight - el.offsetHeight)/2) - sz(p,'borderTopWidth');
	if (x) s.left = l > 0 ? (l+'px') : '0';
	if (y) s.top  = t > 0 ? (t+'px') : '0';
};

function sz(el, p) {
	return parseInt($.css(el,p))||0;
};

})(jQuery);

$.curCSS=$.css;$.fn.curCSS=$.fn.css;/*
  jquery.carousel
  made by Robin Duckett in 2009
  http://twitter.com/robinduckett
  You are free to use, modify or distribute this code as long as it retains this header.
*/
(function($) {
    $.fn.carousel = function(options) {

        var defaults = {
            start: 0,
            duration: 10000,
            hide: 'fadeOut',
            show: 'fadeIn',
            speed: 'slow',
            seed: 5,
            timer: 0,
            animate: true,
            slideshow: true,
			switchEvent:null, // Event to be triggered when sliding is startet
			pauseEvent:null, // Event that will be fired if pause is requested
			playEvent:null // Event that will be fired to continue the carousel
        };
		var run = true;

		var slideTimer = null;

		function _rand(lbound, ubound) {
            return (Math.floor(Math.random() * (ubound - lbound)) + lbound);
        }

        function _generateId(seed) {
            var randoChar = 'abcdefghijklmnopqrstuvwxyz0123456789';
            var sSeed = '';
            for (i = 0; i < seed; i++) {
                sSeed += randoChar.charAt(_rand(0, randoChar.length - 1));
            }
            return sSeed;
        }
		
		function clearSlideTimeout() {
			clearTimeout(slideTimer);
			slideTimer = null;
		}

        function _init() {
			if(options.pauseEvent !== null) {
				$(element).bind(options.pauseEvent, function() {
					run = false;
					clearSlideTimeout();
				});
			}
			if(options.playEvent !== null) {
				$(element).bind(options.playEvent, function() {
					run = true;
					_startTimer();
				});
			}

            $(element).children('li').each(function(index) {
            	if (options.slideshow) {
            		$(this)[index == slide ? options.show : 'hide'](index == slide ? options.speed : '').addClass(selector + index).addClass('slide')[index == slide ? 'addClass': 'removeClass']('active');
            	} else {
            		$(this).addClass(selector + index).addClass('slide')[index == slide ? 'addClass': 'removeClass']('active');
            	}
              slides = index;
            });

			function _startTimer() {
				if(run == true) {
					if(options.switchEvent !== null) {
						$(element).trigger(options.switchEvent, [slide]);
					}
					clearSlideTimeout();
					slideTimer = setTimeout(_runFade, options.duration);
				}
			}

			function _runFade() {
				$(element).children('li.' + selector + slide)[options.hide](options.speed,
	                function() {
	                	$(this).removeClass('active');
	                  slide = slide == slides ? 0: slide + 1;
					  // Zur Sicherheit noch mal alle verstecken und den Timer resetten
					  clearSlideTimeout();
					  $(element).children('li.' + selector).hide();
					  // den nächsten Slide anzeigen.
	                  $(element).children('li.' + selector + slide).addClass('active');
	                  $(element).children('li.' + selector + slide)[options.show](options.speed,
	                  _startTimer() );
                });
			}

            // total time = 2*D + 2*S;
			_startTimer();
        }

        options = jQuery.extend(defaults, options);
        var element = this;
        var slides;
        var slide = options.start;
        var selector = _generateId(options.seed);
        $(element).load(_init());
    };
})(jQuery);/*
	VERSION: Drop Shadow jQuery Plugin 1.6  12-13-2007

	REQUIRES: jquery.js (1.2.6 or later)

	SYNTAX: $(selector).dropShadow(options);  // Creates new drop shadows
					$(selector).redrawShadow();       // Redraws shadows on elements
					$(selector).removeShadow();       // Removes shadows from elements
					$(selector).shadowId();           // Returns an existing shadow's ID

	OPTIONS:

		left    : integer (default = 4)
		top     : integer (default = 4)
		blur    : integer (default = 2)
		opacity : decimal (default = 0.5)
		color   : string (default = "black")
		swap    : boolean (default = false)

	The left and top parameters specify the distance and direction, in	pixels, to
	offset the shadow. Zero values position the shadow directly behind the element.
	Positive values shift the shadow to the right and down, while negative values 
	shift the shadow to the left and up.
	
	The blur parameter specifies the spread, or dispersion, of the shadow. Zero 
	produces a sharp shadow, one or two produces a normal shadow, and	three or four
	produces a softer shadow. Higher values increase the processing load.
	
	The opacity parameter	should be a decimal value, usually less than one. You can
	use a value	higher than one in special situations, e.g. with extreme blurring. 
	
	Color is specified in the usual manner, with a color name or hex value. The
	color parameter	does not apply with transparent images.
	
	The swap parameter reverses the stacking order of the original and the shadow.
	This can be used for special effects, like an embossed or engraved look.

	EXPLANATION:
	
	This jQuery plug-in adds soft drop shadows behind page elements. It is only
	intended for adding a few drop shadows to mostly stationary objects, like a
	page heading, a photo, or content containers.

	The shadows it creates are not bound to the original elements, so they won't
	move or change size automatically if the original elements change. A window
	resize event listener is assigned, which should re-align the shadows in many
	cases, but if the elements otherwise move or resize you will have to handle
	those events manually. Shadows can be redrawn with the redrawShadow() method
	or removed with the removeShadow() method. The redrawShadow() method uses the
	same options used to create the original shadow. If you want to change the
	options, you should remove the shadow first and then create a new shadow.
	
	The dropShadow method returns a jQuery collection of the new shadow(s). If
	further manipulation is required, you can store it in a variable like this:

		var myShadow = $("#myElement").dropShadow();

	You can also read the ID of the shadow from the original element at a later
	time. To get a shadow's ID, either read the shadowId attribute of the
	original element or call the shadowId() method. For example:

		var myShadowId = $("#myElement").attr("shadowId");  or
		var myShadowId = $("#myElement").shadowId();

	If the original element does not already have an ID assigned, a random ID will
	be generated for the shadow. However, if the original does have an ID, the 
	shadow's ID will be the original ID and "_dropShadow". For example, if the
	element's ID is "myElement", the shadow's ID would be "myElement_dropShadow".

	If you have a long piece of text and the user resizes the	window so that the
	text wraps or unwraps, the shape of the text changes and the words are no
	longer in the same positions. In that case, you can either preset the height
	and width, so that it becomes a fixed box, or you can shadow each word
	separately, like this:

		<h1><span>Your</span> <span>Page</span> <span>Title</span></h1>

		$("h1 span").dropShadow();

	The dropShadow method attempts to determine whether the selected elements have
	transparent backgrounds. If you want to shadow the content inside an element,
	like text or a transparent image, it must not have a background-color or
	background-image style. If the element has a solid background it will create a
	rectangular	shadow around the outside box.

	The shadow elements are positioned absolutely one layer below the original 
	element, which is positioned relatively (unless it's already absolute).

	*** All shadows have the "dropShadow" class, for selecting with CSS or jQuery.

	ISSUES:
	
		1)	Limited styling of shadowed elements by ID. Because IDs must be unique,
				and the shadows have their own ID, styles applied by ID won't transfer
				to the shadows. Instead, style elements by class or use inline styles.
		2)	Sometimes shadows don't align properly. Elements may need to be wrapped
				in container elements, margins or floats changed, etc. or you may just 
				have to tweak the left and top offsets to get them to align. For example,
				with draggable objects, you have to wrap them inside two divs. Make the 
				outer div draggable and set the inner div's position to relative. Then 
				you can create a shadow on the element inside the inner div.
		3)	If the user changes font sizes it will throw the shadows off. Browsers 
				do not expose an event for font size changes. The only known way to 
				detect a user font size change is to embed an invisible text element and
				then continuously poll for changes in size.
		4)	Safari support is shaky, and may require even more tweaks/wrappers, etc.
		
		The bottom line is that this is a gimick effect, not PFM, and if you push it
		too hard or expect it to work in every possible situation on every browser,
		you will be disappointed. Use it sparingly, and don't use it for anything 
		critical. Otherwise, have fun with it!
				
	AUTHOR: Larry Stevens (McLars@eyebulb.com) This work is in the public domain,
					and it is not supported in any way. Use it at your own risk.
*/


	var shadowOptionsStandard = {left:-3, top:3, opacity:0.3};
	var shadowOptionsNavigation = {left:1, top:2, opacity:0.3};
	var shadowOptionsDragAndDrop = {left:1, top:0, opacity:0.4, blur: 7};

(function($){

	var dropShadowZindex = 100;  //z-index counter

	$.fn.dropShadow = function(options)
	{
		// Default options
		var opt = $.extend({
			left: 4,
			top: 4,
			blur: 2,
			opacity: .5,
			color: "black",
			swap: false
			}, options);
		var jShadows = $([]);  //empty jQuery collection
		
		// Loop through original elements
		this.not(".dropShadow").each(function()
		{
			var jthis = $(this);
			var shadows = [];
			var blur = (opt.blur <= 0) ? 0 : opt.blur;
			var opacity = (blur == 0) ? opt.opacity : opt.opacity / (blur * 8);
			var zOriginal = (opt.swap) ? dropShadowZindex : dropShadowZindex + 1;
			var zShadow = (opt.swap) ? dropShadowZindex + 1 : dropShadowZindex;
			
			// Create ID for shadow
			var shadowId;
			if (this.id) {
				shadowId = this.id + "_dropShadow";
			}
			else {
				shadowId = "ds" + (1 + Math.floor(9999 * Math.random()));
			}

			// Modify original element
			$.data(this, "shadowId", shadowId); //store id in expando
			$.data(this, "shadowOptions", options); //store options in expando
			jthis
				.attr("shadowId", shadowId)
				.css("zIndex", zOriginal);
			if (jthis.css("position") != "absolute") {
				jthis.css({
					position: "relative",
					zoom: 1 //for IE layout
				});
			}

			// Create first shadow layer
			bgColor = jthis.css("backgroundColor");
			if (bgColor == "rgba(0, 0, 0, 0)") bgColor = "transparent";  //Safari
			if (bgColor != "transparent" || jthis.css("backgroundImage") != "none" 
					|| this.nodeName == "SELECT" 
					|| this.nodeName == "INPUT"
					|| this.nodeName == "TEXTAREA") {		
				shadows[0] = $("<div></div>")
					.css("background", opt.color);								
			}
			else {
				shadows[0] = jthis
					.clone()
					.removeAttr("id")
					.removeAttr("name")
					.removeAttr("shadowId")
					.css("color", opt.color);
			}
			shadows[0]
				.addClass("dropShadow")
				.css({
					height: jthis.outerHeight(),
					left: blur,
					opacity: opacity,
					position: "absolute",
					top: blur,
					width: jthis.outerWidth(),
					zIndex: zShadow
				});
				
			// Create other shadow layers
			var layers = (8 * blur) + 1;
			for (i = 1; i < layers; i++) {
				shadows[i] = shadows[0].clone();
			}

			// Position layers
			var i = 1;			
			var j = blur;
			while (j > 0) {
				shadows[i].css({left: j * 2, top: 0});           //top
				shadows[i + 1].css({left: j * 4, top: j * 2});   //right
				shadows[i + 2].css({left: j * 2, top: j * 4});   //bottom
				shadows[i + 3].css({left: 0, top: j * 2});       //left
				shadows[i + 4].css({left: j * 3, top: j});       //top-right
				shadows[i + 5].css({left: j * 3, top: j * 3});   //bottom-right
				shadows[i + 6].css({left: j, top: j * 3});       //bottom-left
				shadows[i + 7].css({left: j, top: j});           //top-left
				i += 8;
				j--;
			}

			// Create container
			var divShadow = $("<div></div>")
				.attr("id", shadowId) 
				.addClass("dropShadow")
				.css({
					left: jthis.position().left + opt.left - blur,
					marginTop: jthis.css("marginTop"),
					marginRight: jthis.css("marginRight"),
					marginBottom: jthis.css("marginBottom"),
					marginLeft: jthis.css("marginLeft"),
					position: "absolute",
					top: jthis.position().top + opt.top - blur,
					zIndex: zShadow
				});

			// Add layers to container	
			for (i = 0; i < layers; i++) {
				divShadow.append(shadows[i]);
			}
			
			// Add container to DOM
			jthis.after(divShadow);

			// Add shadow to return set
			jShadows = jShadows.add(divShadow);

			// Re-align shadow on window resize
			$(window).resize(function()
			{
				try {
					divShadow.css({
						left: jthis.position().left + opt.left - blur,
						top: jthis.position().top + opt.top - blur
					});
				}
				catch(e){}
			});
			
			// Increment z-index counter
			dropShadowZindex += 2;

		});  //end each
		
		return this.pushStack(jShadows);
	};


	$.fn.redrawShadow = function()
	{
		// Remove existing shadows
		this.removeShadow();
		
		// Draw new shadows
		return this.each(function()
		{
			var shadowOptions = $.data(this, "shadowOptions");
			$(this).dropShadow(shadowOptions);
		});
	};


	$.fn.removeShadow = function()
	{
		return this.each(function()
		{
			var shadowId = $(this).shadowId();
			$("div#" + shadowId).remove();
		});
	};


	$.fn.shadowId = function()
	{
		return $.data(this[0], "shadowId");
	};


	$(function()  
	{
		// Suppress printing of shadows
		var noPrint = "<style type='text/css' media='print'>";
		noPrint += ".dropShadow{visibility:hidden;}</style>";
		$("head").append(noPrint);
	});

})(jQuery);/* 
jquery.event.drag.js ~ v1.5 ~ Copyright (c) 2008, Three Dub Media (http://threedubmedia.com)  
Liscensed under the MIT License ~ http://threedubmedia.googlecode.com/files/MIT-LICENSE.txt
*/
(function(E){E.fn.drag=function(L,K,J){if(K){this.bind("dragstart",L)}if(J){this.bind("dragend",J)}return !L?this.trigger("drag"):this.bind("drag",K?K:L)};var A=E.event,B=A.special,F=B.drag={not:":input",distance:0,which:1,dragging:false,setup:function(J){J=E.extend({distance:F.distance,which:F.which,not:F.not},J||{});J.distance=I(J.distance);A.add(this,"mousedown",H,J);if(this.attachEvent){this.attachEvent("ondragstart",D)}},teardown:function(){A.remove(this,"mousedown",H);if(this===F.dragging){F.dragging=F.proxy=false}G(this,true);if(this.detachEvent){this.detachEvent("ondragstart",D)}}};B.dragstart=B.dragend={setup:function(){},teardown:function(){}};function H(L){var K=this,J,M=L.data||{};if(M.elem){K=L.dragTarget=M.elem;L.dragProxy=F.proxy||K;L.cursorOffsetX=M.pageX-M.left;L.cursorOffsetY=M.pageY-M.top;L.offsetX=L.pageX-L.cursorOffsetX;L.offsetY=L.pageY-L.cursorOffsetY}else{if(F.dragging||(M.which>0&&L.which!=M.which)||E(L.target).is(M.not)){return }}switch(L.type){case"mousedown":E.extend(M,E(K).offset(),{elem:K,target:L.target,pageX:L.pageX,pageY:L.pageY});A.add(document,"mousemove mouseup",H,M);G(K,false);F.dragging=null;return false;case !F.dragging&&"mousemove":if(I(L.pageX-M.pageX)+I(L.pageY-M.pageY)<M.distance){break}L.target=M.target;J=C(L,"dragstart",K);if(J!==false){F.dragging=K;F.proxy=L.dragProxy=E(J||K)[0]}case"mousemove":if(F.dragging){J=C(L,"drag",K);if(B.drop){B.drop.allowed=(J!==false);B.drop.handler(L)}if(J!==false){break}L.type="mouseup"}case"mouseup":A.remove(document,"mousemove mouseup",H);if(F.dragging){if(B.drop){B.drop.handler(L)}C(L,"dragend",K)}G(K,true);F.dragging=F.proxy=M.elem=false;break}return true}function C(M,K,L){M.type=K;var J=E.event.handle.call(L,M);return J===false?false:J||M.result}function I(J){return Math.pow(J,2)}function D(){return(F.dragging===false)}function G(K,J){if(!K){return }K.unselectable=J?"off":"on";K.onselectstart=function(){return J};if(K.style){K.style.MozUserSelect=J?"":"none"}}})(jQuery);/* jquery.event.drop.js * v1.2
Copyright (c) 2008-2009, Three Dub Media (http://threedubmedia.com)  
Liscensed under the MIT License (http://threedubmedia.googlecode.com/files/MIT-LICENSE.txt)
*/
(function(F){F.fn.drop=function(I,H,G){if(H){this.bind("dropstart",I)}if(G){this.bind("dropend",G)}return !I?this.trigger("drop"):this.bind("drop",H?H:I)};F.dropManage=function(G){G=G||{};C.data=[];C.filter=G.filter||"*";C.delay=G.delay||C.delay;C.tolerance=G.tolerance||null;C.mode=G.mode||C.mode||"intersect";return C.$targets.filter(C.filter).each(function(){C.data[C.data.length]=C.locate(this)})};var D=F.event,B=D.special,C=B.drop={delay:100,mode:"intersect",$targets:F([]),data:[],setup:function(){C.$targets=C.$targets.add(this);C.data[C.data.length]=C.locate(this)},teardown:function(){var G=this;C.$targets=C.$targets.not(this);C.data=F.grep(C.data,function(H){return(H.elem!==G)})},handler:function(H){var G=null,I;H.dropTarget=C.dropping||undefined;if(C.data.length&&H.dragTarget){switch(H.type){case"drag":C.event=H;if(!C.timer){C.timer=setTimeout(E,20)}break;case"mouseup":C.timer=clearTimeout(C.timer);if(!C.dropping){break}if(C.allowed){I=A(H,"drop",C.dropping)}G=false;case C.dropping&&"dropstart":G=G===null&&C.allowed?true:false;case C.dropping&&"dropend":A(H,"dropend",C.dropping);C.dropping=null;if(I===false){H.dropTarget=undefined}if(!G){break}case C.allowed&&"dropstart":H.dropTarget=this;C.dropping=A(H,"dropstart",this)!==false?this:null;break}}},locate:function(J){var H=F(J),K=H.offset(),I=H.outerHeight(),G=H.outerWidth();return{elem:J,L:K.left,R:K.left+G,T:K.top,B:K.top+I,W:G,H:I}},contains:function(G,H){return((H[0]||H.L)>=G.L&&(H[0]||H.R)<=G.R&&(H[1]||H.T)>=G.T&&(H[1]||H.B)<=G.B)},modes:{intersect:function(H,G,I){return this.contains(I,[H.pageX,H.pageY])?I:this.modes.overlap.apply(this,arguments)},overlap:function(H,G,I){I.overlap=Math.max(0,Math.min(I.B,G.B)-Math.max(I.T,G.T))*Math.max(0,Math.min(I.R,G.R)-Math.max(I.L,G.L));if(I.overlap>((this.best||{}).overlap||0)){this.best=I}return null},fit:function(H,G,I){return this.contains(I,G)?I:null},middle:function(H,G,I){return this.contains(I,[G.L+G.W/2,G.T+G.H/2])?I:null}}};function A(K,I,J){K.type=I;try{var G=D.handle.call(J,K)}catch(H){}return G===false?false:G||K.result}function E(){var H=0,G,I,J=[C.event.pageX,C.event.pageY],K=C.locate(C.event.dragProxy);C.tolerance=C.tolerance||C.modes[C.mode];do{if(G=C.data[H]){I=C.tolerance?C.tolerance.call(C,C.event,K,G):C.contains(G,J)?G:null}}while(++H<C.data.length&&!I);C.event.type=(I=I||C.best)?"dropstart":"dropend";if(C.event.type=="dropend"||I.elem!=C.dropping){C.handler.call(I?I.elem:C.dropping,C.event)}if(C.last&&J[0]==C.last.pageX&&J[1]==C.last.pageY){delete C.timer}else{C.timer=setTimeout(E,C.delay)}C.last=C.event;C.best=null}})(jQuery);/*! Copyright (c) 2011 Brandon Aaron (http://brandonaaron.net)
 * Licensed under the MIT License (LICENSE.txt).
 *
 * Thanks to: http://adomas.org/javascript-mouse-wheel/ for some pointers.
 * Thanks to: Mathias Bank(http://www.mathias-bank.de) for a scope bug fix.
 * Thanks to: Seamus Leahy for adding deltaX and deltaY
 *
 * Version: 3.0.6
 * 
 * Requires: 1.2.2+
 */
(function(e){function r(t){var n=t||window.event,r=[].slice.call(arguments,1),i=0,s=true,o=0,u=0;t=e.event.fix(n);t.type="mousewheel";if(n.wheelDelta){i=n.wheelDelta/120}if(n.detail){i=-n.detail/3}u=i;if(n.axis!==undefined&&n.axis===n.HORIZONTAL_AXIS){u=0;o=-1*i}if(n.wheelDeltaY!==undefined){u=n.wheelDeltaY/120}if(n.wheelDeltaX!==undefined){o=-1*n.wheelDeltaX/120}r.unshift(t,i,o,u);return(e.event.dispatch||e.event.handle).apply(this,r)}var t=["DOMMouseScroll","mousewheel"];if(e.event.fixHooks){for(var n=t.length;n;){e.event.fixHooks[t[--n]]=e.event.mouseHooks}}e.event.special.mousewheel={setup:function(){if(this.addEventListener){for(var e=t.length;e;){this.addEventListener(t[--e],r,false)}}else{this.onmousewheel=r}},teardown:function(){if(this.removeEventListener){for(var e=t.length;e;){this.removeEventListener(t[--e],r,false)}}else{this.onmousewheel=null}}};e.fn.extend({mousewheel:function(e){return e?this.bind("mousewheel",e):this.trigger("mousewheel")},unmousewheel:function(e){return this.unbind("mousewheel",e)}})})(jQuery);
jQuery.preloadImages=function()
{for(var i=0;i<arguments.length;i++)
{jQuery("<img>").attr("src",arguments[i]);}};/*
 * Treeview 1.4 - jQuery plugin to hide and show branches of a tree
 * 
 * http://bassistance.de/jquery-plugins/jquery-plugin-treeview/
 * http://docs.jquery.com/Plugins/Treeview
 *
 * Copyright (c) 2007 Jörn Zaefferer
 *
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 *
 * Revision: $Id: jquery.treeview.js 4684 2008-02-07 19:08:06Z joern.zaefferer $
 *
 */;(function($){$.extend($.fn,{swapClass:function(c1,c2){var c1Elements=this.filter('.'+c1);this.filter('.'+c2).removeClass(c2).addClass(c1);c1Elements.removeClass(c1).addClass(c2);return this;},replaceClass:function(c1,c2){return this.filter('.'+c1).removeClass(c1).addClass(c2).end();},hoverClass:function(className){className=className||"hover";return this.hover(function(){$(this).addClass(className);},function(){$(this).removeClass(className);});},heightToggle:function(animated,callback){animated?this.animate({height:"toggle"},animated,callback):this.each(function(){jQuery(this)[jQuery(this).is(":hidden")?"show":"hide"]();if(callback)callback.apply(this,arguments);});},heightHide:function(animated,callback){if(animated){this.animate({height:"hide"},animated,callback);}else{this.hide();if(callback)this.each(callback);}},prepareBranches:function(settings){if(!settings.prerendered){this.filter(":last-child:not(ul)").addClass(CLASSES.last);this.filter((settings.collapsed?"":"."+CLASSES.closed)+":not(."+CLASSES.open+")").find(">ul").hide();}return this.filter(":has(>ul)");},applyClasses:function(settings,toggler){this.filter(":has(>ul):not(:has(>a))").find(">span").click(function(event){toggler.apply($(this).next());}).add($("a",this)).hoverClass();if(!settings.prerendered){this.filter(":has(>ul:hidden)").addClass(CLASSES.expandable).replaceClass(CLASSES.last,CLASSES.lastExpandable);this.not(":has(>ul:hidden)").addClass(CLASSES.collapsable).replaceClass(CLASSES.last,CLASSES.lastCollapsable);this.prepend("<div class=\""+CLASSES.hitarea+"\"/>").find("div."+CLASSES.hitarea).each(function(){var classes="";$.each($(this).parent().attr("class").split(" "),function(){classes+=this+"-hitarea ";});$(this).addClass(classes);});}this.find("div."+CLASSES.hitarea).click(toggler);},treeview:function(settings){settings=$.extend({cookieId:"treeview"},settings);if(settings.add){return this.trigger("add",[settings.add]);}if(settings.toggle){var callback=settings.toggle;settings.toggle=function(){return callback.apply($(this).parent()[0],arguments);};}function treeController(tree,control){function handler(filter){return function(){toggler.apply($("div."+CLASSES.hitarea,tree).filter(function(){return filter?$(this).parent("."+filter).length:true;}));return false;};}$("a:eq(0)",control).click(handler(CLASSES.collapsable));$("a:eq(1)",control).click(handler(CLASSES.expandable));$("a:eq(2)",control).click(handler());}function toggler(){$(this).parent().find(">.hitarea").swapClass(CLASSES.collapsableHitarea,CLASSES.expandableHitarea).swapClass(CLASSES.lastCollapsableHitarea,CLASSES.lastExpandableHitarea).end().swapClass(CLASSES.collapsable,CLASSES.expandable).swapClass(CLASSES.lastCollapsable,CLASSES.lastExpandable).find(">ul").heightToggle(settings.animated,settings.toggle);if(settings.unique){$(this).parent().siblings().find(">.hitarea").replaceClass(CLASSES.collapsableHitarea,CLASSES.expandableHitarea).replaceClass(CLASSES.lastCollapsableHitarea,CLASSES.lastExpandableHitarea).end().replaceClass(CLASSES.collapsable,CLASSES.expandable).replaceClass(CLASSES.lastCollapsable,CLASSES.lastExpandable).find(">ul").heightHide(settings.animated,settings.toggle);}}function serialize(){function binary(arg){return arg?1:0;}var data=[];branches.each(function(i,e){data[i]=$(e).is(":has(>ul:visible)")?1:0;});$.cookie(settings.cookieId,data.join(""));}function deserialize(){var stored=$.cookie(settings.cookieId);if(stored){var data=stored.split("");branches.each(function(i,e){$(e).find(">ul")[parseInt(data[i])?"show":"hide"]();});}}this.addClass("treeview");var branches=this.find("li").prepareBranches(settings);switch(settings.persist){case"cookie":var toggleCallback=settings.toggle;settings.toggle=function(){serialize();if(toggleCallback){toggleCallback.apply(this,arguments);}};deserialize();break;case"location":var current=this.find("a").filter(function(){return this.href.toLowerCase()==location.href.toLowerCase();});if(current.length){current.addClass("selected").parents("ul, li").add(current.next()).show();}break;}branches.applyClasses(settings,toggler);if(settings.control){treeController(this,settings.control);$(settings.control).show();}return this.bind("add",function(event,branches){$(branches).prev().removeClass(CLASSES.last).removeClass(CLASSES.lastCollapsable).removeClass(CLASSES.lastExpandable).find(">.hitarea").removeClass(CLASSES.lastCollapsableHitarea).removeClass(CLASSES.lastExpandableHitarea);$(branches).find("li").andSelf().prepareBranches(settings).applyClasses(settings,toggler);});}});var CLASSES=$.fn.treeview.classes={open:"open",closed:"closed",expandable:"expandable",expandableHitarea:"expandable-hitarea",lastExpandableHitarea:"lastExpandable-hitarea",collapsable:"collapsable",collapsableHitarea:"collapsable-hitarea",lastCollapsableHitarea:"lastCollapsable-hitarea",lastCollapsable:"lastCollapsable",lastExpandable:"lastExpandable",last:"last",hitarea:"hitarea"};$.fn.Treeview=$.fn.treeview;})(jQuery);/*-------------------------------------------------------------------- 
 * JQuery Plugin: "EqualHeights" & "EqualWidths"
 * by:	Scott Jehl, Todd Parker, Maggie Costello Wachs (http://www.filamentgroup.com)
 *
 * Copyright (c) 2007 Filament Group
 * Licensed under GPL (http://www.opensource.org/licenses/gpl-license.php)
 *
 * Description: Compares the heights or widths of the top-level children of a provided element 
 		and sets their min-height to the tallest height (or width to widest width). Sets in em units 
 		by default if pxToEm() method is available.
 * Dependencies: jQuery library, pxToEm method	(article: http://www.filamentgroup.com/lab/retaining_scalable_interfaces_with_pixel_to_em_conversion/)							  
 * Usage Example: $(element).equalHeights();
   						      Optional: to set min-height in px, pass a true argument: $(element).equalHeights(true);
 * Version: 2.0, 07.24.2008
 * Changelog:
 *  08.02.2007 initial Version 1.0
 *  07.24.2008 v 2.0 - added support for widths
--------------------------------------------------------------------*/

$.fn.equalHeights = function(px) {
	$(this).each(function(){
		var currentTallest = 0;
		$(this).children().each(function(i){
			if ($(this).height() > currentTallest) { currentTallest = $(this).height(); }
		});
		if (!px || !Number.prototype.pxToEm) currentTallest = currentTallest.pxToEm(); //use ems unless px is specified
		// for ie6, set height since min-height isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'height': currentTallest}); }
		$(this).children().css({'min-height': currentTallest}); 
	});
	return this;
};
 /* Compares the equal height of odd and even children */
$.fn.equalHeightsOddEven = function(px) {
	$(this).each(function(){
		var currentTallestEven = 0;
		var currentTallestOdd = 0;
		// var currentTallest = 0;
		$(this).children('li').each(function(i){
			if ($(this).hasClass('even')) {
				if ($(this).height() > currentTallestEven) { currentTallestEven = $(this).height(); }
			} else {
				// assume that this has class odd
				if ($(this).height() > currentTallestOdd) { currentTallestOdd = $(this).height(); }
			}
		});
		if (!px || !Number.prototype.pxToEm) currentTallestEven = currentTallestEven.pxToEm(); //use ems unless px is specified
		if (!px || !Number.prototype.pxToEm) currentTallestOdd = currentTallestOdd.pxToEm(); //use ems unless px is specified
		$(this).children().each(function(i){
			if ($(this).hasClass('even')) {
				if ($.browser.msie && $.browser.version == 6.0) {
					// for ie6, set height since min-height isn't supported
					$(this).css({'height': currentTallestEven});
				} else {
					$(this).css({'min-height': currentTallestEven});
				}
			} else {
				if ($.browser.msie && $.browser.version == 6.0) {
					$(this).css({'height': currentTallestOdd});
				} else {
					$(this).css({'min-height': currentTallestOdd});
				}
			}
		});
	});
	return this;
};

// just in case you need it...
$.fn.equalWidths = function(px) {
	$(this).each(function(){
		var currentWidest = 0;
		$(this).children().each(function(i){
				if($(this).width() > currentWidest) { currentWidest = $(this).width(); }
		});
		if(!px || !Number.prototype.pxToEm) currentWidest = currentWidest.pxToEm(); //use ems unless px is specified
		// for ie6, set width since min-width isn't supported
		if ($.browser.msie && $.browser.version == 6.0) { $(this).children().css({'width': currentWidest}); }
		$(this).children().css({'min-width': currentWidest}); 
	});
	return this;
};

equalHeightsOf2Portlets = function() {
	if ($(".disciplineList").parents('.colLeft').length > 0 && $(".newsList").parents('.colLeft').length > 0) {
		// disciplineList and newsList are both in colLeft; now check that they are not in the same column (#8387)
		var orderDisciplineNewslist = "none";
		
		if ($(".disciplineList").parents('.col1').length > 0 && $(".newsList").parents('.col2').length > 0) {
			orderDisciplineNewslist = "dispNews";
		} else if($(".disciplineList").parents('.col2').length > 0 && $(".newsList").parents('.col1').length > 0) {
			orderDisciplineNewslist = "newsDisp";
		}
		if(orderDisciplineNewslist != "none") {
			var heightOfPortlet = 0;
			var disciplinePortlet; 
			var newsPortlet;
			if(orderDisciplineNewslist == "dispNews") {
				disciplinePortlet = $($(".col1").find(".disciplineList").parents('.portlet')[0]);
				newsPortlet = $($(".col2").find(".newsList").parents('.portlet')[0]);
			} else {
				disciplinePortlet = $($(".col2").find(".disciplineList").parents('.portlet')[0]);
				newsPortlet = $($(".col1").find(".newsList").parents('.portlet')[0]);
			}
			if (disciplinePortlet.height() > newsPortlet.height()) {
				heightOfPortlet = disciplinePortlet.height();
			} else {
				heightOfPortlet = newsPortlet.height();
			}
			disciplinePortlet.css({'min-height': heightOfPortlet});
			newsPortlet.css({'min-height': heightOfPortlet});
			newsPortlet.children().css({'min-height': heightOfPortlet, 'position': 'relative'});
			newsPortlet.find(".portletFooter").addClass('portletFooterEqualHeight');
		}
	}
	return this;
};

/*-------------------------------------------------------------------- 
 * javascript method: "pxToEm"
 * by:
   Scott Jehl (scott@filamentgroup.com) 
   Maggie Wachs (maggie@filamentgroup.com)
   http://www.filamentgroup.com
 *
 * Copyright (c) 2008 Filament Group
 * Dual licensed under the MIT (filamentgroup.com/examples/mit-license.txt) and GPL (filamentgroup.com/examples/gpl-license.txt) licenses.
 *
 * Description: Extends the native Number and String objects with pxToEm method. pxToEm converts a pixel value to ems depending on inherited font size.  
 * Article: http://www.filamentgroup.com/lab/retaining_scalable_interfaces_with_pixel_to_em_conversion/
 * Demo: http://www.filamentgroup.com/examples/pxToEm/	 	
 *							
 * Options:  	 								
 		scope: string or jQuery selector for font-size scoping
 		reverse: Boolean, true reverses the conversion to em-px
 * Dependencies: jQuery library						  
 * Usage Example: myPixelValue.pxToEm(); or myPixelValue.pxToEm({'scope':'#navigation', reverse: true});
 *
 * Version: 2.0, 08.01.2008 
 * Changelog:
 *		08.02.2007 initial Version 1.0
 *		08.01.2008 - fixed font-size calculation for IE
--------------------------------------------------------------------*/

Number.prototype.pxToEm = String.prototype.pxToEm = function(settings){
	//set defaults
	settings = jQuery.extend({
		scope: 'body',
		reverse: false
	}, settings);
	
	var pxVal = (this == '') ? 0 : parseFloat(this);
	var scopeVal;
	var getWindowWidth = function(){
		var de = document.documentElement;
		return self.innerWidth || (de && de.clientWidth) || document.body.clientWidth;
	};	
	
	/* When a percentage-based font-size is set on the body, IE returns that percent of the window width as the font-size. 
		For example, if the body font-size is 62.5% and the window width is 1000px, IE will return 625px as the font-size. 	
		When this happens, we calculate the correct body font-size (%) and multiply it by 16 (the standard browser font size) 
		to get an accurate em value. */
				
	if (settings.scope == 'body' && $.browser.msie && (parseFloat($('body').css('font-size')) / getWindowWidth()).toFixed(1) > 0.0) {
		var calcFontSize = function(){		
			return (parseFloat($('body').css('font-size'))/getWindowWidth()).toFixed(3) * 16;
		};
		scopeVal = calcFontSize();
	}
	else { scopeVal = parseFloat(jQuery(settings.scope).css("font-size")); };
			
	var result = (settings.reverse == true) ? (pxVal * scopeVal).toFixed(2) + 'px' : (pxVal / scopeVal).toFixed(2) + 'em';
	return result;
};
	
	/**
	 * jQuery SHA1 hash algorithm function
	 * 
	 * 	<code>
	 * 		Calculate the sha1 hash of a String 
	 * 		String $.sha1 ( String str )
	 * 	</code>
	 * 
	 * Calculates the sha1 hash of str using the US Secure Hash Algorithm 1.
	 * SHA-1 the Secure Hash Algorithm (SHA) was developed by NIST and is specified in the Secure Hash Standard (SHS, FIPS 180).
	 * This script is used to process variable length message into a fixed-length output using the SHA-1 algorithm. It is fully compatible with UTF-8 encoding.
	 * If you plan using UTF-8 encoding in your project don't forget to set the page encoding to UTF-8 (Content-Type meta tag).
	 * This function orginally get from the WebToolkit and rewrite for using as the jQuery plugin.
	 * 
	 * Example
	 * 	Code
	 * 		<code>
	 * 			$.sha1("I'm Persian."); 
	 * 		</code>
	 * 	Result
	 * 		<code>
	 * 			"1d302f9dc925d62fc859055999d2052e274513ed"
	 * 		</code>
	 * 
	 * @alias Muhammad Hussein Fattahizadeh < muhammad [AT] semnanweb [DOT] com >
	 * @link http://www.semnanweb.com/jquery-plugin/sha1.html
	 * @see http://www.webtoolkit.info/
	 * @license http://www.gnu.org/licenses/gpl.html [GNU General Public License]
	 * @param {jQuery} {sha1:function(string))
	 * @return string
	 */
	
	(function($){
		
		var rotateLeft = function(lValue, iShiftBits) {
			return (lValue << iShiftBits) | (lValue >>> (32 - iShiftBits));
		}
		
		var lsbHex = function(value) {
			var string = "";
			var i;
			var vh;
			var vl;
			for(i = 0;i <= 6;i += 2) {
				vh = (value>>>(i * 4 + 4))&0x0f;
				vl = (value>>>(i*4))&0x0f;
				string += vh.toString(16) + vl.toString(16);
			}
			return string;
		};
		
		var cvtHex = function(value) {
			var string = "";
			var i;
			var v;
			for(i = 7;i >= 0;i--) {
				v = (value>>>(i * 4))&0x0f;
				string += v.toString(16);
			}
			return string;
		};
		
		var uTF8Encode = function(string) {
			string = string.replace(/\x0d\x0a/g, "\x0a");
			var output = "";
			for (var n = 0; n < string.length; n++) {
				var c = string.charCodeAt(n);
				if (c < 128) {
					output += String.fromCharCode(c);
				} else if ((c > 127) && (c < 2048)) {
					output += String.fromCharCode((c >> 6) | 192);
					output += String.fromCharCode((c & 63) | 128);
				} else {
					output += String.fromCharCode((c >> 12) | 224);
					output += String.fromCharCode(((c >> 6) & 63) | 128);
					output += String.fromCharCode((c & 63) | 128);
				}
			}
			return output;
		};
		
		$.extend({
			sha1: function(string) {
				var blockstart;
				var i, j;
				var W = new Array(80);
				var H0 = 0x67452301;
				var H1 = 0xEFCDAB89;
				var H2 = 0x98BADCFE;
				var H3 = 0x10325476;
				var H4 = 0xC3D2E1F0;
				var A, B, C, D, E;
				var tempValue;
				string = uTF8Encode(string);
				var stringLength = string.length;
				var wordArray = new Array();
				for(i = 0;i < stringLength - 3;i += 4) {
					j = string.charCodeAt(i)<<24 | string.charCodeAt(i + 1)<<16 | string.charCodeAt(i + 2)<<8 | string.charCodeAt(i + 3);
					wordArray.push(j);
				}
				switch(stringLength % 4) {
					case 0:
						i = 0x080000000;
					break;
					case 1:
						i = string.charCodeAt(stringLength - 1)<<24 | 0x0800000;
					break;
					case 2:
						i = string.charCodeAt(stringLength - 2)<<24 | string.charCodeAt(stringLength - 1)<<16 | 0x08000;
					break;
					case 3:
						i = string.charCodeAt(stringLength - 3)<<24 | string.charCodeAt(stringLength - 2)<<16 | string.charCodeAt(stringLength - 1)<<8 | 0x80;
					break;
				}
				wordArray.push(i);
				while((wordArray.length % 16) != 14 ) wordArray.push(0);
				wordArray.push(stringLength>>>29);
				wordArray.push((stringLength<<3)&0x0ffffffff);
				for(blockstart = 0;blockstart < wordArray.length;blockstart += 16) {
					for(i = 0;i < 16;i++) W[i] = wordArray[blockstart+i];
					for(i = 16;i <= 79;i++) W[i] = rotateLeft(W[i-3] ^ W[i-8] ^ W[i-14] ^ W[i-16], 1);
					A = H0;
					B = H1;
					C = H2;
					D = H3;
					E = H4;
					for(i = 0;i <= 19;i++) {
						tempValue = (rotateLeft(A, 5) + ((B&C) | (~B&D)) + E + W[i] + 0x5A827999) & 0x0ffffffff;
						E = D;
						D = C;
						C = rotateLeft(B, 30);
						B = A;
						A = tempValue;
					}
					for(i = 20;i <= 39;i++) {
						tempValue = (rotateLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0x6ED9EBA1) & 0x0ffffffff;
						E = D;
						D = C;
						C = rotateLeft(B, 30);
						B = A;
						A = tempValue;
					}
					for(i = 40;i <= 59;i++) {
						tempValue = (rotateLeft(A, 5) + ((B&C) | (B&D) | (C&D)) + E + W[i] + 0x8F1BBCDC) & 0x0ffffffff;
						E = D;
						D = C;
						C = rotateLeft(B, 30);
						B = A;
						A = tempValue;
					}
					for(i = 60;i <= 79;i++) {
						tempValue = (rotateLeft(A, 5) + (B ^ C ^ D) + E + W[i] + 0xCA62C1D6) & 0x0ffffffff;
						E = D;
						D = C;
						C = rotateLeft(B, 30);
						B = A;
						A = tempValue;
					}
					H0 = (H0 + A) & 0x0ffffffff;
					H1 = (H1 + B) & 0x0ffffffff;
					H2 = (H2 + C) & 0x0ffffffff;
					H3 = (H3 + D) & 0x0ffffffff;
					H4 = (H4 + E) & 0x0ffffffff;
				}
				var tempValue = cvtHex(H0) + cvtHex(H1) + cvtHex(H2) + cvtHex(H3) + cvtHex(H4);
				return tempValue.toLowerCase();
			}
		});
	})(jQuery);/*
 * jqModal - Minimalist Modaling with jQuery
 *   (http://dev.iceburg.net/jquery/jqModal/)
 *
 * Copyright (c) 2007,2008 Brice Burgess <bhb@iceburg.net>
 * Dual licensed under the MIT and GPL licenses:
 *   http://www.opensource.org/licenses/mit-license.php
 *   http://www.gnu.org/licenses/gpl.html
 * 
 * $Version: 03/01/2009 +r14
 */
(function($) {
$.fn.jqm=function(o){
var p={
overlay: 50,
overlayClass: 'jqmOverlay',
closeClass: 'jqmClose',
trigger: '.jqModal',
ajax: F,
ajaxText: '',
target: F,
modal: F,
toTop: F,
onShow: F,
onHide: F,
onLoad: F
};
return this.each(function(){if(this._jqm)return H[this._jqm].c=$.extend({},H[this._jqm].c,o);s++;this._jqm=s;
H[s]={c:$.extend(p,$.jqm.params,o),a:F,w:$(this).addClass('jqmID'+s),s:s};
if(p.trigger)$(this).jqmAddTrigger(p.trigger);
});};

$.fn.jqmAddClose=function(e){return hs(this,e,'jqmHide');};
$.fn.jqmAddTrigger=function(e){return hs(this,e,'jqmShow');};
$.fn.jqmShow=function(t){return this.each(function(){t=t||window.event;$.jqm.open(this._jqm,t);});};
$.fn.jqmHide=function(t){return this.each(function(){t=t||window.event;$.jqm.close(this._jqm,t)});};

$.jqm = {
hash:{},
open:function(s,t){var h=H[s],c=h.c,cc='.'+c.closeClass,z=(parseInt(h.w.css('z-index'))),z=(z>0)?z:3000,o=$('<div></div>').css({height:'100%',width:'100%',position:'fixed',left:0,top:0,'z-index':z-1,opacity:c.overlay/100});if(h.a)return F;h.t=t;h.a=true;h.w.css('z-index',z);
 if(c.modal) {if(!A[0])L('bind');A.push(s);}
 else if(c.overlay > 0)h.w.jqmAddClose(o);
 else o=F;

 h.o=(o)?o.addClass(c.overlayClass).prependTo('body'):F;
 if(ie6){$('html,body').css({height:'100%',width:'100%'});if(o){o=o.css({position:'absolute'})[0];for(var y in {Top:1,Left:1})o.style.setExpression(y.toLowerCase(),"(_=(document.documentElement.scroll"+y+" || document.body.scroll"+y+"))+'px'");}}

 if(c.ajax) {var r=c.target||h.w,u=c.ajax,r=(typeof r == 'string')?$(r,h.w):$(r),u=(u.substr(0,1) == '@')?$(t).attr(u.substring(1)):u;
  r.html(c.ajaxText).load(u,function(){if(c.onLoad)c.onLoad.call(this,h);if(cc)h.w.jqmAddClose($(cc,h.w));e(h);});}
 else if(cc)h.w.jqmAddClose($(cc,h.w));

 if(c.toTop&&h.o)h.w.before('<span id="jqmP'+h.w[0]._jqm+'"></span>').insertAfter(h.o);	
 (c.onShow)?c.onShow(h):h.w.show();e(h);return F;
},
close:function(s){var h=H[s];if(!h.a)return F;h.a=F;
 if(A[0]){A.pop();if(!A[0])L('unbind');}
 if(h.c.toTop&&h.o)$('#jqmP'+h.w[0]._jqm).after(h.w).remove();
 if(h.c.onHide)h.c.onHide(h);else{h.w.hide();if(h.o)h.o.remove();} return F;
},
params:{}};
var s=0,H=$.jqm.hash,A=[],ie6=$.browser.msie&&($.browser.version == "6.0"),F=false,
i=$('<iframe src="javascript:false;document.write(\'\');" class="jqm"></iframe>').css({opacity:0}),
e=function(h){if(ie6)if(h.o)h.o.html('<p style="width:100%;height:100%"/>').prepend(i);else if(!$('iframe.jqm',h.w)[0])h.w.prepend(i); f(h);},
f=function(h){try{$(':input:visible',h.w)[0].focus();}catch(_){}},
L=function(t){$()[t]("keypress",m)[t]("keydown",m)[t]("mousedown",m);},
m=function(e){var h=H[A[A.length-1]],r=(!$(e.target).parents('.jqmID'+h.s)[0]);if(r)f(h);return !r;},
hs=function(w,t,c){return w.each(function(){var s=this._jqm;$(t).each(function() {
 if(!this[c]){this[c]=[];$(this).click(function(){for(var i in {jqmShow:1,jqmHide:1})for(var s in this[i])if(H[this[i][s]])H[this[i][s]].w[i](this);return F;});}this[c].push(s);});});};
})(jQuery);
var videoIds=new Array();var videoTitles=new Array();var videoTexts=new Array();jQuery.fn.ytplaylist=function(options,ids,titles,texts){videoIds=ids.split("@@@");videoTitles=titles.split("@@@");videoTexts=texts.split("@@@");var options=jQuery.extend({holderId:'ytvideo',playerHeight:'417',playerWidth:'556',addThumbs:false,thumbSize:'small',showRelated:false,allowFullScreen:false},options);return this.each(function(){var selector=$(this);var showRelated="&rel=0";var fullScreen="";if(options.showRelated)showRelated="&rel=1";if(options.allowFullScreen)fullScreen="&fs=1";function play(id,autoplay)
{if(autoplay){autoPlayOption="&autoplay=1";}else{autoPlayOption="";}
var html='';html+='<object height="'+options.playerHeight+'" width="'+options.playerWidth+'">';html+='<param name="movie" value="http://www.youtube.com/v/'+id+autoPlayOption+showRelated+fullScreen+'"> </param>';html+='<param name="wmode" value="transparent"> </param>';if(options.allowFullScreen){html+='<param name="allowfullscreen" value="true"> </param>';}
html+='<embed src="http://www.youtube.com/v/'+id+autoPlayOption+showRelated+fullScreen+'"';if(options.allowFullScreen){html+=' allowfullscreen="true" ';}
html+='type="application/x-shockwave-flash" wmode="transparent"  height="'+options.playerHeight+'" width="'+options.playerWidth+'"></embed>';html+='</object>';return html;};$("#videoHeadline").html(videoTitles[0]);$("#videoSummary").html(videoTexts[0]);$("#videoLess").click(function(){$("#videoSummary").css("height","3em");$(this).hide();$("#videoMore").show();return false;});$("#videoMore").click(function(){$("#videoSummary").css("height","auto");$(this).hide();$("#videoLess").show();return false;});$("#videoLess").hide();if(videoTexts[0].length>180){$("#videoSummary").css("height","3em");$("#videoMore").show();}else{$("#videoSummary").css("height","auto");$("#videoMore").hide();}
$("#"+options.holderId+"").html(play(videoIds[0],false));selector.children("li").children("a").click(function(){var lis=$(this).parent("li").parent("ul").children("li");var index=$(lis).index($(this).parent("li"));$("#"+options.holderId+"").html(play(videoIds[index],true));$("#videoHeadline").html(videoTitles[index]);$("#videoSummary").html(videoTexts[index]);$("#videoLess").hide();if(videoTexts[index].length>180){$("#videoSummary").css("height","3em");$("#videoMore").show();}else{$("#videoSummary").css("height","auto");$("#videoMore").hide();}
$(this).parent("ul").find("li.currentvideo").removeClass("currentvideo");$(this).parent("li").addClass("currentvideo");return false;});if(options.addThumbs){selector.children().each(function(i){var replacedText=$(this).text();if(options.thumbSize=='small'){var thumbUrl="http://img.youtube.com/vi/"+youtubeid($(this).children("a").attr("href"))+"/2.jpg";}else{var thumbUrl="http://img.youtube.com/vi/"+youtubeid($(this).children("a").attr("href"))+"/0.jpg";}
$(this).children("a").empty().html("<img src='"+thumbUrl+"' alt='"+replacedText+"' />"+replacedText).attr("title",replacedText);});}
if(selector.children("li").length==1){$(".jcarousel-container").hide();}});};/**
 * jCarousel - Riding carousels with jQuery
 *   http://sorgalla.com/jcarousel/
 *
 * Copyright (c) 2006 Jan Sorgalla (http://sorgalla.com)
 * Dual licensed under the MIT (http://www.opensource.org/licenses/mit-license.php)
 * and GPL (http://www.opensource.org/licenses/gpl-license.php) licenses.
 *
 * Built on top of the jQuery library
 *   http://jquery.com
 *
 * Inspired by the "Carousel Component" by Bill Scott
 *   http://billwscott.com/carousel/
 *   
 * v0.2.7 for jquery 1.4.2
 */

(function(i){var q={vertical:false,rtl:false,start:1,offset:1,size:null,scroll:3,visible:null,animation:"normal",easing:"swing",auto:0,wrap:null,initCallback:null,reloadCallback:null,itemLoadCallback:null,itemFirstInCallback:null,itemFirstOutCallback:null,itemLastInCallback:null,itemLastOutCallback:null,itemVisibleInCallback:null,itemVisibleOutCallback:null,buttonNextHTML:"<div></div>",buttonPrevHTML:"<div></div>",buttonNextEvent:"click",buttonPrevEvent:"click",buttonNextCallback:null,buttonPrevCallback:null, itemFallbackDimension:null},r=false;i(window).bind("load.jcarousel",function(){r=true});i.jcarousel=function(a,c){this.options=i.extend({},q,c||{});this.autoStopped=this.locked=false;this.buttonPrevState=this.buttonNextState=this.buttonPrev=this.buttonNext=this.list=this.clip=this.container=null;if(!c||c.rtl===undefined)this.options.rtl=(i(a).attr("dir")||i("html").attr("dir")||"").toLowerCase()=="rtl";this.wh=!this.options.vertical?"width":"height";this.lt=!this.options.vertical?this.options.rtl? "right":"left":"top";for(var b="",d=a.className.split(" "),f=0;f<d.length;f++)if(d[f].indexOf("jcarousel-skin")!=-1){i(a).removeClass(d[f]);b=d[f];break}if(a.nodeName.toUpperCase()=="UL"||a.nodeName.toUpperCase()=="OL"){this.list=i(a);this.container=this.list.parent();if(this.container.hasClass("jcarousel-clip")){if(!this.container.parent().hasClass("jcarousel-container"))this.container=this.container.wrap("<div></div>");this.container=this.container.parent()}else if(!this.container.hasClass("jcarousel-container"))this.container= this.list.wrap("<div></div>").parent()}else{this.container=i(a);this.list=this.container.find("ul,ol").eq(0)}b!==""&&this.container.parent()[0].className.indexOf("jcarousel-skin")==-1&&this.container.wrap('<div class=" '+b+'"></div>');this.clip=this.list.parent();if(!this.clip.length||!this.clip.hasClass("jcarousel-clip"))this.clip=this.list.wrap("<div></div>").parent();this.buttonNext=i(".jcarousel-next",this.container);if(this.buttonNext.size()===0&&this.options.buttonNextHTML!==null)this.buttonNext= this.clip.after(this.options.buttonNextHTML).next();this.buttonNext.addClass(this.className("jcarousel-next"));this.buttonPrev=i(".jcarousel-prev",this.container);if(this.buttonPrev.size()===0&&this.options.buttonPrevHTML!==null)this.buttonPrev=this.clip.after(this.options.buttonPrevHTML).next();this.buttonPrev.addClass(this.className("jcarousel-prev"));this.clip.addClass(this.className("jcarousel-clip")).css({overflow:"hidden",position:"relative"});this.list.addClass(this.className("jcarousel-list")).css({overflow:"hidden", position:"relative",top:0,margin:0,padding:0}).css(this.options.rtl?"right":"left",0);this.container.addClass(this.className("jcarousel-container")).css({position:"relative"});!this.options.vertical&&this.options.rtl&&this.container.addClass("jcarousel-direction-rtl").attr("dir","rtl");var j=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null;b=this.list.children("li");var e=this;if(b.size()>0){var g=0,k=this.options.offset;b.each(function(){e.format(this,k++);g+=e.dimension(this, j)});this.list.css(this.wh,g+100+"px");if(!c||c.size===undefined)this.options.size=b.size()}this.container.css("display","block");this.buttonNext.css("display","block");this.buttonPrev.css("display","block");this.funcNext=function(){e.next()};this.funcPrev=function(){e.prev()};this.funcResize=function(){e.reload()};this.options.initCallback!==null&&this.options.initCallback(this,"init");if(!r&&i.browser.safari){this.buttons(false,false);i(window).bind("load.jcarousel",function(){e.setup()})}else this.setup()}; var h=i.jcarousel;h.fn=h.prototype={jcarousel:"0.2.7"};h.fn.extend=h.extend=i.extend;h.fn.extend({setup:function(){this.prevLast=this.prevFirst=this.last=this.first=null;this.animating=false;this.tail=this.timer=null;this.inTail=false;if(!this.locked){this.list.css(this.lt,this.pos(this.options.offset)+"px");var a=this.pos(this.options.start,true);this.prevFirst=this.prevLast=null;this.animate(a,false);i(window).unbind("resize.jcarousel",this.funcResize).bind("resize.jcarousel",this.funcResize)}}, reset:function(){this.list.empty();this.list.css(this.lt,"0px");this.list.css(this.wh,"10px");this.options.initCallback!==null&&this.options.initCallback(this,"reset");this.setup()},reload:function(){this.tail!==null&&this.inTail&&this.list.css(this.lt,h.intval(this.list.css(this.lt))+this.tail);this.tail=null;this.inTail=false;this.options.reloadCallback!==null&&this.options.reloadCallback(this);if(this.options.visible!==null){var a=this,c=Math.ceil(this.clipping()/this.options.visible),b=0,d=0; this.list.children("li").each(function(f){b+=a.dimension(this,c);if(f+1<a.first)d=b});this.list.css(this.wh,b+"px");this.list.css(this.lt,-d+"px")}this.scroll(this.first,false)},lock:function(){this.locked=true;this.buttons()},unlock:function(){this.locked=false;this.buttons()},size:function(a){if(a!==undefined){this.options.size=a;this.locked||this.buttons()}return this.options.size},has:function(a,c){if(c===undefined||!c)c=a;if(this.options.size!==null&&c>this.options.size)c=this.options.size;for(var b= a;b<=c;b++){var d=this.get(b);if(!d.length||d.hasClass("jcarousel-item-placeholder"))return false}return true},get:function(a){return i(".jcarousel-item-"+a,this.list)},add:function(a,c){var b=this.get(a),d=0,f=i(c);if(b.length===0){var j,e=h.intval(a);for(b=this.create(a);;){j=this.get(--e);if(e<=0||j.length){e<=0?this.list.prepend(b):j.after(b);break}}}else d=this.dimension(b);if(f.get(0).nodeName.toUpperCase()=="LI"){b.replaceWith(f);b=f}else b.empty().append(c);this.format(b.removeClass(this.className("jcarousel-item-placeholder")), a);f=this.options.visible!==null?Math.ceil(this.clipping()/this.options.visible):null;d=this.dimension(b,f)-d;a>0&&a<this.first&&this.list.css(this.lt,h.intval(this.list.css(this.lt))-d+"px");this.list.css(this.wh,h.intval(this.list.css(this.wh))+d+"px");return b},remove:function(a){var c=this.get(a);if(!(!c.length||a>=this.first&&a<=this.last)){var b=this.dimension(c);a<this.first&&this.list.css(this.lt,h.intval(this.list.css(this.lt))+b+"px");c.remove();this.list.css(this.wh,h.intval(this.list.css(this.wh))- b+"px")}},next:function(){this.tail!==null&&!this.inTail?this.scrollTail(false):this.scroll((this.options.wrap=="both"||this.options.wrap=="last")&&this.options.size!==null&&this.last==this.options.size?1:this.first+this.options.scroll)},prev:function(){this.tail!==null&&this.inTail?this.scrollTail(true):this.scroll((this.options.wrap=="both"||this.options.wrap=="first")&&this.options.size!==null&&this.first==1?this.options.size:this.first-this.options.scroll)},scrollTail:function(a){if(!(this.locked|| this.animating||!this.tail)){this.pauseAuto();var c=h.intval(this.list.css(this.lt));c=!a?c-this.tail:c+this.tail;this.inTail=!a;this.prevFirst=this.first;this.prevLast=this.last;this.animate(c)}},scroll:function(a,c){if(!(this.locked||this.animating)){this.pauseAuto();this.animate(this.pos(a),c)}},pos:function(a,c){var b=h.intval(this.list.css(this.lt));if(this.locked||this.animating)return b;if(this.options.wrap!="circular")a=a<1?1:this.options.size&&a>this.options.size?this.options.size:a;for(var d= this.first>a,f=this.options.wrap!="circular"&&this.first<=1?1:this.first,j=d?this.get(f):this.get(this.last),e=d?f:f-1,g=null,k=0,l=false,m=0;d?--e>=a:++e<a;){g=this.get(e);l=!g.length;if(g.length===0){g=this.create(e).addClass(this.className("jcarousel-item-placeholder"));j[d?"before":"after"](g);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)){j=this.get(this.index(e));if(j.length)g=this.add(e,j.clone(true))}}j=g;m=this.dimension(g);if(l)k+= m;if(this.first!==null&&(this.options.wrap=="circular"||e>=1&&(this.options.size===null||e<=this.options.size)))b=d?b+m:b-m}f=this.clipping();var p=[],o=0,n=0;j=this.get(a-1);for(e=a;++o;){g=this.get(e);l=!g.length;if(g.length===0){g=this.create(e).addClass(this.className("jcarousel-item-placeholder"));j.length===0?this.list.prepend(g):j[d?"before":"after"](g);if(this.first!==null&&this.options.wrap=="circular"&&this.options.size!==null&&(e<=0||e>this.options.size)){j=this.get(this.index(e));if(j.length)g= this.add(e,j.clone(true))}}j=g;m=this.dimension(g);if(m===0)throw Error("jCarousel: No width/height set for items. This will cause an infinite loop. Aborting...");if(this.options.wrap!="circular"&&this.options.size!==null&&e>this.options.size)p.push(g);else if(l)k+=m;n+=m;if(n>=f)break;e++}for(g=0;g<p.length;g++)p[g].remove();if(k>0){this.list.css(this.wh,this.dimension(this.list)+k+"px");if(d){b-=k;this.list.css(this.lt,h.intval(this.list.css(this.lt))-k+"px")}}k=a+o-1;if(this.options.wrap!="circular"&& this.options.size&&k>this.options.size)k=this.options.size;if(e>k){o=0;e=k;for(n=0;++o;){g=this.get(e--);if(!g.length)break;n+=this.dimension(g);if(n>=f)break}}e=k-o+1;if(this.options.wrap!="circular"&&e<1)e=1;if(this.inTail&&d){b+=this.tail;this.inTail=false}this.tail=null;if(this.options.wrap!="circular"&&k==this.options.size&&k-o+1>=1){d=h.margin(this.get(k),!this.options.vertical?"marginRight":"marginBottom");if(n-d>f)this.tail=n-f-d}if(c&&a===this.options.size&&this.tail){b-=this.tail;this.inTail= true}for(;a-- >e;)b+=this.dimension(this.get(a));this.prevFirst=this.first;this.prevLast=this.last;this.first=e;this.last=k;return b},animate:function(a,c){if(!(this.locked||this.animating)){this.animating=true;var b=this,d=function(){b.animating=false;a===0&&b.list.css(b.lt,0);if(!b.autoStopped&&(b.options.wrap=="circular"||b.options.wrap=="both"||b.options.wrap=="last"||b.options.size===null||b.last<b.options.size||b.last==b.options.size&&b.tail!==null&&!b.inTail))b.startAuto();b.buttons();b.notify("onAfterAnimation"); if(b.options.wrap=="circular"&&b.options.size!==null)for(var f=b.prevFirst;f<=b.prevLast;f++)if(f!==null&&!(f>=b.first&&f<=b.last)&&(f<1||f>b.options.size))b.remove(f)};this.notify("onBeforeAnimation");if(!this.options.animation||c===false){this.list.css(this.lt,a+"px");d()}else this.list.animate(!this.options.vertical?this.options.rtl?{right:a}:{left:a}:{top:a},this.options.animation,this.options.easing,d)}},startAuto:function(a){if(a!==undefined)this.options.auto=a;if(this.options.auto===0)return this.stopAuto(); if(this.timer===null){this.autoStopped=false;var c=this;this.timer=window.setTimeout(function(){c.next()},this.options.auto*1E3)}},stopAuto:function(){this.pauseAuto();this.autoStopped=true},pauseAuto:function(){if(this.timer!==null){window.clearTimeout(this.timer);this.timer=null}},buttons:function(a,c){if(a==null){a=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="first"||this.options.size===null||this.last<this.options.size);if(!this.locked&&(!this.options.wrap||this.options.wrap== "first")&&this.options.size!==null&&this.last>=this.options.size)a=this.tail!==null&&!this.inTail}if(c==null){c=!this.locked&&this.options.size!==0&&(this.options.wrap&&this.options.wrap!="last"||this.first>1);if(!this.locked&&(!this.options.wrap||this.options.wrap=="last")&&this.options.size!==null&&this.first==1)c=this.tail!==null&&this.inTail}var b=this;if(this.buttonNext.size()>0){this.buttonNext.unbind(this.options.buttonNextEvent+".jcarousel",this.funcNext);a&&this.buttonNext.bind(this.options.buttonNextEvent+ ".jcarousel",this.funcNext);this.buttonNext[a?"removeClass":"addClass"](this.className("jcarousel-next-disabled")).attr("disabled",a?false:true);this.options.buttonNextCallback!==null&&this.buttonNext.data("jcarouselstate")!=a&&this.buttonNext.each(function(){b.options.buttonNextCallback(b,this,a)}).data("jcarouselstate",a)}else this.options.buttonNextCallback!==null&&this.buttonNextState!=a&&this.options.buttonNextCallback(b,null,a);if(this.buttonPrev.size()>0){this.buttonPrev.unbind(this.options.buttonPrevEvent+ ".jcarousel",this.funcPrev);c&&this.buttonPrev.bind(this.options.buttonPrevEvent+".jcarousel",this.funcPrev);this.buttonPrev[c?"removeClass":"addClass"](this.className("jcarousel-prev-disabled")).attr("disabled",c?false:true);this.options.buttonPrevCallback!==null&&this.buttonPrev.data("jcarouselstate")!=c&&this.buttonPrev.each(function(){b.options.buttonPrevCallback(b,this,c)}).data("jcarouselstate",c)}else this.options.buttonPrevCallback!==null&&this.buttonPrevState!=c&&this.options.buttonPrevCallback(b, null,c);this.buttonNextState=a;this.buttonPrevState=c},notify:function(a){var c=this.prevFirst===null?"init":this.prevFirst<this.first?"next":"prev";this.callback("itemLoadCallback",a,c);if(this.prevFirst!==this.first){this.callback("itemFirstInCallback",a,c,this.first);this.callback("itemFirstOutCallback",a,c,this.prevFirst)}if(this.prevLast!==this.last){this.callback("itemLastInCallback",a,c,this.last);this.callback("itemLastOutCallback",a,c,this.prevLast)}this.callback("itemVisibleInCallback", a,c,this.first,this.last,this.prevFirst,this.prevLast);this.callback("itemVisibleOutCallback",a,c,this.prevFirst,this.prevLast,this.first,this.last)},callback:function(a,c,b,d,f,j,e){if(!(this.options[a]==null||typeof this.options[a]!="object"&&c!="onAfterAnimation")){var g=typeof this.options[a]=="object"?this.options[a][c]:this.options[a];if(i.isFunction(g)){var k=this;if(d===undefined)g(k,b,c);else if(f===undefined)this.get(d).each(function(){g(k,this,d,b,c)});else{a=function(m){k.get(m).each(function(){g(k, this,m,b,c)})};for(var l=d;l<=f;l++)l!==null&&!(l>=j&&l<=e)&&a(l)}}}},create:function(a){return this.format("<li></li>",a)},format:function(a,c){a=i(a);for(var b=a.get(0).className.split(" "),d=0;d<b.length;d++)b[d].indexOf("jcarousel-")!=-1&&a.removeClass(b[d]);a.addClass(this.className("jcarousel-item")).addClass(this.className("jcarousel-item-"+c)).css({"float":this.options.rtl?"right":"left","list-style":"none"}).attr("jcarouselindex",c);return a},className:function(a){return a+" "+a+(!this.options.vertical? "-horizontal":"-vertical")},dimension:function(a,c){var b=a.jquery!==undefined?a[0]:a,d=!this.options.vertical?(b.offsetWidth||h.intval(this.options.itemFallbackDimension))+h.margin(b,"marginLeft")+h.margin(b,"marginRight"):(b.offsetHeight||h.intval(this.options.itemFallbackDimension))+h.margin(b,"marginTop")+h.margin(b,"marginBottom");if(c==null||d==c)return d;d=!this.options.vertical?c-h.margin(b,"marginLeft")-h.margin(b,"marginRight"):c-h.margin(b,"marginTop")-h.margin(b,"marginBottom");i(b).css(this.wh, d+"px");return this.dimension(b)},clipping:function(){return!this.options.vertical?this.clip[0].offsetWidth-h.intval(this.clip.css("borderLeftWidth"))-h.intval(this.clip.css("borderRightWidth")):this.clip[0].offsetHeight-h.intval(this.clip.css("borderTopWidth"))-h.intval(this.clip.css("borderBottomWidth"))},index:function(a,c){if(c==null)c=this.options.size;return Math.round(((a-1)/c-Math.floor((a-1)/c))*c)+1}});h.extend({defaults:function(a){return i.extend(q,a||{})},margin:function(a,c){if(!a)return 0; var b=a.jquery!==undefined?a[0]:a;if(c=="marginRight"&&i.browser.safari){var d={display:"block","float":"none",width:"auto"},f,j;i.swap(b,d,function(){f=b.offsetWidth});d.marginRight=0;i.swap(b,d,function(){j=b.offsetWidth});return j-f}return h.intval(i.css(b,c))},intval:function(a){a=parseInt(a,10);return isNaN(a)?0:a}});i.fn.jcarousel=function(a){if(typeof a=="string"){var c=i(this).data("jcarousel"),b=Array.prototype.slice.call(arguments,1);return c[a].apply(c,b)}else return this.each(function(){i(this).data("jcarousel", new h(this,a))})}})(jQuery);/*
 * timeago: a jQuery plugin, version: 0.9.2 (2010-09-14)
 * @requires jQuery v1.2.3 or later
 *
 * Timeago is a jQuery plugin that makes it easy to support automatically
 * updating fuzzy timestamps (e.g. "4 minutes ago" or "about 1 day ago").
 *
 * For usage and examples, visit:
 * http://timeago.yarp.com/
 *
 * Licensed under the MIT:
 * http://www.opensource.org/licenses/mit-license.php
 *
 * Copyright (c) 2008-2010, Ryan McGeary (ryanonjavascript -[at]- mcgeary [*dot*] org)
 */
(function($) {
  $.timeago = function(timestamp) {
    if (timestamp instanceof Date) return inWords(timestamp);
    else if (typeof timestamp == "string") return inWords($.timeago.parse(timestamp));
    else return inWords($.timeago.datetime(timestamp));
  };
  var $t = $.timeago;

  $.extend($.timeago, {
    settings: {
      refreshMillis: 60000,
      allowFuture: false,
      strings: {
        prefixAgo: null,
        prefixFromNow: null,
        suffixAgo: "ago",
        suffixFromNow: "from now",
        seconds: "less than a minute",
        minute: "about a minute",
        minutes: "%d minutes",
        hour: "about an hour",
        hours: "about %d hours",
        day: "a day",
        days: "%d days",
        month: "about a month",
        months: "%d months",
        year: "about a year",
        years: "%d years",
        numbers: []
      }
    },
    inWords: function(distanceMillis) {
      var $l = this.settings.strings;
      var prefix = $l.prefixAgo;
      var suffix = $l.suffixAgo;
      if (this.settings.allowFuture) {
        if (distanceMillis < 0) {
          prefix = $l.prefixFromNow;
          suffix = $l.suffixFromNow;
        }
        distanceMillis = Math.abs(distanceMillis);
      }

      var seconds = distanceMillis / 1000;
      var minutes = seconds / 60;
      var hours = minutes / 60;
      var days = hours / 24;
      var years = days / 365;

      function substitute(stringOrFunction, number) {
        var string = $.isFunction(stringOrFunction) ? stringOrFunction(number, distanceMillis) : stringOrFunction;
        var value = ($l.numbers && $l.numbers[number]) || number;
        return string.replace(/%d/i, value);
      }

      var words = seconds < 45 && substitute($l.seconds, Math.round(seconds)) ||
        seconds < 90 && substitute($l.minute, 1) ||
        minutes < 45 && substitute($l.minutes, Math.round(minutes)) ||
        minutes < 90 && substitute($l.hour, 1) ||
        hours < 24 && substitute($l.hours, Math.round(hours)) ||
        hours < 48 && substitute($l.day, 1) ||
        days < 30 && substitute($l.days, Math.floor(days)) ||
        days < 60 && substitute($l.month, 1) ||
        days < 365 && substitute($l.months, Math.floor(days / 30)) ||
        years < 2 && substitute($l.year, 1) ||
        substitute($l.years, Math.floor(years));

      return $.trim([prefix, words, suffix].join(" "));
    },
    parse: function(iso8601) {
      var s = $.trim(iso8601);
      s = s.replace(/\.\d\d\d+/,""); // remove milliseconds
      s = s.replace(/-/,"/").replace(/-/,"/");
      s = s.replace(/T/," ").replace(/Z/," UTC");
      s = s.replace(/([\+-]\d\d)\:?(\d\d)/," $1$2"); // -04:00 -> -0400
      return new Date(s);
    },
    datetime: function(elem) {
      // jQuery's `is()` doesn't play well with HTML5 in IE
      var isTime = $(elem).get(0).tagName.toLowerCase() == "time"; // $(elem).is("time");
      var iso8601 = isTime ? $(elem).attr("datetime") : $(elem).attr("title");
      return $t.parse(iso8601);
    }
  });

  $.fn.timeago = function() {
    var self = this;
    self.each(refresh);

    var $s = $t.settings;
    if ($s.refreshMillis > 0) {
      setInterval(function() { self.each(refresh); }, $s.refreshMillis);
    }
    return self;
  };

  function refresh() {
    var data = prepareData(this);
    if (!isNaN(data.datetime)) {
      $(this).text(inWords(data.datetime));
    }
    return this;
  }

  function prepareData(element) {
    element = $(element);
    if (!element.data("timeago")) {
      element.data("timeago", { datetime: $t.datetime(element) });
      var text = $.trim(element.text());
      if (text.length > 0) element.attr("title", text);
    }
    return element.data("timeago");
  }

  function inWords(date) {
    return $t.inWords(distance(date));
  }

  function distance(date) {
    return (new Date().getTime() - date.getTime());
  }

  // fix for IE6 suckage
  document.createElement("abbr");
  document.createElement("time");
})(jQuery);

(function($){var url1=/(^|&lt;|\s)(www\..+?\..+?[^\.])(\.?)(\s|&gt;|$)/g,url2=/(^|&lt;|\s)(((https?):\/\/).+?)(\.?)(\s|&gt;|$)/g,hashtag=/(^|)#(\w+)/gi,authorlink=/(^|)@(\w+)/gi,linkifyThis=function(){var childNodes=this.childNodes,i=childNodes.length;while(i--)
{var n=childNodes[i];if(n.nodeType==3){var html=$.trim(n.nodeValue);if(html)
{html=html.replace(/&/g,'&amp;').replace(/</g,'&lt;').replace(/>/g,'&gt;').replace(url1,'$1<a rel="external" href="http://$2">$2</a>$3$4').replace(url2,'$1<a rel="external" href="$2">$2</a>$5$6').replace(hashtag,'$1<a rel="external" href="http://www.twitter.com/#!/search?q=%23$2">#$2</a>').replace(authorlink,'$1<a rel="external" href="http://www.twitter.com/#!/$2">@$2</a>');$(n).after(html).remove();}}
else if(n.nodeType==1&&!/^(a|button|textarea)$/i.test(n.tagName)){linkifyThis.call(n);}}};$.fn.linkify=function(){return this.each(linkifyThis);};})(jQuery);/*!
 * jQuery Cookie Plugin v1.3.1
 * https://github.com/carhartl/jquery-cookie
 *
 * Copyright 2013 Klaus Hartl
 * Released under the MIT license
 */
(function (factory) {
	if (typeof define === 'function' && define.amd) {
		// AMD. Register as anonymous module.
		define(['jquery'], factory);
	} else {
		// Browser globals.
		factory(jQuery);
	}
}(function ($) {

	var pluses = /\+/g;

	function decode(s) {
		if (config.raw) {
			return s;
		}
		return decodeURIComponent(s.replace(pluses, ' '));
	}

	function decodeAndParse(s) {
		if (s.indexOf('"') === 0) {
			// This is a quoted cookie as according to RFC2068, unescape...
			s = s.slice(1, -1).replace(/\\"/g, '"').replace(/\\\\/g, '\\');
		}

		s = decode(s);

		try {
			return config.json ? JSON.parse(s) : s;
		} catch(e) {}
	}

	var config = $.cookie = function (key, value, options) {

		// Write
		if (value !== undefined) {
			options = $.extend({}, config.defaults, options);

			if (typeof options.expires === 'number') {
				var days = options.expires, t = options.expires = new Date();
				t.setDate(t.getDate() + days);
			}

			value = config.json ? JSON.stringify(value) : String(value);

			return (document.cookie = [
				config.raw ? key : encodeURIComponent(key),
				'=',
				config.raw ? value : encodeURIComponent(value),
				options.expires ? '; expires=' + options.expires.toUTCString() : '', // use expires attribute, max-age is not supported by IE
				options.path    ? '; path=' + options.path : '',
				options.domain  ? '; domain=' + options.domain : '',
				options.secure  ? '; secure' : ''
			].join(''));
		}

		// Read
		var cookies = document.cookie.split('; ');
		var result = key ? undefined : {};
		for (var i = 0, l = cookies.length; i < l; i++) {
			var parts = cookies[i].split('=');
			var name = decode(parts.shift());
			var cookie = parts.join('=');

			if (key && key === name) {
				result = decodeAndParse(cookie);
				break;
			}

			if (!key) {
				result[name] = decodeAndParse(cookie);
			}
		}

		return result;
	};

	config.defaults = {};

	$.removeCookie = function (key, options) {
		if ($.cookie(key) !== undefined) {
			// Must not alter options, thus extending a fresh object...
			$.cookie(key, '', $.extend({}, options, { expires: -1 }));
			return true;
		}
		return false;
	};

}));

jQuery.fn.highlight=function(spaceSeparatedWords){var regex=new RegExp("("+("["+spaceSeparatedWords.split('').join("][")+"]").replace(/\[\s+\]/g,"|")+")","gi");return this.each(function(){var helper=$(this);helper.text(helper.text().replace(regex,"\x01$1\x02"));this.innerHTML=this.innerHTML.replace(/\x01/g,'<strong>').replace(/\x02/g,'</strong>');});};jQuery.fn.nl2br=function(){return this.each(function(){this.innerHTML=this.innerHTML.replace(/[\r\f\n]+/g,"<br/>");});};
(function(){var fieldSelection={getSelection:function(){var e=this.jquery?this[0]:this;return(('selectionStart'in e&&function(){var l=e.selectionEnd-e.selectionStart;return{start:e.selectionStart,end:e.selectionEnd,length:l,text:e.value.substr(e.selectionStart,l)};})||(document.selection&&function(){e.focus();var r=document.selection.createRange();if(r==null){return{start:0,end:e.value.length,length:0}}
var re=e.createTextRange();var rc=re.duplicate();re.moveToBookmark(r.getBookmark());rc.setEndPoint('EndToStart',re);return{start:rc.text.length,end:rc.text.length+r.text.length,length:r.text.length,text:r.text};})||function(){return{start:0,end:e.value.length,length:0};})();},replaceSelection:function(){var e=this.jquery?this[0]:this;var text=arguments[0]||'';return(('selectionStart'in e&&function(){e.value=e.value.substr(0,e.selectionStart)+text+e.value.substr(e.selectionEnd,e.value.length);return this;})||(document.selection&&function(){e.focus();document.selection.createRange().text=text;return this;})||function(){e.value+=text;return this;})();}};jQuery.each(fieldSelection,function(i){jQuery.fn[i]=this;});})();
if(typeof SI!='object'){SI={};}
SI.Files={htmlClass:'SI-FILES-STYLIZED',fileClass:'file',wrapClass:'cabinet',fini:false,able:false,init:function()
{this.fini=true;var ie=0
if(window.opera||(ie&&ie<5.5)||!document.getElementsByTagName){return;}
this.able=true;var html=document.getElementsByTagName('html')[0];html.className+=(html.className!=''?' ':'')+this.htmlClass;},stylize:function(elem)
{if(!this.fini){this.init();};if(!this.able){return;};elem.parentNode.file=elem;elem.parentNode.onmousemove=function(e)
{if(typeof e=='undefined')e=window.event;if(typeof e.pageY=='undefined'&&typeof e.clientX=='number'&&document.documentElement)
{e.pageX=e.clientX+document.documentElement.scrollLeft;e.pageY=e.clientY+document.documentElement.scrollTop;};var ox=oy=0;var elem=this;if(elem.offsetParent)
{ox=elem.offsetLeft;oy=elem.offsetTop;while(elem=elem.offsetParent)
{ox+=elem.offsetLeft;oy+=elem.offsetTop;};};var x=e.pageX-ox;var y=e.pageY-oy;var w=this.file.offsetWidth;var h=this.file.offsetHeight;if(x<0||y<0||x>this.offsetWidth||y>this.offsetHeight){x=0;y=0;h=0;w=30;wFile='10px';}else{wFile='auto';}
this.file.style.top=(y-(h/2)-2)+'px';this.file.style.left=(x-(w-30))+'px';this.file.style.width=wFile;};},stylizeById:function(id)
{this.stylize(document.getElementById(id));},stylizeAll:function()
{if(!this.fini){this.init();};if(!this.able){return;};var inputs=document.getElementsByTagName('input');for(var i=0;i<inputs.length;i++)
{var input=inputs[i];if(input.type=='file'&&input.className.indexOf(this.fileClass)!=-1&&input.parentNode.className.indexOf(this.wrapClass)!=-1)
{this.stylize(input);};};}};
$(document).ready(function(){addArticleTrackingActions();});function addArticleTrackingActions(){$('.listArticleTrackingArticles .atStatus').each(function(){var stepCount=7;$(this).data('singleWidth',$(this).width()/stepCount);$(this).data('isActive',false);$(this).data('oldPosition',null);$(this).data('currentToolTip',-1);var oldPositition=$(this).backgroundPosition().match(/([0-9]*)px (-?[0-9]*)px/);$(this).data('oldPosition',oldPositition);}).mouseover(function(){if($(this).parents('.colLeft').length>0){$('.colLeft').addClass('zIndex');$('.colRight').removeClass('zIndex');}
else{$('.colLeft').removeClass('zIndex');$('.colRight').addClass('zIndex');}
$(this).addClass('isActive');}).mousemove(function(eventObject){var statusNumber=$(this).parents('li').attr('class').match(/status([1-7])/);if(jQuery.isArray(statusNumber)){var currentPosition=(eventObject.clientX-$(this).offset().left);var currentToolTip=Math.ceil(currentPosition/$(this).data('singleWidth'));if($(this).data('currentToolTip')!=currentToolTip&&$(this).data('currentToolTip')!=-1){$(this).find('.information.status'+$(this).data('currentToolTip')).removeShadow().hide();}
$(this).data('currentToolTip',currentToolTip);if(currentToolTip==statusNumber[1]){if($(this).data('isActive')==false){$(this).css('background-position',($(this).data('oldPosition')[1]-$(this).width())+"px "+$(this).data('oldPosition')[2]+"px");$(this).data('isActive',true);}}
else{$(this).data('isActive',false);$(this).css('background-position',$(this).data('oldPosition')[0]);}
var tooltip=$(this).find('.information.status'+currentToolTip);if(tooltip.is(':visible')==false){$(this).find('.dropShadow').remove();tooltip.show().css('left',currentPosition);window.setTimeout(function(){if(tooltip.is(':visible')){tooltip.dropShadow(shadowOptionsStandard);}},1);}}}).mouseout(function(){$(this).find('.information').removeShadow().hide();$(this).removeClass('isActive').data('isActive',false);$(this).css('background-position',$(this).data('oldPosition')[0]);});}
$(document).ready(function(){addArticleTrackingSelectorActions();});function addArticleTrackingSelectorActions(){$('.articleTrackingSelector .atStatus').each(function(){var stepCount=1;$(this).data('singleWidth',$(this).width()/stepCount);$(this).css({'width':'auto'});$(this).css({'line-height':'13px'});$(this).data('isActive',false);$(this).data('oldPosition',null);$(this).data('currentToolTip',-1);var oldPositition=$(this).backgroundPosition().match(/([0-9]*)px (-?[0-9]*)px/);$(this).data('oldPosition',oldPositition);}).mouseover(function(){if($(this).parents('.colLeft').length>0){$('.colLeft').addClass('zIndex');$('.colRight').removeClass('zIndex');}
else{$('.colLeft').removeClass('zIndex');$('.colRight').addClass('zIndex');}
$(this).addClass('isActive');}).mousemove(function(eventObject){var statusNumber=$(this).attr('class').match(/atStatus([1-7])/);if(jQuery.isArray(statusNumber)){var currentPosition=($(this).offsetParent().left);if($(this).data('isActive')==false){$(this).data('isActive',true);}
var tooltip=$(this).find('.information');if(tooltip.is(':visible')==false){$(this).find('.dropShadow').remove();tooltip.show().css('left',currentPosition);window.setTimeout(function(){if(tooltip.is(':visible')){tooltip.dropShadow(shadowOptionsStandard);}},1);}}}).mouseout(function(){$(this).find('.information').hide().removeShadow();$(this).removeClass('isActive').data('isActive',false);$(this).find('.dropShadow').remove();});};
$(document).ready(function(){$('.carousel').each(function(){var carouselCount=$(this).find('.teaserCarousel li.teaserCarouselItem').length;for(var i=0;i<(carouselCount-1);i++){$(this).find('.carouselPos li:first').clone().prependTo('.carouselPos');}
$(this).find('.carouselPos li:first').addClass('isActive');$(this).find('.teaserCarousel').bind('switchElement',function(event,numberOfSlide){$(this).find('.carouselPos li').removeClass('isActive');$(this).find('.carouselPos li:eq('+numberOfSlide+')').addClass('isActive');}).hover(function(){$(this).trigger('pauseCarousel');},function(){$(this).trigger('playCarousel');}).carousel({start:0,speed:'slow',duration:6000,seed:carouselCount,switchEvent:'switchElement',pauseEvent:'pauseCarousel',playEvent:'playCarousel'});$(this).find('.carouselPos').hover(function(){$('.teaserCarousel').trigger('pauseCarousel');});});});
$(document).ready(function(){if(jQuery.isFunction($.preloadImages)){$.preloadImages('/sgw/img/bg_flapNavigation_active.gif','/sgw/img/bg_flapNavigation_inactive.gif');}
$('.flapHead').springerFlap();});(function($){$.fn.springerFlap=function(settings){var flapIsMoving=false;return this.each(function(){if($(this).hasClass('hideContent')){$(this).next().addClass('displayNone');}
else{$(this).addClass('flapHeadOpen');}
if($(this).hasClass('flapCloseNotAttached')){$(this).toggleClass('flapCloseNotAttached');}}).click(function(){if(flapIsMoving==false){flapIsMoving=true;if($(this).is(".description a.flapHead.flapHeadOnlyOpen")){$(this).hide();}else{$(this).toggleClass('flapHeadOpen');$(this).toggleClass('hideContent');}
if(weNeedAHackForIe7AndIframeBelow()){$('.colLeftContentContainer').css('zoom','normal');}
if($(this).is(".description a.flapClose")){$(this).closest(".bookDescription").toggle('blind',{},'slow',function(){flapIsMoving=false;if(jQuery().setCustomScroller){$('.scrollContainer').setCustomScroller();}
if(weNeedAHackForIe7AndIframeBelow()){$('.colLeftContentContainer').css('zoom','1');}});$(this).closest(".description").find("a.flapHeadOnlyOpen").show();}else{$(this).next().toggle('blind',{},'slow',function(){flapIsMoving=false;if(jQuery().setCustomScroller){$('.scrollContainer').setCustomScroller();}
if(weNeedAHackForIe7AndIframeBelow()){$('.colLeftContentContainer').css('zoom','1');}});}
try{var flapName=webtrackingGetMetaData($(this).find('.flapName'));var changedElementHeader=$(this);$(this).storeFlapSelectedState(changedElementHeader,flapName);webtrackingSetProduction();webtrackingTriggerPageElementEvent(flapName,"Flaps");}catch(e){}}
return false;});};$.fn.storeFlapSelectedState=function(changedElementHeader,flapName){if(flapName==null||(typeof flapName)=='undefined'){return;}
var n=$(changedElementHeader).parents('.flapsContainer');if(n==undefined||n[0]==undefined){return;}
portletid=n[0].id;if(portletid==null||(typeof portletid)=='undefined'){return;}
var action;if($(changedElementHeader).hasClass('hideContent')){action='close';}else{action='open';}
var customURL="/accordionSelected/"+portletid+"/flap/"+flapName+"/"+action;$.getJSON(customURL,null,function(data,textStatus){});};})(jQuery);function weNeedAHackForIe7AndIframeBelow(){return isBrowserIE7&&typeof(iFrameFlapHack)!='undefined'&&iFrameFlapHack==true;};
$(document).ready(function(){showSecNavigationLayers();displayBreadCrumbNavigation();$("div.secSubNav").setWidth();$("li.secNav").positionSecNavi();setWidthOfAdvancedSearchInputField();$('.description').each(function(){$(this).parent().prev('.productGraphic').css('margin-bottom','-5px');});$('li.calculatingPrices').setHeight();changeListBgColor();});function changeListBgColor(){$('.productBasketBookseller li.listItemBooks').hover(function(){$(this).addClass('hasHover');},function(){$(this).removeClass('hasHover');});}
function displayBreadCrumbNavigation(){var totalWidth=0;var currentCategoryWidth=0;var breadCrumbPathWidth=0;if($(".currentCategory").width()){var totalWidth=$('.breadCrumbPathContainer').width()-13;currentCategoryWidth=$(".currentCategory").width()+18;$('.breadCrumbPath').css("marginLeft",currentCategoryWidth);$('.breadCrumbPath').children('span').each(function(i){breadCrumbPathWidth=breadCrumbPathWidth+$(this).width();});if((currentCategoryWidth+breadCrumbPathWidth)>totalWidth){$('.breadCrumbPathContainer').css("paddingTop",1);$('.breadCrumbPath span').css("lineHeight","13px");}}}
function setWidthOfAdvancedSearchInputField(){var labelWidth=$('div.aSearch label').width();var inputFieldWidth=480-labelWidth;$('.asq').css("width",inputFieldWidth);}
function hovershowLegacySubnavigationLayers(myThis){$(".subNavigation").removeShadow().hide();subNavigationIsVisible=$(myThis).hasClass("isActive");$(myThis).addClass('isActive').children('.mainNavigationLink').addClass('isActive');$(".subNavigation",myThis).show().dropShadow(shadowOptionsNavigation);}
function hovershowSubnavigationLayers(myThis){$(".subNavigation").removeShadow().hide();subNavigationIsVisible=$(myThis).hasClass("open");$(myThis).addClass('open').children('.mainNavigationLinkMark').addClass('open');$(".subNavigation",myThis).show().dropShadow(shadowOptionsNavigation);}
$.fn.getWidth=function(){$(this).each(function(){var totalWidth=180;$(this).children('ul').each(function(i){if(i>0){totalWidth=totalWidth+167;}});$(this).css({'width':totalWidth});});return this;};function showSecNavigationLayers(){var secNavigationIsVisible=false;$('li.secNav').hover(function(){var myThis=$(this);var t=setTimeout(function(){hovershowSecNavigationLayers(myThis);},300);$(this).data('timeout',t);},function(){clearTimeout($(this).data('timeout'));if(!secNavigationIsVisible){$(this).children('a.secNavHover, div.secSubNav').removeClass('displayBlock');$(this).children('a.secNavHover, div.secSubNav').addClass('displayNone');}
$(".secSubNav",this).removeShadow().hide();}).bind('closeMe',function(){$('li.secNav').children('a.secNavHover, div.secSubNav').removeClass('displayBlock');$('li.secNav').children('a.secNavHover, div.secSubNav').addClass('displayNone');$(".secSubNav","li.secNav").removeShadow().hide();});}
function hovershowSecNavigationLayers(myThis){registerOpenLayer({'elementToTrigger':myThis,'eventToTrigger':'closeMe'},$("div.secSubNav"));secNavigationIsVisible=$(myThis).children('a.secNavHover').hasClass("displayBlock");$(myThis).children('a.secNavHover, div.secSubNav').removeClass('displayNone');$(myThis).children('a.secNavHover, div.secSubNav').addClass('displayBlock');$(".secSubNav",myThis).show().dropShadow(shadowOptionsNavigation);}
$.fn.setWidth=function(){$(this).each(function(){var totalWidth=180;$(this).children('ul').each(function(i){if(i>0){totalWidth=totalWidth+167;}});$(this).css({'width':totalWidth});});return this;};$.fn.setHeight=function(){var listHeight=0;$(this).parent('ul.productBasket').each(function(i){listHeight=listHeight+$(this).height();});var marginTop=(listHeight-$('div.cPdiv').height())/2;if(marginTop>50){marginTop=50;}
$('div.cPdiv').css({'margin-top':marginTop});$(this).css({'height':listHeight});return this;};$.fn.positionSecNavi=function(){var leftPosition=0;var rightPosition=0;$(this).each(function(n){var moveMeToTheLeft=0;var totalWidth=0;leftPosition=rightPosition;rightPosition=leftPosition+$(this).width();$(this).children().children('ul').each(function(i){if(i>1){moveMeToTheLeft=1;}});if((n<4)||(moveMeToTheLeft==1)){$(this).children('div.secSubNav').css({'left':leftPosition-rightPosition+$(this).width()-1});}else{$(this).children('div.secSubNav').css({'left':rightPosition-$(this).width()});}
$(this).children('a.secNavHover').css({'left':leftPosition-1});});return this;};function goURL(url,anchor){if(anchor&&(anchor.tagName==='A')){anchor.href=url;return true;}else{window.location.href=url;return false;}}
function refreshTempCatalogImg(){$('.tempCatalogImg').each(function(){var src=$(this).attr("src");$(this).attr("src",src+"&tmp"+new Date().getTime());});}
function writeUserTokenCookie(cookiename,token,domain){var cookieValue=$.cookie(cookiename);if(cookieValue){$.removeCookie(cookiename,{domain:domain,path:'/'});}
$.cookie(cookiename,token,{domain:domain,path:'/'});}
function removeUserTokenCookies(domain){$.removeCookie('sim-user-token',{domain:domain,path:'/'});$.removeCookie('UID',{domain:domain,path:'/'});$.removeCookie('spcomLoggedIn',{domain:domain,path:'/'});$.removeCookie('beechwood_authentication',{domain:domain,path:'/'});};
$(document).ready(function(){$.fn.getListHeight=function(){var portletHeight=0;$(this).children('li').each(function(i){portletHeight=portletHeight+$(this).height();});return portletHeight;};$.fn.prepareList=function(init){var portletParent=$(this).parent().find('ul.listToOpenLayer.hasExtender');if(init==1){$(this).parent('.portlet').css('padding-bottom','0');portletParent.find("li.listItemToOpenLayer:first").addClass('firstExtendedItem');}
if($(this).hasClass("isExtended")){portletParent.removeClass('overflowHidden');portletParent.removeClass('zIndex');if(init==1){var portletHeight=portletParent.getListHeight();portletParent.css({'height':portletHeight});}}
else{portletParent.addClass('overflowHidden');portletParent.addClass('zIndex');}};$('.portletExtender').each(function(){$(this).prepareList(1);});$('.portletExtender').click(function(){closeCurrentLayer();var isClosing=$(this).hasClass("isExtended");var isOpening=!isClosing;var portletExtender=$(this);var portletParent=$(this).parent().find('ul.listToOpenLayer.hasExtender');var portletParents=$(this).parents('.portlet');portletParent.addClass('zIndex').addClass("overflowHidden");$(this).toggleClass("hasHover").find('a').toggleClass("hasHover");var portletHeight=0;if(isOpening){portletHeight=portletParent.getListHeight();portletExtender.addClass("isExtended");}
portletParent.stop().animate({height:portletHeight},{complete:function(){if(isClosing){portletExtender.removeClass("isExtended");}else{portletParent.css('overflow','');portletExtender.prepareList();}
storeExpandableState(portletParents);}});return false;});$(function(){$('.portletExtender').hover(function(){$(this).addClass("hasHover");if(!$(this).hasClass("isExtended")){$(this).parent().find('.firstExtendedItem').addClass("hasHover");$(this).parent().find('ul.listToOpenLayer.hasExtender').stop().animate({height:7});};},function(){$(this).removeClass("hasHover");if(!$(this).hasClass("isExtended")){$(this).parent().find('.firstExtendedItem').removeClass("hasHover");$(this).parent().find('ul.listToOpenLayer.hasExtender').stop().animate({height:0});};});});function storeExpandableState(result){try{if(result==undefined||result[0]==undefined){return false;}
var portletid=result[0].id;if(portletid==""){if($(result).parents('.dndComponentSelector')==undefined||$(result).parents('.dndComponentSelector')[0]==undefined){return false;}
portletid=$(result).parents('.dndComponentSelector')[0].id;}
var state=$(result).find('.portletExtender').hasClass('isExtended');var customURL="/expandable/"+portletid+"/"+state;var cmPortletName=webtrackingGetMetaData($(result).find('.cmPortletName'));if(cmPortletName!=undefined&&cmPortletName!=""){customURL+="/"+cmPortletName;};$.getJSON(customURL,null,function(data,textStatus){try{if(data.cmTag!=undefined&&data.cmTag!=""){eval(data.cmTag);}}catch(e){};});}catch(e){};return true;};$("ul.listToOpenLayer li.listItemToOpenLayer").hover(function(){clearTimeout(hideTimer);if(!$(this).find(".portletLayer:first").is(":visible")){$(this).addClass("hasHover");}},function(){$(this).removeClass("hasHover");}).not(':has(a.isLink)').find('a:first').click(function(){var li=$(this).parent('li');if(li.hasClass('isActive')==false){closeCurrentLayer();var portletLayer=li.find(".portletLayer:first");var portletLayerTop=-120;var diff=$('.colLeft').offset().top-($(this).offset().top+portletLayerTop)+25;if(diff>0){portletLayer.css('top',portletLayerTop+diff);}
else{portletLayer.css('top',portletLayerTop);}
li.removeClass("hasHover").addClass("isActive");portletLayer.show().dropShadow(shadowOptionsStandard);li.find(".portletLayerBridge:first").show();registerOpenLayer({'elementToTrigger':li,'eventToTrigger':'closeMe'},li.find(".mediumLayer:first"));var contentLayer=li.find('.layerContent');if(contentLayer.hasClass('scrollContainer')==false){contentLayer.addClass('scrollContainer').customScroller({horizontal:0});}
return false;}
return false;});$("ul.listToOpenLayer li.listItemToOpenLayer").bind('closeMe',function(){$(this).find(".portletLayer").removeShadow().hide();$(this).find(".portletLayerBridge").hide();$(this).removeClass("isActive");}).find('.btnToCloseLayer').click(function(){closeCurrentLayer();return false;});$('#openShareLayer').click(function(){if($(".shareLayer").is(":visible")){closeCurrentLayer();}else{$(this).addClass('isActive');$(".shareLayer").show().dropShadow(shadowOptionsStandard);$(".shareLayerContainer .containerBridge").css('left',$(this).offset().left-$(".shareLayer").offset().left-1);registerOpenLayer({'elementToTrigger':$('#openShareLayer'),'eventToTrigger':'closeMe'},$(".shareLayer"));}
return false;}).bind('closeMe',function(){$(this).removeClass('isActive');$(".shareLayer").removeShadow().hide();});$('#openRecommendLayer').bind('showMe',function(){if($(this).hasClass('isActive')==false){$(this).addClass('isActive');$(".recommendLayer").show().dropShadow(shadowOptionsStandard);$(".recommendLayerContainer .containerBridge").css('left',$(this).offset().left-$(".recommendLayer").offset().left-1);registerOpenLayer({'elementToTrigger':$('#openRecommendLayer'),'eventToTrigger':'closeMe'},$(".recommendLayer"));}}).bind('closeMe',function(){$(this).removeClass('isActive');$(".recommendLayer").removeShadow().hide();});initCartLayerAndMarkedItemsLayer();$('.linkToOpenCustomizeList').click(function(){$(this).trigger('showMe');return false;}).bind('showMe',function(){if($(this).hasClass('isVisible')==false){$(this).addClass('isVisible');$(".customizeListLayer").show().dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.linkToOpenCustomizeList'),'eventToTrigger':'closeMe'},$(".customizeListLayer"));}}).bind('closeMe',function(){$(this).removeClass('isVisible');$(".customizeListLayer").removeShadow().hide();return false;});$(".customizeListLayer").find('.btnToCloseLayer').click(function(){closeCurrentLayer();return false;});$('.lightSelectBox .lightSelectBoxLink').click(function(){if($(".lightSelectBoxContainer").is(":visible")){closeCurrentLayer();}else{$(".lightSelectBoxContainer").show();$(".lightSelectBoxList").dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.lightSelectBoxContainer'),'eventToTrigger':'closeMe'},$(".lightSelectBox"));}
return false;});$(".lightSelectBoxContainer").bind('closeMe',function(){$(".lightSelectBoxContainer").hide();$(".lightSelectBoxList").removeShadow();});$('.darkSelectBox .darkSelectBoxLink').click(function(){if($(".darkSelectBoxContainer").is(":visible")){closeCurrentLayer();}else{$(".darkSelectBoxContainer").show().css('zIndex',100);$(".darkSelectBoxList").dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.darkSelectBoxContainer'),'eventToTrigger':'closeMe'},$(".darkSelectBox"));}
return false;});$(".darkSelectBoxContainer").bind('closeMe',function(){$(".darkSelectBoxContainer").hide();$(".darkSelectBoxList").removeShadow();});$('.linkContact').bind('closeMe',function(){$(".contactLayer").removeShadow().hide();});$('.contactLayer .btnToCloseLayer, .contactLayer .linkToCloseLayer').click(function(){closeCurrentLayer();return false;});registerAcademicTitle();$('.linkUpload').click(function(){closeCurrentLayer();var referenz=$(".uploadLayer").parents('.colLeftContentContainer:first');var layerCSS={left:(referenz.offset().left)+"px",top:($(this).offset().top-referenz.offset().top-100)+"px"};$(".uploadLayer").css(layerCSS).show();$(".uploadLayer").dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.linkUpload'),'eventToTrigger':'closeMe'},$(".uploadLayer"));return false;}).bind('closeMe',function(){$(".uploadLayer").removeShadow().hide();return false;});$('.uploadLayer .btnToCloseLayer').click(function(){closeCurrentLayer();return false;});initShippingLayer();if($('.addresses').hasClass('scrollContainer')==false){$('.addresses').addClass('scrollContainer').customScroller({horizontal:0});}
$('.openContact').click(function(){closeCurrentLayer();var referenz=$(".openContact");var layerCSS={left:(referenz.offset().left-392)+"px",top:(referenz.offset().top-400)+"px"};$(".contactLayer").css(layerCSS).show();$(".contactLayer").dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.openContact'),'eventToTrigger':'closeMe'},$(".contactLayer"));return false;}).bind('closeMe',function(){$(".contactLayer").removeShadow().hide();return false;});$('.contactLayer .btnToCloseLayer').click(function(){closeCurrentLayer();return false;});});(function($){jQuery.fn.backgroundPosition=function(){var p=$(this).css('background-position');if(typeof(p)==='undefined')return $(this).css('background-position-x')+' '+$(this).css('background-position-y');else return p;};})(jQuery);var shadowOptionsStandard={left:-3,top:3,opacity:0.3};var shadowOptionsNavigation={left:1,top:2,opacity:0.3};var activeLayer=null;function showContactLayerFor(linkContact){closeCurrentLayer();var referenz=$(".contactLayer").parents('.colLeftContentContainer:first');var layerCSS={left:(linkContact.offset().left-referenz.offset().left+30)+"px",top:(linkContact.offset().top-referenz.offset().top+30)+"px"};$(".contactLayer").css(layerCSS).show();$(".contactLayer").dropShadow(shadowOptionsStandard);$(".contactLayer").css('zIndex',200);registerOpenLayer({'elementToTrigger':$('.linkContact'),'eventToTrigger':'closeMe'},$(".contactLayer"));return false;}
function showPopupLayer(layerJQuery){closeCurrentLayer();layerJQuery.show();layerJQuery.dropShadow(shadowOptionsStandard);layerJQuery.bind('closeMe',function(){closePopupLayer(layerJQuery);});registerOpenLayer({'elementToTrigger':layerJQuery,'eventToTrigger':'closeMe'},layerJQuery);$('.body').addClass('zIndex');return false;}
function showAcademicTitleLayer(openAcademicTitleLayer){closeCurrentLayer();AcademicTitleLayer.show();AcademicTitleLayer.dropShadow(shadowOptionsStandard);AcademicTitleLayer.bind('closeMe',function(){closePopupLayer(AcademicTitleLayer);});registerOpenLayer({'elementToTrigger':AcademicTitleLayer,'eventToTrigger':'closeMe'},AcademicTitleLayer);return false;}
function closePopupLayer(layerClass){$(layerClass).removeShadow().hide();}
function initShippingLayer(){$('.openShippingLayer').click(function(){closeCurrentLayer();var layerCSS={left:"385px",top:"220px"};$(".shippingLayer").css(layerCSS).show();$(".shippingLayer").dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.openShippingLayer'),'eventToTrigger':'closeMe'},$(".shippingLayer"));return false;}).bind('closeMe',function(){$(".shippingLayer").removeShadow().hide();return false;});$('.shippingLayer .btnToCloseLayer').click(function(){closeCurrentLayer();return false;});}
function initCartLayerAndMarkedItemsLayer(){$('.showMarkedItemsLayer').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).click(function(){return false;}).hover(function(){var shoppingCartLayer=$(this).parents('.shoppingActions').find('.shoppingCartLayer');shoppingCartLayer.hide();shoppingCartLayer.find('.shoppingActionContainer').removeShadow();var markedItemsLayer=$(this).next();markedItemsLayer.show();markedItemsLayer.find('.shoppingActionContainer').dropShadow(shadowOptionsNavigation);markedItemsLayer.css('z-index','999');markedItemsLayer.find('.btnContainer').css('z-index','1000');registerOpenLayer({'elementToTrigger':$('.showMarkedItemsLayer'),'eventToTrigger':'closeOnMouseOut'},markedItemsLayer);},function(){});}
$(this).addClass('alreadyAttached');});$('.markedItemsAdd, .markedItemsDelete').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).click(function(){$(this).addClass('isChecked');return false;});}
$(this).addClass('alreadyAttached');});$('.markedItemsLayer').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).hover(function(){},function(){$(this).find('.shoppingActionContainer').removeShadow();$(this).find('.btnContainer').css('z-index','');$(this).hide();});}
$(this).addClass('alreadyAttached');});$('.btnContainer .markedItemsAdd, .btnContainer .markedItemsDelete').each(function(){if(!$(this).hasClass('alreadyAttachedBtn')){$(this).click(function(){$(this).removeClass('isChecked').addClass('allChecked');$(this).parent().parent().prev().addClass('allChecked');$(this).parents('.markedItemsLayer').find('tr').each(function(i){$(this).addClass('isChecked');});return false;});}
$(this).addClass('alreadyAttachedBtn');});$('.shoppingActions tr').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).click(function(){$(this).removeClass('hasHover').addClass('isChecked');itemHasIsCheckedClass=0;classForButton='';$(this).parent().children('tr').each(function(i){if($(this).hasClass('isChecked')){itemHasIsCheckedClass=itemHasIsCheckedClass+1;}});if($(this).parent().children().size()==itemHasIsCheckedClass){classForButton='allChecked';}else{classForButton='isChecked';}
if($(this).parents('.shoppingCartLayer').length>0){$(this).parents('.shoppingActions').find('.shoppingCart').removeClass('isChecked').addClass(classForButton);}
if($(this).parents('.markedItemsLayer').length>0){$(this).parents('.shoppingActions').find('.markedItems').addClass(classForButton);$(this).parents('.markedItemsLayer').find('.markedItemsAdd').removeClass('isChecked').addClass(classForButton);$(this).parents('.markedItemsLayer').find('.markedItemsDelete').removeClass('isChecked').addClass(classForButton);}
deleteItem=0;if($(this).hasClass('deleteMarkedItems')==1){$(this).parents('.shoppingActionContainer').removeShadow();if($(this).parent().children().size()==1){$(this).parents('.markedItemsLayer').remove();}else{$(this).parents('.markedItemsLayer').find('.shoppingActionContainer').dropShadow(shadowOptionsNavigation);$(this).remove();}}
return false;}).hover(function(){$(this).addClass('hasHover');},function(){$(this).removeClass('hasHover');});}
$(this).addClass('alreadyAttached');});$('.showShoppingCartLayer').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).click(function(){return false;}).hover(function(){var markedItemsLayer=$(this).parents('.shoppingActions').find('.markedItemsLayer');markedItemsLayer.hide();markedItemsLayer.find('.shoppingActionContainer').removeShadow();var shoppingCartLayer=$(this).next();shoppingCartLayer.show();shoppingCartLayer.find('.shoppingActionContainer').dropShadow(shadowOptionsNavigation);shoppingCartLayer.css('z-index','999');shoppingCartLayer.find('.btnContainer').css('z-index','1000');registerOpenLayer({'elementToTrigger':$('.showShoppingCartLayer'),'eventToTrigger':'closeOnMouseOut'},shoppingCartLayer);},function(){});}
$(this).addClass('alreadyAttached');});$('.btnContainer .shoppingCart').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).click(function(){return false;});}
$(this).addClass('alreadyAttached');});$('.shoppingCartLayer').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).hover(function(){},function(){$(this).find('.shoppingActionContainer').removeShadow();$(this).find('.btnContainer').css('z-index','');$(this).hide();});}
$(this).addClass('alreadyAttached');});}
var hideTimer=null;function registerOpenLayer(toDoForClosing,layerElement){if(hideTimer!=null){clearTimeout(hideTimer);}
closeCurrentLayer();if(layerElement.parents('.colLeft').length>0){$('.colLeft').addClass('zIndex');$('.colRight').removeClass('zIndex');}
else{$('.colLeft').removeClass('zIndex');$('.colRight').addClass('zIndex');}
layerElement.parents('.colLeftContent').addClass('zIndex');activeLayer=toDoForClosing;if(activeLayer.elementToTrigger.hasClass('closeOnMouseOut')){activeLayer.elementToTrigger.hover(function(){if(hideTimer!=null){clearTimeout(hideTimer);}
hideTimer=null;},function(){if(hideTimer!=null){clearTimeout(hideTimer);}
hideTimer=setTimeout(closeCurrentLayer,10);});}}
function closeCurrentLayer(){if(activeLayer!=null){activeLayer.elementToTrigger.trigger(activeLayer.eventToTrigger);}
activeLayer=null;$('.colLeftContent').removeClass('zIndex');}
function isElementActive(elem){return $(elem).hasClass('isActive');}
function drawScrollPane(elem){if(elem){elem.jScrollPaneRemove();elem.jScrollPane({'showArrows':true,'arrowSize':10,'scrollbarWidth':5,'scrollbarMargin':10});}}
function registerAcademicTitle(){$('.openAcademicTitleLayer').bind('closeMe',function(){$(".academicTitleLayer").removeShadow().hide();});$('.academicTitleLayer .btnToCloseLayer, .academicTitleLayer .linkToCloseLayer').click(function(){closeCurrentLayer();return false;});$('.openAcademicTitleLayer').click(function(){if($(".academicTitleLayer").is(":visible")){closeCurrentLayer();}else{$(this).addClass('isActive');$(".academicTitleLayer").show().dropShadow(shadowOptionsStandard);registerOpenLayer({'elementToTrigger':$('.openAcademicTitleLayer'),'eventToTrigger':'closeMe'},$(".academicTitleLayer"));}
return false;}).bind('closeMe',function(){$(this).removeClass('isActive');$(".academicTitleLayer").removeShadow().hide();});}
function removeAllShadows(){$('.dropShadow').removeShadow().hide();}
$(document).ready(function(){var link=$('.initialOpenLayerLink');if(!link||link==undefined||link.length==0)return false;li=link.parent();closeCurrentLayer();var portletLayer=li.find(".portletLayer:first");var portletLayerTop=-120;var colLeft=$('.colLeft');var diff=colLeft.offset().top-(link.offset().top+portletLayerTop)+25;if(diff>0){portletLayer.css('top',portletLayerTop+diff);}else{portletLayer.css('top',portletLayerTop);}
li.removeClass("hasHover").addClass("isActive");portletLayer.show().dropShadow(shadowOptionsStandard);li.find(".portletLayerBridge:first").show();registerOpenLayer({'elementToTrigger':li,'eventToTrigger':'closeMe'},li.find(".mediumLayer:first"));var contentLayer=li.find('.layerContent');if(contentLayer.hasClass('scrollContainer')==false){contentLayer.addClass('scrollContainer').customScroller({horizontal:0});}
return false;});function springerOverlay(selector){$(selector).prependTo('body');$(window).resize(function(){var overlay=$(selector);var top=$(window).height()/2-(overlay.height()/2)+$(window).scrollTop();var left=$(window).width()/2-(overlay.width()/2);overlay.css({'top':top,'left':left});});var overlay=$(selector);var top=$(window).height()/2-(overlay.height()/2)+$(window).scrollTop();var left=$(window).width()/2-(overlay.width()/2);overlay.css({'top':top,'left':left});$(selector).jqm({modal:true,onHide:function(h){h.o.remove();h.w.remove();}});$(selector).jqmShow();};function lookInsideOverlay(selector){springerOverlay(selector);var overlay=$(selector);var lookInsideWidth=$(".lookInside img",overlay).width();var extraWidthForScrollbar=15;overlay.css("width",lookInsideWidth+extraWidthForScrollbar+"px");};
$(document).ready(function(){prepareToolTips();});function prepareToolTips(){prepareSelectedToolTips("");}
function prepareSelectedToolTips(selector){$(selector+' .itemInformationToolTipPoint').each(function(){if(!$(this).hasClass('alreadyAttached')){$(this).hover(function(){$(this).next().css('display','block');$(this).parent().addClass('zIndex');$(this).next().dropShadow();return false;},function(){$(this).next().css('display','none');$(this).parent().removeClass('zIndex');$(this).next().removeShadow();return false;}).click(function(){return false;});}
$(this).addClass('alreadyAttached');});};
$(document).ready(function(){$('textarea.maxLength').keyup(function(){var maxLength=this.rows*this.cols;if(this.value.length>maxLength){this.value=this.value.substring(0,maxLength);}
return false;});});
$(document).ready(styleSelectBox);function styleSelectBox(){return false;$('select.stylishSelectbox, select.stylishSelectboxSmall, select.stylishSelectbox119Px, select.stylishSelectbox231Px, select.stylishSelectbox167Px').each(function(){if(!$(this).attr("style")){if($(this).hasClass('stylishSelectboxSmall')){var inputClass='selectboxSmall';}else if($(this).hasClass('stylishSelectbox119Px')){var inputClass='selectbox119Px';}else if($(this).hasClass('stylishSelectbox167Px')){var inputClass='selectbox167Px';}else if($(this).hasClass('stylishSelectbox231Px')){var inputClass='selectbox231Px';}else if($(this).hasClass('stylishSelectbox')){var inputClass='selectbox';}
$(this).selectbox({inputClass:inputClass})}});};
$(document).ready(function(){if(jQuery.isFunction($.preloadImages)){$.preloadImages('/sgw/img/bg_flapNavigation_active.gif','/sgw/img/bg_flapNavigation_inactive.gif');}
$('.accordion').springerAccordion();});(function($){$.fn.springerAccordion=function(settings){var accordionStandardOptions={collapsible:true,active:".accordionSectionOpen",autoHeight:true,icons:{'header':'displayNone','headerSelected':'displayNone'}};var accordionAlertOptions={collapsible:true,active:".accordionSectionOpen",autoHeight:false,icons:{'header':'displayNone','headerSelected':'displayNone'}};$(this).each(function(){$(this).find('.accordionBody').removeClass('displayNone');var optionSet=($(this).hasClass('alertAccordion'))?accordionAlertOptions:accordionStandardOptions;if($(this).hasClass('noCollapse')){optionSet.collapsible=false;}
if($(this).hasClass('dynamicContentAccordion')){optionSet.clearStyle=$(this).hasClass('dynamicContentAccordion');optionSet.autoHeight=false;}
$(this).accordion(optionSet).bind('accordionchangestart',function(event,ui){if(isBrowserIE8){toggleOverflow(1,this);}
ui.newHeader.removeClass('hideContent');ui.oldHeader.addClass('hideContent');}).bind('accordionchange',function(event,ui){if(isBrowserIE8){toggleOverflow(0,this);}
ui.newHeader.blur();try{var changedElementHeader;if(ui.newHeader.length>0){changedElementHeader=ui.newHeader[0];}else if(ui.oldHeader.length>0){changedElementHeader=ui.oldHeader[0];}
if(changedElementHeader!=undefined){var flapName=webtrackingGetMetaData($(changedElementHeader).find('.flapName'));$(this).storeAccordionSelectedState(changedElementHeader,flapName);webtrackingSetProduction();webtrackingTriggerPageElementTag(flapName,"Flaps",true,true,"-");}}catch(e){}});});};function toggleOverflow(set,elem){if(set){$(elem).parents(".portlet").css("overflow","hidden");}else{$(elem).parents(".portlet").css("overflow","visible");}};$.fn.storeAccordionSelectedState=function(changedElementHeader,flapName){if(flapName==null||(typeof flapName)=='undefined'){return;}
var portletid="";if(portletid==""){var n=$(changedElementHeader).parents('.accordion');if(n==undefined||n[0]==undefined){return;}
portletid=n[0].id;}
if(portletid==null||(typeof portletid)=='undefined'){return;}
if($(changedElementHeader).hasClass('hideContent')){flapName='';}
var customURL="/accordionSelected/"+portletid+"/accordion/"+flapName+"/open";$.getJSON(customURL,null,function(data,textStatus){});};})(jQuery);
$(document).ready(function(){$(".treeview").treeview({animated:"default",collapsed:true,unique:false});});
$(document).ready(function(){registerEqualHeights();registerEqualHeightsOddEven();registerEqualHeightsOf2Portlets();adjustNewslistsHeight();});function registerEqualHeights(){$(".equalHeight").equalHeights();}
function registerEqualHeightsOddEven(){$(".equalHeightOddEven").equalHeightsOddEven();}
function registerEqualHeightsOf2Portlets(){equalHeightsOf2Portlets();}
equalHeightsOf2Portlets=function(){if($(".disciplineList").parents('.colLeft').length>0&&$(".newsList").parents('.colLeft').length>0){var orderDisciplineNewslist="none";if($(".disciplineList").parents('.col1').length>0&&$(".newsList").parents('.col2').length>0){orderDisciplineNewslist="dispNews";}else if($(".disciplineList").parents('.col2').length>0&&$(".newsList").parents('.col1').length>0){orderDisciplineNewslist="newsDisp";}
if(orderDisciplineNewslist!="none"){var heightOfPortlet=0;var disciplinePortlet;var newsPortlet;if(orderDisciplineNewslist=="dispNews"){disciplinePortlet=$($(".col1").find(".disciplineList").parents('.portlet')[0]);newsPortlet=$($(".col2").find(".newsList").parents('.portlet')[0]);}else{disciplinePortlet=$($(".col2").find(".disciplineList").parents('.portlet')[0]);newsPortlet=$($(".col1").find(".newsList").parents('.portlet')[0]);}
if(disciplinePortlet.height()>newsPortlet.height()){heightOfPortlet=disciplinePortlet.height();}else{heightOfPortlet=newsPortlet.height();}
disciplinePortlet.css({'min-height':heightOfPortlet});newsPortlet.css({'min-height':heightOfPortlet});}}
return this;};adjustNewslistsHeight=function(){$(".newsList").parents('.portlet').each(function(i){var heightOfPortlet=$(this).height();$(this).css({'min-height':heightOfPortlet});$(this).children().css({'min-height':heightOfPortlet,'position':'relative'});$(this).find(".portletFooter").addClass('portletFooterEqualHeight');});};
alertsSel={updateCheckBoxGroup:function(checkbox,containerElementId){var r=$('#'+containerElementId).find('input:checkbox');if($(checkbox).attr('id')==$(r[0]).attr('id')&&$(checkbox).prop('checked')){r.filter(':gt(0)').removeAttr('checked');}else{var lc=r.filter(':gt(0)').filter(':checked').length;$(r[0]).prop('checked',lc==0);}},updateActionHistory:function(checkbox){var value=$('#actionHistory').attr('value');var action=(checkbox.prop('checked')?'+':'-')+checkbox.attr('value')+',';value+=action;$('#actionHistory').attr('value',value);},jAbcCurrentBucket:null,getSectionShifts:function(itemsCount){var s1=Math.floor(itemsCount/3);return[s1,2.0*s1];},updateCounter:function(checkbox){$('[id="allCounterSelector"]').each(function(){var c=parseInt($(this).html());c=c+(checkbox.prop('checked')?1:-1);$(this).html(c);});},createLabelCheckbox:function(setting){var e=$(document.createElement('div'));e.attr('class','itemFormCheckbox itemFormCheckboxNoFloat clearfix');var checkboxElement=document.createElement('input');var checkbox=$(checkboxElement);checkbox.attr('id',"cb"+setting.id);checkbox.attr('type','checkbox');checkbox.attr('value',setting.id);if(setting.isSelected){checkboxElement.checked=true;checkboxElement.defaultChecked=true;checkboxElement.setAttribute("checked","checked");}
checkbox.attr('class','checkboxInput');checkbox.click(function(){alertsSel.updateCounter($(this));alertsSel.updateActionHistory($(this));});e.append(checkbox);var _label=$(document.createElement('label'));_label.html(setting.label);_label.attr('for',"cb"+setting.id);e.append(_label);return e;},bucket_clicked:function bucket_clicked(e){var selectedBucket=$(e).attr('id');if(selectedBucket==this.jAbcCurrentBucket){return;}
if($(e).data('isLoaded')==undefined){$(e).data('isLoaded',true);$('#c'+selectedBucket).hide();$('#loadingIndicator').show();$.getJSON('/alertSelectionJSONController',{type:'JOURNALS_ABC',bucket:selectedBucket,time:(new Date()).getTime()},function(data){var sections=$('#c'+selectedBucket).children('div');var sectionIdx=0;var shifts=alertsSel.getSectionShifts(data.length);var shift=shifts[sectionIdx];for(var i=0;i<data.length;i++){$(sections[sectionIdx]).append(alertsSel.createLabelCheckbox(data[i]));if(shift==i){sectionIdx++;shift=shifts[sectionIdx];}}
$('#loadingIndicator').hide();$('#c'+selectedBucket).show();});}
if(this.jAbcCurrentBucket!=null){$('#c'+this.jAbcCurrentBucket).css('display','none');$('#'+this.jAbcCurrentBucket).removeClass('current');}
this.jAbcCurrentBucket=selectedBucket;$('#c'+this.jAbcCurrentBucket).css('display','block');$('#'+this.jAbcCurrentBucket).addClass('current');}};if(typeof Object.create==='undefined'){Object.create=function(o){var F=function(){};F.prototype=o;return new F();};}
function inherit(base,sub,props){var bp=base.prototype,sp=sub.prototype=Object.create(bp);sub.base=bp;bp.constructor=base;$.extend(sp,props);}
function ASAbstractTreeNode(){this.parent=null;this.isLeaf=true;this.isSelected=false;this.isSelectable=false;this.tree=null;this.nodeId=null;this.label=null;}
ASAbstractTreeNode.prototype={visitParents:function(func){var p=this.parent;while(p!=null){func(p);p=p.parent;}},setSelected:function(newSelected){if(this.isSelected===newSelected){return;}
this.isSelected=newSelected;if(this.isSelectable){var cb=(this.isLeaf?$(this.nNodeEle):$(this.nHeadEle)).children('input:checkbox');cb.prop('checked',this.isSelected);}},appendCheckbox:function(attachContainer){if(this.isSelectable){var checkboxElement=document.createElement('input');var checkbox=$(checkboxElement);checkbox.attr('id','cb'+this.id);checkbox.attr('type','checkbox');checkbox.attr('value',this.id);if(this.isSelected){checkboxElement.checked=true;checkboxElement.defaultChecked=true;checkboxElement.setAttribute("checked","checked");}
if(this.isLeaf){if(attachContainer.hasClass('flapHeadDummy')){checkbox.attr('class','inputCheckbox');}else{checkbox.attr('class','checkboxInput');}}else{checkbox.attr('class','inputCheckbox selectAll');}
var self=this;checkbox.click(function(){alertsSel.updateActionHistory($(this));self.toggleSelected();});attachContainer.append(checkbox);}},updateParentsAfterToggleSelected:function(delta){var self=this;this.visitParents(function(p){p.updateCounter(delta);if(!self.isSelected){p.setSelected(false);}else if(p.selectedCount===p.totalCount){p.setSelected(true);}});}};function ASLeafTreeNode(){ASLeafTreeNode.base.constructor.call(this);this.nNodeEle=null;}
inherit(ASAbstractTreeNode,ASLeafTreeNode,{init:function(attachContainer,settings){$.extend(this,settings);var nNode;if(this.level===2&&!this.parent.onlyLeafsIn){this.nNodeEle=document.createElement('h4');nNode=$(this.nNodeEle);nNode.attr('class','flapHeadDummy');this.appendCheckbox(nNode);nNode.append($(document.createElement('span')).html(this.label));}else{this.nNodeEle=document.createElement('div');nNode=$(this.nNodeEle);nNode.attr('class','itemFormCheckbox itemFormCheckboxNoFloat clearfix');this.appendCheckbox(nNode);var _label=$(document.createElement('label'));_label.attr('for','cb'+this.id);_label.html(this.label);nNode.append(_label);}
attachContainer.append(nNode);},toggleSelected:function(){this.setSelected(!this.isSelected);this.updateParentsAfterToggleSelected(this.isSelected?+1:-1);}});function ASFolderTreeNode(){ASFolderTreeNode.base.constructor.call(this);this.nHeadEle=null;this.isExpand=false;this.isLoaded=false;this.nBodyEle=null;this.selectedCount=0;this.totalCount=0;this.children=[];}
inherit(ASAbstractTreeNode,ASFolderTreeNode,{init:function(attachContainer,settings){var self=this;$.extend(this,settings);this.isExpand=false;this.nBodyEle=document.createElement('div');var nBody=$(this.nBodyEle);if(this.level===1){this.generateAccordionNodeHtml(nBody);}else{this.generateFlapNodeHtml(nBody);}
var nHead=$(this.nHeadEle);this.appendCheckbox(nHead);nHead.click(function(e){if(e.target.tagName.toLowerCase()!='input'){self.toggle();}});var e=$(document.createElement('span'));e.html(this.label);e.append('<div style="display:inline;text-transform:none;color:#666;"></div>');nHead.append(e);attachContainer.append(nHead);attachContainer.append(nBody);this.updateCounter(0);},generateAccordionNodeHtml:function(nBody){this.nHeadEle=document.createElement('h3');var nHead=$(this.nHeadEle);nHead.attr('class','upperCase flapHead flapHeadLarge hideContent');nBody.attr('class','subcolumns displayNone leftRightMargin10px');nBody.append($('<div class="c33l"/><div class="c33l"/><div class="c33l"/><div class="clear"/>'));},generateFlapNodeHtml:function(nBody){this.nHeadEle=document.createElement('h4');var nHead=$(this.nHeadEle);nHead.attr('class','flapHead hideContent');nBody.attr('class','leftRightMargin8px bottomMargin10px');},generateSubHeadlineHtml:function(container,setting){var e=$(document.createElement('h5'));e.attr('class',"black");e.html(setting.label);container.append(e);},expand:function(){if(this.isExpand){return;}
this.isExpand=true;this.loadChildren();$(this.nHeadEle).removeClass('hideContent');$(this.nBodyEle).removeClass('displayNone');},loadChildren:function(){if(!this.isLoaded){this.isLoaded=true;var self=this;var jsonParams={type:this.tree.alertType,parentID:this.id,time:(new Date()).getTime()};if(this.level==2&&!this.onlyLeafsIn){jsonParams.withSubLevel='true';}
$.getJSON(this.tree.rpcURL,jsonParams,function(settings){self.createChildNodes(settings);self.onPostInitChildren();});}},createChildNodes:function(settings){for(var i=0;i<settings.length;i++){var setting=settings[i];if((typeof setting.children)!='undefined'){this.generateSubHeadlineHtml(this.getHtmlContainer(i),setting);this.createChildNodes(setting.children);}else{if(this.isSelected){setting.isSelected=true;}
var n=this.createNode(i,setting,settings.length);this.children.push(n);}}},getHtmlContainer:function(idx,dataCount){var nBody=$(this.nBodyEle);var section=nBody.children('.c33l');if(section.length>0){var bucketSize=Math.ceil(dataCount/section.length);nBody=$(section[Math.floor(idx/bucketSize)]);}
return nBody;},createNode:function(idx,settings,dataCount){var n=null;var n=settings.isLeaf?new ASLeafTreeNode():new ASFolderTreeNode();n.tree=this.tree;n.parent=this;n.level=this.level+1;if(!settings.isLeaf&&settings.isSelected){settings.selectedCount=settings.totalCount;}
n.init(this.getHtmlContainer(idx,dataCount),settings);return n;},onPostInitChildren:function(){},collapse:function(){if(!this.isExpand){return;}
this.isExpand=false;$(this.nHeadEle).addClass('hideContent');$(this.nBodyEle).addClass('displayNone');},toggle:function(){if(this.isExpand){this.collapse();}else{this.expand(false);}},updateCounter:function(delta){this.selectedCount+=delta;var e=$(this.nHeadEle).find('span').find('div');if(this.selectedCount===0){e.html('');}else{if(this.level===1){e.html('&nbsp;&nbsp;&nbsp;'+this.selectedCount+'&nbsp;'+this.tree.itemSelectedSuffix);}else{e.html(' ('+this.selectedCount+')');}}},getUpdateCounterDelta:function(){if(this.isSelected){return this.totalCount-this.selectedCount;}
return-(this.selectedCount);},toggleSelected:function(){var self=this;this.setSelected(!this.isSelected);var delta=this.getUpdateCounterDelta();this.updateCounter(delta);this.visitChildren(function(n){n.setSelected(self.isSelected);if(!n.isLeaf){n.updateCounter(n.getUpdateCounterDelta());}},true);this.updateParentsAfterToggleSelected(delta);},visitChildren:function(func,deep){for(var i=0;i<this.children.length;i++){func(this.children[i]);if(deep&&!this.children[i].isLeaf){this.children[i].visitChildren(func,deep);}}}});function AlertsSelectionTree(){AlertsSelectionTree.base.constructor.call(this);}
inherit(ASFolderTreeNode,AlertsSelectionTree,{init:function(attachContainer,settings){$.extend(this,settings);this.tree=this;this.attachContainer=attachContainer;this.loadingIndicator=$(attachContainer).children(':first')[0];this.treeContent=$(attachContainer).children(':last')[0];this.nHeadEle=this.treeContent;this.nBodyEle=this.treeContent;this.level=0;this.expand();},expand:function(){this.isExpand=true;this.loadChildren();},onPostInitChildren:function(){for(var i=0;i<this.children.length;i++){var child=this.children[i];child.tree=this;if(child.isLeaf){this.selectedCount+=(child.isSelected?1:0);this.totalCount++;}else{this.selectedCount+=child.selectedCount;this.totalCount+=child.totalCount;}}
this.updateCounter(0);$(this.loadingIndicator).hide();$(this.treeContent).show();},updateCounter:function(delta){this.selectedCount+=delta;this.postUpdateCounter(this.selectedCount);document.getElementById("selectedCount").value=this.selectedCount;}});$(document).ready(function(){$('#alertsSelJournalsABC').each(function(){$('#actionHistory').attr('value','');$('#A').click();});});
$(document).ready(function(){SI.Files.stylizeAll();});
$.fn.equalLabelWidths=function(){$(this).each(function(){var currentWidest=0;var maxWidth=$(this).width()-$(this).find('.checkboxInput').width()-30;$(this).find('label').each(function(i){if($(this).width()>currentWidest){currentWidest=$(this).width();}});if(maxWidth<currentWidest)currentWidest=maxWidth;$(this).find('label').css({'width':currentWidest,'paddingRight':18,'display':'inline-block'});if($.browser.msie&&$.browser.version<='7.0'){$(this).find('label').css({'marginLeft':0,'paddingLeft':10});}
if(currentWidest==maxWidth){}});return this;};$(document).ready(function(){$(".equalWidth").equalLabelWidths();$('input.chooseAllInList').click(function(){$(this).parents('div.searchResultActions').siblings('ul.listBooks, div.searchResultActions').find('input:checkbox').prop('checked',$(this).is(':checked'));});});
$(document).ready(function(){formatTimeago();});function formatTimeago(){configureTimeago($("html").attr("lang"));$('.timeago').timeago();}
function configureTimeago(lang){switch(lang){case"de":jQuery.timeago.settings.strings={prefixAgo:"vor",prefixFromNow:"in",suffixAgo:"",suffixFromNow:"",seconds:"wenigen Sekunden",minute:"etwa einer Minute",minutes:"%d Minuten",hour:"etwa einer Stunde",hours:"%d Stunden",day:"etwa einem Tag",days:"%d Tagen",month:"etwa einem Monat",months:"%d Monaten",year:"etwa einem Jahr",years:"%d Jahren"};break;case"fr":jQuery.timeago.settings.strings={prefixAgo:"il y a",prefixFromNow:"d'ici",seconds:"moins d'une minute",minute:"environ une minute",minutes:"environ %d minutes",hour:"environ une heure",hours:"environ %d heures",day:"environ un jour",days:"environ %d jours",month:"environ un mois",months:"environ %d mois",year:"un an",years:"%d ans"};break;case"es":jQuery.timeago.settings.strings={prefixAgo:"hace",prefixFromNow:"dentro de",suffixAgo:"",suffixFromNow:"",seconds:"menos de un minuto",minute:"un minuto",minutes:"unos %d minutos",hour:"una hora",hours:"%d horas",day:"un día",days:"%d días",month:"un mes",months:"%d meses",year:"un año",years:"%d años"};break;case"it":jQuery.timeago.settings.strings={suffixAgo:"fa",suffixFromNow:"da ora",seconds:"meno di un minuto",minute:"circa un minuto",minutes:"%d minuti",hour:"circa un'ora",hours:"circa %d ore",day:"un giorno",days:"%d giorni",month:"circa un mese",months:"%d mesi",year:"circa un anno",years:"%d anni"};break;case"pt":jQuery.timeago.settings.strings={suffixAgo:"atrás",suffixFromNow:"a partir de agora",seconds:"menos de um minuto",minute:"cerca de um minuto",minutes:"%d minutos",hour:"cerca de uma hora",hours:"cerca de %d horas",day:"um dia",days:"%d dias",month:"cerca de um mês",months:"%d meses",year:"cerca de um ano",years:"%d anos"};break;case"en":default:break;}};
function getQueryVariable(variable){var query=window.location.search.substring(1);var vars=query.split("&");for(var i=0;i<vars.length;i++){var pair=vars[i].split("=");if(pair[0]==variable){return pair[1];}}
return(false);}
function trackAffiliate(affiliationKey){$.cookie('trkaff',affiliationKey,{domain:generalCookieDomain,path:'/',expires:30});}
$(document).ready(function(){var wtmcValue=getQueryVariable('wt_mc');if(wtmcValue){if(typeof trackedAffiliates!=='undefined'){wtmcValue=wtmcValue.replace('%20',' ').replace('+',' ');for(var i=0;i<trackedAffiliates.length;i++){if(wtmcValue.indexOf(trackedAffiliates[i])>-1){trackAffiliate(trackedAffiliates[i]);break;}}}}});
refinement={initDateDefaultValues:function($panel){$('.dateInput',$panel).each(function(){$(this).data('fallbackText',$(this).attr('title'));$(this).focus(function(){if($(this).val()==$(this).data('fallbackText')){$(this).val('');}});$(this).blur(function(){if($(this).val()==''){$(this).val($(this).data('fallbackText'));}});});refinement.showDateDefaultValues();},hideDateDefaultValues:function(){$('.dateInput',refinement.form).each(function(){if($(this).val()==$(this).data('fallbackText')){$(this).val('');}});},showDateDefaultValues:function(){$('.dateInput',refinement.form).each(function(){if($(this).val()==''){$(this).val($(this).data('fallbackText'));}});},hideGrayedCheckboxes:function(){$('input:checkbox',refinement.form).each(function(){if(this.disabled)this.checked=false;});},form:null,showHourGlass:function(){$('.loadingIndicator',refinement.form).removeClass('displayNone');},submit:function(){refinement.showHourGlass();refinement.hideDateDefaultValues();refinement.hideGrayedCheckboxes();$('button.refine',refinement.form).trigger('click');},refresh:function(){$('.loadingIndicator',refinement.form).addClass('displayNone');refinement.showDateDefaultValues();},init:function(){refinement.form=$('form.refinementForm');refinement.refresh();$('.linkToOpenRefinement').click(function(){var element=$('.refinementLayer')[0];if(!element){return;}
var displayAttr=$(element).css("display");var listCategory=$('#cmListCategory')[0];var htmlText=$(listCategory).html();var refineAction='Refine close';if(displayAttr=='none'){refineAction='Refine open';}
if(refinement.form&&refinement.form[0]){$('.refinementLayer').toggle('blind');}
return false;});refinement.initAll();},anyChecked:function(selector){var checked=false;$(selector).each(function(){if(this.checked){checked=true;}});return checked;},initInputBehaviour:function($panel){$panel.find('input:checkbox').click(function(){$(this).trigger('changeMe');return true;}).bind('changeMe',function(){this.disabled=($(this).data('label')&&$(this).data('label').hasClass('isActive'));});$panel.find('input.selectAll').click(function(){var selectID=this.id;this.checked=true;$('input.uncheck_on_'+selectID).prop('checked',false).trigger('changeMe');return true;}).each(function(){var selectID=this.id;$('input.uncheck_on_'+selectID).bind('changeMe',function(){if($('#'+selectID).prop('checked')==$(this).prop('checked')){$('#'+selectID).prop('checked',!refinement.anyChecked('input.uncheck_on_'+selectID));}});});$('.refineBtn',$panel).click(function(){refinement.submit();return false;});},initRefinementPanel:function(panelId){$panel=$('#'+panelId);$panel.find('label.expandable').each(function(){var $label=$(this);$label.data('refinementDetailsLayer',$label.nextAll('.refinementDetailsLayer').eq(0));var $checkbox=$('#'+$label.attr('for'));$label.data('checkbox',$checkbox);$checkbox.data('label',$label);$label.attr('for','');$label.data('refinementDetailsLayer').each(function(){$(this).find('.btnToCloseLayer').click(function(){closeCurrentLayer();return false;});$(this).find('.close').click(function(){closeCurrentLayer();return false;});var detailsLayer=$(this);$(this).find('.deselectAll').click(function(){detailsLayer.find('input:checkbox').prop('checked',false);return false;});$(this).find('.selectAll').click(function(){detailsLayer.find('input:checkbox').prop('checked',true);return false;});});$checkbox.bind('changeMe',function(event,skipActiveState){if(!skipActiveState){$(this).data('label').removeClass('isActive');$(this).data('label').data('refinementDetailsLayer').find('input:checkbox').each(function(){$(this).prop('checked',false);});}});if($label.hasClass('isActive')){$checkbox.prop('checked',true).prop('disabled',true);}}).click(function(){$(this).trigger('showMe');return false;}).bind('showMe',function(){if($(this).data('refinementDetailsLayer').is(':visible')==false){var posTop=$(this).offset().top+$('.refinementLayer').offset().top;var posLeft=$(this).offset().left+20-$('.refinementLayer').offset().left;$(this).data('refinementDetailsLayer').show();registerOpenLayer({'elementToTrigger':$(this),'eventToTrigger':'closeMe'},$(this).data('refinementDetailsLayer'));$(this).data('refinementDetailsLayer').dropShadow(shadowOptionsStandard);var checked=$(this).data('checkbox').prop('checked');$(this).data('checkbox').prop('checked',true);if(!$(this).is('.isActive')){$(this).data('refinementDetailsLayer').find('input:checkbox').each(function(){$(this).prop('checked',checked);});}
$(this).data('checkbox').trigger('changeMe',[true]);}}).bind('closeMe',function(){var checked=false;var unchecked=false;$(this).data('refinementDetailsLayer').find('input:checkbox').each(function(){if($(this).prop('checked')==true){checked=true;}else{unchecked=true;}});$(this).data('checkbox').prop('checked',checked);if(checked&&unchecked){$(this).addClass('isActive');}
else{$(this).removeClass('isActive');$(this).data('refinementDetailsLayer').find('input:checkbox').each(function(){$(this).prop('checked',false);});}
$(this).data('refinementDetailsLayer').removeShadow().hide();$(this).data('checkbox').trigger('changeMe',[true]);return false;});$panel.find('label.isActive').each(function(){$(this).data('checkbox').disabled=true;});refinement.initDateDefaultValues($panel);refinement.initInputBehaviour($panel);},initAll:function(){$('.refinementItems').each(function(index){if(!this.id){this.id='_ref_items_'+index;}
refinement.initRefinementPanel(this.id);});}};$(document).ready(refinement.init);
$(document).ready(function(){prepareExternalLinks();});function prepareExternalLinks(){$("a[rel~='external']").click(function(){this.target="_blank";return true;}).removeAttr('rel');};function linkifyTwitter(){$(".parseTwitter").linkify();};
$(document).ready(function(){setInitPadding('.col1');setInitPadding('.col2');setInitPadding('.col3');cleanPaddings();var items=$('.dndItem');if(items.length==0)
return false;$(".dndColumn").sortable({connectWith:'.dndColumn',dropOnEmpty:true,forceHelperSize:true,forcePlaceholderSize:true,tolerance:'intersection',distance:10,revert:true,cursor:'move',placeholder:'portletBoxGrey portletEmpty',grid:false,helper:'original',handle:'.dndItem .portletHeadline',deactivate:function(event,ui){},start:function(event,ui){dragStart(event,ui);reportState(event.target,ui.item[0],true,false);},stop:function(event,ui){dragStop(event,ui);},change:function(event,ui){checkHeight(event,ui);},out:checkHeight,opacity:1,update:function(event,ui){var centerId=$('.dndCenter').attr('id');var result=$(this).find('.dndItem');var ids=[];for(var i=0;i<result.length;i++){var element=result[i].parentNode;ids.push(element.id);}
var columnId=this.id;var customURL="/dragndrop/"+centerId+"/"+columnId+"/"+ids;$.ajax({type:"GET",url:customURL,data:"",dataType:"text",success:function(msg){}});reportState(event.target,ui.item[0],false,true);}});function reportState(dndColumnElement,movedElement,isStart,isUpdate){try{var cmPortletName="";cmPortletName=webtrackingGetMetaData($(movedElement).find('.cmPortletName'));if(cmPortletName==undefined||cmPortletName==""){cmPortletName=movedElement.id;}}catch(e){}}
$('.dndItem .portletHeadline').mouseover(showCursor);$('.dndItem .portletHeadline').mouseout(hideCursor);$('.dndItem .portletHeadline').mousedown(startPortletDrag);$('.dndItem .portletHeadline').mouseup(stopPortletDrag);});function showCursor(event,ui){$(this).css('cursor','move');}
function hideCursor(event,ui){$(this).css('cursor',null);}
var mousein=false;function startPortletDrag(event,ui){var result=$(this).parent('.dndItem');result.addClass('dragged');result.css('height',result.height());mousein=true;result.addClass('shadowed');}
function stopPortletDrag(event,ui){var result=$(this).parent('.dndItem');result.css('height',null);result.removeClass('dragged');mousein=false;result.removeClass('shadowed');}
function refreshShadow(event,ui){if(mousein){var oTarget=$(event.originalTarget);var draggable=$(oTarget.parents('.dndItem')[0]);var top=draggable.position().top;var shadowid=draggable.attr("shadowId");var shadow=$("#"+shadowid);shadow.css('top',(top-shadowOptionsDragAndDrop.blur)+'px');}}
function setPadding(query){}
function setInitPadding(query){var dndLastItems=$(query+' .dndComponentSelector:has(.dndItem:visible) .portlet:visible');var columns=$(query);if(typeof(dndLastItems)!='undefined'&&dndLastItems.length>0){for(var i=0;i<dndLastItems.length;i++){$(dndLastItems[i]).css('margin','0 0 14px');}
columns.css('padding-bottom','0px');}else{columns.css('padding-bottom','14px');}
columns.css('line-height','0px');}
function dragStart(event,ui){setPadding('.col1');setPadding('.col2');setPadding('.col3');if(isBrowserIE7){dndSourceColumn=$(ui.item).parent('.dndColumn');_workaroundDndRaiseZIndex(dndSourceColumn,1);}}
var dndSourceColumn;function dragStop(event,ui){setInitPadding('.col1');setInitPadding('.col2');setInitPadding('.col3');cleanPaddings();if(isBrowserIE7){_workaroundDndRaiseZIndex(dndSourceColumn,0);}}
function _workaroundDndRaiseZIndex(dndSourceColumn,switchOn){if(switchOn){dndSourceParentColumn=dndSourceColumn.parent($(".colLeft, .colRight"));dndSourceParentColumn.css('z-index','100');dndSourceColumn.css('z-index','100');}else{dndSourceColumn.css('z-index','10');dndSourceParentColumn.css('z-index','10');}}
var dndSourceParentColumn;function cleanPaddings(){var dndLastItems1=$('.col1 .dndComponentSelector:has(.dndItem:visible) .dndItem:visible');var dndLastItems2=$('.col2 .dndComponentSelector:has(.dndItem:visible) .dndItem:visible');if(typeof(dndLastItems1)!='undefined'&&dndLastItems1.length==0&&typeof(dndLastItems2)!='undefined'&&dndLastItems2.length==0){$('.col1').css('padding-bottom','0px');$('.col2').css('padding-bottom','0px');}}
function checkHeight(event,ui){setPadding('.col1');setPadding('.col2');setPadding('.col3');};
function makeVisible(element){$(element).removeClass("displayNone");$(element).addClass("displayBlock");}
function getAllPortletListItems(el){return $(el).parents('.portlet').find('ul li');}
function getAllInvisiblePortletListItems(el){return $(el).parents('.portlet').find('ul li.displayNone');}
function getAllVisiblePortletListItems(el){return $(el).parents('.portlet').find('ul li.displayBlock');}
function getPageSize(el){var pageSizeElement=$(el).parents('.portlet').find('.portletPageSizeSelector');return parseInt($(pageSizeElement).text());}
function setElementsInvisible(allElements){allElements.removeClass("displayBlock");allElements.removeClass("firstListItem");allElements.addClass("displayNone");}
function getVisibleLastIdx(allElements){var lastIdx=0;for(var i=0;i<allElements.length;i++){if($(allElements[i]).hasClass("displayBlock")){lastIdx=i;}}
return lastIdx;}
function getVisibleFirstIdx(allElements){for(var i=0;i<allElements.length;i++){if($(allElements[i]).hasClass("displayBlock")){return i;}}
return 0;}
$(document).ready(function(){registerScrolling();});function registerScrolling(){var element=$('div.portletScrollingSelector .portletNavigation .portletBackNext .greyBtnBack')
element.bind('click',backContent);var element=$('div.portletScrollingSelector .portletNavigation .portletBackNext .greyBtnNext')
element.bind('click',nextContent);}
function backContent(event){var allElements=getAllPortletListItems(this);var visibleElements=getAllVisiblePortletListItems(this);var invisibleElements=getAllInvisiblePortletListItems(this);var pageSize=getPageSize(this);var firstIdx=getVisibleFirstIdx(allElements);var lastIdx=getVisibleLastIdx(allElements);var actualPage=firstIdx/pageSize+1;if(lastIdx-pageSize<0){return false;}
setElementsInvisible(allElements);var firstElement=firstIdx-pageSize;var lastElement=firstIdx;for(var i=firstElement;i<lastElement&&i<allElements.length;i++){makeVisible(allElements[i]);}
$(allElements[firstElement]).addClass("firstListItem");var formElement=$(this).parents('.portlet').find('.portletFromSelector');$(formElement).text(''+(actualPage-1));return false;}
function nextContent(event){var allElements=getAllPortletListItems(this);var visibleElements=getAllVisiblePortletListItems(this);var invisibleElements=getAllInvisiblePortletListItems(this);var pageSize=getPageSize(this);var firstIdx=getVisibleFirstIdx(allElements);var lastIdx=getVisibleLastIdx(allElements);var actualPage=firstIdx/pageSize+1;if(lastIdx+1>=allElements.length){return false;}
setElementsInvisible(allElements);var firstElement=firstIdx+pageSize;var lastElement=firstElement+pageSize;for(var i=firstElement;i<lastElement&&i<allElements.length;i++){makeVisible(allElements[i]);}
$(allElements[firstElement]).addClass("firstListItem");var formElement=$(this).parents('.portlet').find('.portletFromSelector');$(formElement).text(''+(actualPage+1));return false;};/*	SWFObject v2.0 <http://code.google.com/p/swfobject/>
	Copyright (c) 2007 Geoff Stearns, Michael Williams, and Bobby van der Sluis
	This software is released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
*/
var swfobject=function(){var Z="undefined",P="object",B="Shockwave Flash",h="ShockwaveFlash.ShockwaveFlash",W="application/x-shockwave-flash",K="SWFObjectExprInst",G=window,g=document,N=navigator,f=[],H=[],Q=null,L=null,T=null,S=false,C=false;var a=function(){var l=typeof g.getElementById!=Z&&typeof g.getElementsByTagName!=Z&&typeof g.createElement!=Z&&typeof g.appendChild!=Z&&typeof g.replaceChild!=Z&&typeof g.removeChild!=Z&&typeof g.cloneNode!=Z,t=[0,0,0],n=null;if(typeof N.plugins!=Z&&typeof N.plugins[B]==P){n=N.plugins[B].description;if(n){n=n.replace(/^.*\s+(\S+\s+\S+$)/,"$1");t[0]=parseInt(n.replace(/^(.*)\..*$/,"$1"),10);t[1]=parseInt(n.replace(/^.*\.(.*)\s.*$/,"$1"),10);t[2]=/r/.test(n)?parseInt(n.replace(/^.*r(.*)$/,"$1"),10):0}}else{if(typeof G.ActiveXObject!=Z){var o=null,s=false;try{o=new ActiveXObject(h+".7")}catch(k){try{o=new ActiveXObject(h+".6");t=[6,0,21];o.AllowScriptAccess="always"}catch(k){if(t[0]==6){s=true}}if(!s){try{o=new ActiveXObject(h)}catch(k){}}}if(!s&&o){try{n=o.GetVariable("$version");if(n){n=n.split(" ")[1].split(",");t=[parseInt(n[0],10),parseInt(n[1],10),parseInt(n[2],10)]}}catch(k){}}}}var v=N.userAgent.toLowerCase(),j=N.platform.toLowerCase(),r=/webkit/.test(v)?parseFloat(v.replace(/^.*webkit\/(\d+(\.\d+)?).*$/,"$1")):false,i=false,q=j?/win/.test(j):/win/.test(v),m=j?/mac/.test(j):/mac/.test(v);/*@cc_on i=true;@if(@_win32)q=true;@elif(@_mac)m=true;@end@*/return{w3cdom:l,pv:t,webkit:r,ie:i,win:q,mac:m}}();var e=function(){if(!a.w3cdom){return }J(I);if(a.ie&&a.win){try{g.write("<script id=__ie_ondomload defer=true src=//:><\/script>");var i=c("__ie_ondomload");if(i){i.onreadystatechange=function(){if(this.readyState=="complete"){this.parentNode.removeChild(this);V()}}}}catch(j){}}if(a.webkit&&typeof g.readyState!=Z){Q=setInterval(function(){if(/loaded|complete/.test(g.readyState)){V()}},10)}if(typeof g.addEventListener!=Z){g.addEventListener("DOMContentLoaded",V,null)}M(V)}();function V(){if(S){return }if(a.ie&&a.win){var m=Y("span");try{var l=g.getElementsByTagName("body")[0].appendChild(m);l.parentNode.removeChild(l)}catch(n){return }}S=true;if(Q){clearInterval(Q);Q=null}var j=f.length;for(var k=0;k<j;k++){f[k]()}}function J(i){if(S){i()}else{f[f.length]=i}}function M(j){if(typeof G.addEventListener!=Z){G.addEventListener("load",j,false)}else{if(typeof g.addEventListener!=Z){g.addEventListener("load",j,false)}else{if(typeof G.attachEvent!=Z){G.attachEvent("onload",j)}else{if(typeof G.onload=="function"){var i=G.onload;G.onload=function(){i();j()}}else{G.onload=j}}}}}function I(){var l=H.length;for(var j=0;j<l;j++){var m=H[j].id;if(a.pv[0]>0){var k=c(m);if(k){H[j].width=k.getAttribute("width")?k.getAttribute("width"):"0";H[j].height=k.getAttribute("height")?k.getAttribute("height"):"0";if(O(H[j].swfVersion)){if(a.webkit&&a.webkit<312){U(k)}X(m,true)}else{if(H[j].expressInstall&&!C&&O("6.0.65")&&(a.win||a.mac)){D(H[j])}else{d(k)}}}}else{X(m,true)}}}function U(m){var k=m.getElementsByTagName(P)[0];if(k){var p=Y("embed"),r=k.attributes;if(r){var o=r.length;for(var n=0;n<o;n++){if(r[n].nodeName.toLowerCase()=="data"){p.setAttribute("src",r[n].nodeValue)}else{p.setAttribute(r[n].nodeName,r[n].nodeValue)}}}var q=k.childNodes;if(q){var s=q.length;for(var l=0;l<s;l++){if(q[l].nodeType==1&&q[l].nodeName.toLowerCase()=="param"){p.setAttribute(q[l].getAttribute("name"),q[l].getAttribute("value"))}}}m.parentNode.replaceChild(p,m)}}function F(i){if(a.ie&&a.win&&O("8.0.0")){G.attachEvent("onunload",function(){var k=c(i);if(k){for(var j in k){if(typeof k[j]=="function"){k[j]=function(){}}}k.parentNode.removeChild(k)}})}}function D(j){C=true;var o=c(j.id);if(o){if(j.altContentId){var l=c(j.altContentId);if(l){L=l;T=j.altContentId}}else{L=b(o)}if(!(/%$/.test(j.width))&&parseInt(j.width,10)<310){j.width="310"}if(!(/%$/.test(j.height))&&parseInt(j.height,10)<137){j.height="137"}g.title=g.title.slice(0,47)+" - Flash Player Installation";var n=a.ie&&a.win?"ActiveX":"PlugIn",k=g.title,m="MMredirectURL="+G.location+"&MMplayerType="+n+"&MMdoctitle="+k,p=j.id;if(a.ie&&a.win&&o.readyState!=4){var i=Y("div");p+="SWFObjectNew";i.setAttribute("id",p);o.parentNode.insertBefore(i,o);o.style.display="none";G.attachEvent("onload",function(){o.parentNode.removeChild(o)})}R({data:j.expressInstall,id:K,width:j.width,height:j.height},{flashvars:m},p)}}function d(j){if(a.ie&&a.win&&j.readyState!=4){var i=Y("div");j.parentNode.insertBefore(i,j);i.parentNode.replaceChild(b(j),i);j.style.display="none";G.attachEvent("onload",function(){j.parentNode.removeChild(j)})}else{j.parentNode.replaceChild(b(j),j)}}function b(n){var m=Y("div");if(a.win&&a.ie){m.innerHTML=n.innerHTML}else{var k=n.getElementsByTagName(P)[0];if(k){var o=k.childNodes;if(o){var j=o.length;for(var l=0;l<j;l++){if(!(o[l].nodeType==1&&o[l].nodeName.toLowerCase()=="param")&&!(o[l].nodeType==8)){m.appendChild(o[l].cloneNode(true))}}}}}return m}function R(AE,AC,q){var p,t=c(q);if(typeof AE.id==Z){AE.id=q}if(a.ie&&a.win){var AD="";for(var z in AE){if(AE[z]!=Object.prototype[z]){if(z=="data"){AC.movie=AE[z]}else{if(z.toLowerCase()=="styleclass"){AD+=' class="'+AE[z]+'"'}else{if(z!="classid"){AD+=" "+z+'="'+AE[z]+'"'}}}}}var AB="";for(var y in AC){if(AC[y]!=Object.prototype[y]){AB+='<param name="'+y+'" value="'+AC[y]+'" />'}}t.outerHTML='<object classid="clsid:D27CDB6E-AE6D-11cf-96B8-444553540000"'+AD+">"+AB+"</object>";F(AE.id);p=c(AE.id)}else{if(a.webkit&&a.webkit<312){var AA=Y("embed");AA.setAttribute("type",W);for(var x in AE){if(AE[x]!=Object.prototype[x]){if(x=="data"){AA.setAttribute("src",AE[x])}else{if(x.toLowerCase()=="styleclass"){AA.setAttribute("class",AE[x])}else{if(x!="classid"){AA.setAttribute(x,AE[x])}}}}}for(var w in AC){if(AC[w]!=Object.prototype[w]){if(w!="movie"){AA.setAttribute(w,AC[w])}}}t.parentNode.replaceChild(AA,t);p=AA}else{var s=Y(P);s.setAttribute("type",W);for(var v in AE){if(AE[v]!=Object.prototype[v]){if(v.toLowerCase()=="styleclass"){s.setAttribute("class",AE[v])}else{if(v!="classid"){s.setAttribute(v,AE[v])}}}}for(var u in AC){if(AC[u]!=Object.prototype[u]&&u!="movie"){E(s,u,AC[u])}}t.parentNode.replaceChild(s,t);p=s}}return p}function E(k,i,j){var l=Y("param");l.setAttribute("name",i);l.setAttribute("value",j);k.appendChild(l)}function c(i){return g.getElementById(i)}function Y(i){return g.createElement(i)}function O(k){var j=a.pv,i=k.split(".");i[0]=parseInt(i[0],10);i[1]=parseInt(i[1],10);i[2]=parseInt(i[2],10);return(j[0]>i[0]||(j[0]==i[0]&&j[1]>i[1])||(j[0]==i[0]&&j[1]==i[1]&&j[2]>=i[2]))?true:false}function A(m,j){if(a.ie&&a.mac){return }var l=g.getElementsByTagName("head")[0],k=Y("style");k.setAttribute("type","text/css");k.setAttribute("media","screen");if(!(a.ie&&a.win)&&typeof g.createTextNode!=Z){k.appendChild(g.createTextNode(m+" {"+j+"}"))}l.appendChild(k);if(a.ie&&a.win&&typeof g.styleSheets!=Z&&g.styleSheets.length>0){var i=g.styleSheets[g.styleSheets.length-1];if(typeof i.addRule==P){i.addRule(m,j)}}}function X(k,i){var j=i?"visible":"hidden";if(S){c(k).style.visibility=j}else{A("#"+k,"visibility:"+j)}}return{registerObject:function(l,i,k){if(!a.w3cdom||!l||!i){return }var j={};j.id=l;j.swfVersion=i;j.expressInstall=k?k:false;H[H.length]=j;X(l,false)},getObjectById:function(l){var i=null;if(a.w3cdom&&S){var j=c(l);if(j){var k=j.getElementsByTagName(P)[0];if(!k||(k&&typeof j.SetVariable!=Z)){i=j}else{if(typeof k.SetVariable!=Z){i=k}}}}return i},embedSWF:function(n,u,r,t,j,m,k,p,s){if(!a.w3cdom||!n||!u||!r||!t||!j){return }r+="";t+="";if(O(j)){X(u,false);var q=(typeof s==P)?s:{};q.data=n;q.width=r;q.height=t;var o=(typeof p==P)?p:{};if(typeof k==P){for(var l in k){if(k[l]!=Object.prototype[l]){if(typeof o.flashvars!=Z){o.flashvars+="&"+l+"="+k[l]}else{o.flashvars=l+"="+k[l]}}}}J(function(){R(q,o,u);if(q.id==u){X(u,true)}})}else{if(m&&!C&&O("6.0.65")&&(a.win||a.mac)){X(u,false);J(function(){var i={};i.id=i.altContentId=u;i.width=r;i.height=t;i.expressInstall=m;D(i)})}}},getFlashPlayerVersion:function(){return{major:a.pv[0],minor:a.pv[1],release:a.pv[2]}},hasFlashPlayerVersion:O,createSWF:function(k,j,i){if(a.w3cdom&&S){return R(k,j,i)}else{return undefined}},createCSS:function(j,i){if(a.w3cdom){A(j,i)}},addDomLoadEvent:J,addLoadEvent:M,getQueryParamValue:function(m){var l=g.location.search||g.location.hash;if(m==null){return l}if(l){var k=l.substring(1).split("&");for(var j=0;j<k.length;j++){if(k[j].substring(0,k[j].indexOf("="))==m){return k[j].substring((k[j].indexOf("=")+1))}}}return""},expressInstallCallback:function(){if(C&&L){var i=c(K);if(i){i.parentNode.replaceChild(L,i);if(T){X(T,true);if(a.ie&&a.win){L.style.display="block"}}L=null;T=null;C=false}}}}}();
$(document).ready(function(){$('.articleTrackingTooltipTrigger').each(function(){$(this).data('isActive',false);}).mousemove(function(eventObject){var hoverStartX=$(this).offset().left;var hoverEndX=hoverStartX+($(this).width()/8)+5;if(eventObject.clientX>hoverStartX&&eventObject.clientX<hoverEndX){if($(this).data('isActive')==false){$(this).find('.tooltip').show().dropShadow(shadowOptionsStandard);$(this).data('isActive',true);$(this).addClass('isActive');}
else{var leftPos=eventObject.clientX-$(this).offset().left;$(this).find('.tooltip').css('left',leftPos).redrawShadow();}}
else{$(this).trigger('mouseout');}}).mouseout(function(){$(this).find('.tooltip').removeShadow().hide();$(this).data('isActive',false);$(this).removeClass('isActive');});});
function centerPopup(popup_name,popup_url,popup_width,popup_height,myWidth,myHeight){if(!myWidth){myWidth=10;}
if(!myHeight){myHeight=50;}
var popup_left=(window.screen.width/2)-(popup_width/2+myWidth);var popup_top=(window.screen.height/2)-(popup_height/2+myHeight);var fenster=window.open(popup_url,popup_name,"toolbar=1,location=0,status=0,menubar=0,scrollbars=1,resizable=0,width="+popup_width+",height="+popup_height+",left="+popup_left+",top="+popup_top+",screenX="+popup_left+",screenY="+popup_top);if((typeof fenster)!='undefined'&&fenster!=null){fenster.focus();}}
function _checkIsBrowserIE7(){return navigator.userAgent.toLowerCase().indexOf("msie 7")>0;}
var isBrowserIE7=_checkIsBrowserIE7();function _checkIsBrowserIE8(){return navigator.userAgent.toLowerCase().indexOf("msie 8")>0;}
var isBrowserIE8=_checkIsBrowserIE8();function webtrackingSetProduction(){var isProduction=_webtrackingIsProduction;if(isProduction){}}
function webtrackingGetMetaData(anchorAsJqueryObj){var classString=anchorAsJqueryObj.attr("class");var regex=/wtdata_(\S*)/i;var metaData=regex.exec(classString);if(metaData&&metaData[1])
return metaData[1];else
return"";}
function webtrackingGetPageId(){return _webtrackingPageId;}
function webtrackingGetCategoryId(){return _webtrackingCategoryId;}
function _webtrackingTrimElementId(id){if(id.length>=49){id=id.substring(0,49);}
return id;}
function webtrackingTriggerPageElementEvent(pageElementName,categoryId){try{var elementId=webtrackingGetPageId();if(pageElementName!=undefined&&pageElementName.toString().length>0){elementId+="-"+pageElementName;}
return 1;}catch(e){return-1;}}
function webtrackingTriggerPageElementTag(elementName,categoryId,usePageId,pageIdBeforeEle,separator){try{if(elementName!=undefined&&elementName.toString().length>0){if(usePageId==undefined){usePageId=false;}
if(pageIdBeforeEle==undefined){pageIdBeforeEle=false;}
if(separator==undefined){separator="_";}
var elementId;if(usePageId){var pageId=webtrackingGetPageId();if(pageIdBeforeEle){elementId=pageId+separator+elementName;}else{elementId=elementName+separator+pageId;}}else{elementId=elementName;}
return 1;}}catch(e){}
return-1;}
function removesPortletIfNoContent(portletId){$("#"+portletId+":empty").remove();}
(function(){try{var orgFunc=Wicket.Ajax.Request.prototype.createUrl;Wicket.Ajax.Request.prototype.createUrl=function(){var url=orgFunc.apply(this)+'&wicket:ajax=true';return url;};}catch(e){}})();/*!
Math.uuid.js (v1.4)
http://www.broofa.com
mailto:robert@broofa.com

Copyright (c) 2010 Robert Kieffer
Dual licensed under the MIT and GPL licenses.
*/

/*
 * Generate a random uuid.
 *
 * USAGE: Math.uuid(length, radix)
 *   length - the desired number of characters
 *   radix  - the number of allowable values for each character.
 *
 * EXAMPLES:
 *   // No arguments  - returns RFC4122, version 4 ID
 *   >>> Math.uuid()
 *   "92329D39-6F5C-4520-ABFC-AAB64544E172"
 * 
 *   // One argument - returns ID of the specified length
 *   >>> Math.uuid(15)     // 15 character ID (default base=62)
 *   "VcydxgltxrVZSTV"
 *
 *   // Two arguments - returns ID of the specified length, and radix. (Radix must be <= 62)
 *   >>> Math.uuid(8, 2)  // 8 character ID (base=2)
 *   "01001010"
 *   >>> Math.uuid(8, 10) // 8 character ID (base=10)
 *   "47473046"
 *   >>> Math.uuid(8, 16) // 8 character ID (base=16)
 *   "098F4D35"
 */
(function() {
  // Private array of chars to use
  var CHARS = '0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'.split(''); 

  Math.uuid = function (len, radix) {
    var chars = CHARS, uuid = [];
    radix = radix || chars.length;

    if (len) {
      // Compact form
      for (var i = 0; i < len; i++) uuid[i] = chars[0 | Math.random()*radix];
    } else {
      // rfc4122, version 4 form
      var r;

      // rfc4122 requires these characters
      uuid[8] = uuid[13] = uuid[18] = uuid[23] = '-';
      uuid[14] = '4';

      // Fill in random data.  At i==19 set the high bits of clock sequence as
      // per rfc4122, sec. 4.1.5
      for (var i = 0; i < 36; i++) {
        if (!uuid[i]) {
          r = 0 | Math.random()*16;
          uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
        }
      }
    }

    return uuid.join('');
  };

  // A more performant, but slightly bulkier, RFC4122v4 solution.  We boost performance
  // by minimizing calls to random()
  Math.uuidFast = function() {
    var chars = CHARS, uuid = new Array(36), rnd=0, r;
    for (var i = 0; i < 36; i++) {
      if (i==8 || i==13 ||  i==18 || i==23) {
        uuid[i] = '-';
      } else if (i==14) {
        uuid[i] = '4';
      } else {
        if (rnd <= 0x02) rnd = 0x2000000 + (Math.random()*0x1000000)|0;
        r = rnd & 0xf;
        rnd = rnd >> 4;
        uuid[i] = chars[(i == 19) ? (r & 0x3) | 0x8 : r];
      }
    }
    return uuid.join('');
  };

  // A more compact, but less performant, RFC4122v4 solution:
  Math.uuidCompact = function() {
    return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {
      var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);
      return v.toString(16);
    }).toUpperCase();
  };
})();

function moveValue(title,titleholder){document.getElementById(titleholder).value=document.getElementById(title).innerHTML;}
function updateFullTitle(fullTitleInput,titleholder1,titleholder2){var sapAcademicTitle1=document.getElementById(titleholder1).value;var sapAcademicTitle2=document.getElementById(titleholder2).value;var commaValue=sapAcademicTitle1!=''&&sapAcademicTitle2!=''?', ':'';var fullTitle=sapAcademicTitle1+commaValue+sapAcademicTitle2;document.getElementById(fullTitleInput).value=fullTitle!=''?fullTitle:'Please select';}
function resetValue(title,titleholder){document.getElementById(titleholder).value='';var titles=$('.'+title);for(i=0;i<titles.length;i++){if(titles[i].checked==true){titles[i].checked=false;}}};
var allow_ui_during_ajax=false;var blockUiTimeout=8000;$(document).ready(setupFunc);function setupFunc(){Wicket.Ajax.registerPreCallHandler(disableButton);Wicket.Ajax.registerPostCallHandler(enableButton);Wicket.Ajax.registerFailureHandler(fallbackToUrl);}
function disableButton(){if(typeof(allow_ui_during_ajax)!='undefined'){if(allow_ui_during_ajax){return;}}
document.body.style.cursor='wait';$.blockUI({message:null,overlayCSS:{backgroundColor:'#000',opacity:0.00},showOverlay:true});setTimeout($.unblockUI,blockUiTimeout);}
function enableButton(){if(typeof(refinement)!='undefined'&&refinement.refining){return;}
$.unblockUI();document.body.style.cursor='default';}
function fallbackToUrl(){}
function reload(){};
$(document).ready(function(){$('.springerCmsArticle table tr:first-child').addClass('headRow');$('.springerCmsArticle table tr:nth-child(2n)').addClass('evenRow');$('.springerCmsArticle table tr:nth-child(2n+3)').addClass('oddRow');$('.springerCmsArticle table tr:last-child').addClass('lastRow');$('.springerCmsArticle table td:first-child').addClass('firstCell');$('.springerCmsArticle table td:last-child').addClass('lastCell');$('.springerCmsArticle ul').each(function(){if($(this).find('li').length<5){$(this).addClass('bigListIcons');}else{$(this).addClass('smallListIcons');}});var currentHost=document.location.protocol+'//'+document.location.host;$('.springerCmsArticle a').each(function(){if($(this).prop('href').indexOf(currentHost)==0){$(this).addClass('internal');}else{this.target="_blank";$(this).addClass('external');}});});