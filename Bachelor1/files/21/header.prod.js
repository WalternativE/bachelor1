/**
* hoverIntent r6 // 2011.02.26 // jQuery 1.5.1+
* <http://cherne.net/brian/resources/jquery.hoverIntent.html>
* 
* @param  f  onMouseOver function || An object with configuration options
* @param  g  onMouseOut function  || Nothing (use configuration options object)
* @author    Brian Cherne brian(at)cherne(dot)net
*/
(function($){$.fn.hoverIntent=function(f,g){var cfg={sensitivity:7,interval:100,timeout:0};cfg=$.extend(cfg,g?{over:f,out:g}:f);var cX,cY,pX,pY;var track=function(ev){cX=ev.pageX;cY=ev.pageY};var compare=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);if((Math.abs(pX-cX)+Math.abs(pY-cY))<cfg.sensitivity){$(ob).unbind("mousemove",track);ob.hoverIntent_s=1;return cfg.over.apply(ob,[ev])}else{pX=cX;pY=cY;ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}};var delay=function(ev,ob){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t);ob.hoverIntent_s=0;return cfg.out.apply(ob,[ev])};var handleHover=function(e){var ev=jQuery.extend({},e);var ob=this;if(ob.hoverIntent_t){ob.hoverIntent_t=clearTimeout(ob.hoverIntent_t)}if(e.type=="mouseenter"){pX=ev.pageX;pY=ev.pageY;$(ob).bind("mousemove",track);if(ob.hoverIntent_s!=1){ob.hoverIntent_t=setTimeout(function(){compare(ev,ob)},cfg.interval)}}else{$(ob).unbind("mousemove",track);if(ob.hoverIntent_s==1){ob.hoverIntent_t=setTimeout(function(){delay(ev,ob)},cfg.timeout)}}};return this.bind('mouseenter',handleHover).bind('mouseleave',handleHover)}})(jQuery);/**
 * All functions for the header
 *
 * Dependencies: jquery-1.3.2.min.js,jquery-ui-1.7.2.core.min.js,jquery-ui-1.7.2.effects.min.js
 */
jQuery(function($){

	/** MANIPULATE DOM **/
	positioningSubNavigation();

	/** header navigation **/
	showSubnavigationLayers();
	registerMainNavigationResponsiveClick();
	
	addReturnUrlForLinks();
	
	/* Menu on dropdown buttons
	------------------------------------------------------------------------------*/
	$('.pillow-btn, .pillow-btn-header')
		.not('.pillow-btn-processed')
		.click(function() {
			$(this).toggleClass('pillow-btn-active');
		})
		.addClass('pillow-btn-processed');
	$('.open-menu').click(function() {
		$('body').toggleClass('show-menu');
	});
	$('.open-search').click(function() {
		$('body').toggleClass('show-search');
	});
	$('.open-legal')
		.not('.open-legal-processed')
		.click(function(){
			$("body").toggleClass("show-legal")
		}).addClass('open-legal-processed');
	
    /* Flyout
	------------------------------------------------------------------------------*/
	var flyOutconfig = {
      over: function(){} , // function = onMouseOver callback (REQUIRED)
      timeout: 500, // number = milliseconds delay before onMouseOut
      out: function(){
         if ($('input:focus', this).length <= 0) {
             $(this).removeClass('is-open').find('.pillow-btn-active').removeClass('pillow-btn-active');
             $('body').removeClass('show-footer-menu');
             $('.flyout-caption',this).blur();
         }
      }
    };

    $('.flyout').click(function(e){
          // force flyout to open/close also on click (for touch devices)
          if($(this).hasClass('is-open')){
              if(!$('form',this).length){
                $(this).removeClass('is-open');
              }
          }
          else{
            $(this).addClass('is-open');
        }
        e.stopPropagation(); // don't let event bubble up to document (for closing by clicking outside)
     }).hoverIntent(flyOutconfig);

     $(document).click(function(){
       $('.flyout.is-open').removeClass('is-open').find('.pillow-btn-active').removeClass('pillow-btn-active');
     });
     // close flyout on click outside

    function addReturnUrlForLinks() {
    	$('a.returnURL').each(function(){
    		if (!this.search.match(/[&?]returnURL=/)) {
    			this.search += (this.search ? '&' : '?') + 'returnURL=' + encodeURIComponent(window.location.href); 
    		}
    	});
    }
    
	$('.logoutButton').click(
		function(e) {
			e.preventDefault();			
			var domain = window.location.hostname;
			var simUserCookie = $.cookie('sim-user-token');
			if (console) {
				console.log("simUserCookie: " + simUserCookie);
			}
			if (simUserCookie === "undefined") {
				if (console) {
					console.log("simUserCookie is undefined");
				}
				return;
			}
			var domainParts = window.location.hostname.split(".");
			for (var startIndex=0; startIndex<domainParts.length; startIndex++) {
				var domain = "";
				for (var i=startIndex; i<domainParts.length; i++) {
					domain += "." + domainParts[i]; 
				}
				if (console) {
					console.log("domain: " + domain);
				}
				removeUserTokenCookies(domain);				
				removeUserTokenCookies(domain.replace(/^[.]/,''));				
			}
			if(window.location.search.indexOf('IFormSubmitListener') > 0) {
				// do not resubmit forms on logout
				window.location = window.location.href.split('?')[0];
			} else {
				window.location.reload();
			}
		}
	);
    
});

function reloadHeader(callback) {
	$('header').load(setParamValue($('header').data('src'), 'sim-user-token', $.cookie('sim-user-token')), callback);
}

function removeUserTokenCookies(domain){
	$.removeCookie('sim-user-token',{ domain : domain, path: '/' });
	$.removeCookie('UID',{ domain : domain, path: '/' });
	$.removeCookie('spcomLoggedIn',{ domain : domain, path: '/' });
	$.removeCookie('beechwood_authentication',{ domain : domain, path: '/' });
}
function setParamValue(url, key, value){
	key = encodeURIComponent(key);
	value = encodeURIComponent(value || "");
	var regexp = new RegExp('([&?]'+key+'=)[^&]*');
	if (url.match(regexp)) {
		return url.replace(regexp, '$1'+value);
	} else {
		return url + (url.indexOf('?') > 0 ? '&' : '?') + key + '=' + value;
	}
}

function showSubnavigationLayers(){
	
	var mainNavConfig = {
		timeout: 300, // number = milliseconds delay before onMouseOver / onMouseOut
		over: function() {
			$(this).addClass('open');
		},
		out: function() {
			$(this)
				.removeClass('open');
		}
	};

	$('nav li.mainNavigationMark').hoverIntent(mainNavConfig);
	
	// close nav layers on click outside of layers; needed for devices without onmouseout (tablets)
	$('body').on('click', function() {
		$('nav li.mainNavigationMark').removeClass('open').removeClass('open-clicked');
	});
}

function positioningSubNavigation() {
	// position the main navigation layer
	var navWidth = $("nav").width();
	var layerMarginRight = 10;
	if($('.lt-ie8').length == 0) {
		$("nav > ul > li").each(function() {
			var $this = $(this);
			var $layer = $('.nav-sub', $this);
			var offsetLabel = $this.position().left;
			var widthLabel = $this.width();
			var widthLayer = $layer.width();
			if(offsetLabel + widthLayer > navWidth) {
				$layer.css('left', 'auto');
				$layer.css('right', '-' + (navWidth - offsetLabel - widthLabel - layerMarginRight) + 'px');
			}
		});
	}
}

function registerMainNavigationResponsiveClick() {
	$('.mainNavigationLinkMark').on('click', function(e) {
		var $li = $(this).closest('.mainNavigationMark');
		if($li.find('.nav-sub').length > 0) {
			e.preventDefault();
			var $lis = $li.closest('#global-nav').children();
			if($li.hasClass('open-clicked')) {
				$li.removeClass('open-clicked');
			} else {
				$li.addClass('open-clicked');
			}
			return false;
		}
	});
}

/**
 * Check for currently clicking on a a-href-link.
 * Used by {@link com.artnology.sgw.cda.general.behaviors.ObfuscatedLinkBehavior}
 * <p>
 * see e.g. http://stackoverflow.com/questions/4762254/javascript-window-location-does-not-set-referer-in-the-request-header
 * @param url URL to go to
 * @param anchor HTML link element, if the user currently clicks on
 * @return true if clicked on anchor (thus anchor will handle clicking itself),
 * 	       false otherwise
 */
function goURL(url, anchor) {
	if (anchor && (anchor.tagName === 'A')) {
		// if clicking on a link then really click on that link to have the browser's referrer set correctly
		anchor.href = url;
		return true;
	} else {
		// in any other case just set window.location.href
		window.location.href = url;
		return false;
	}
}
