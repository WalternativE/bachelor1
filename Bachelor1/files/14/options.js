fb.options = {
//
// EDIT ONLY WITHIN THE FOUR OPTION BLOCKS BELOW.
// See the instructions for information about setting floatbox options.
// See the options reference for details about all the available options.
// Commas are required after each entry except the last entry in a block.
// A comma on the last entry in a block will cause the option settings to fail in some browsers.

// globalOptions is where you enter a license key and change default floatbox options site-wide.
// Use the configurator.html form in this folder to generate globalOptions preferences through form selections.

globalOptions: {
  // Paste your license key between the quotes below, or enter it in the configurator form.
  // (Separate multiple license keys with spaces or commas.)
 licenseKey: "ptf1oduQptfE cG8Oh2BRins@ y4SAwpfBzY6R j4bRfZ2OiYrA 842wDYjUCoKh leJBb$dSlucH Gz5bJy4gJi9q D0Ss$V7r@FOd IP8WEuNtL$4n 7@hn1fYm6@dW j5KOcV$KfJzA j5iQiIrKfYyB BW1vAHgUCXRe XSQKVipFZzo7 SkicT0HmSkGt fqwSg69Tfa4D PqAiQqtjPKoz XigKYC1LXiA7 HdPVHtqUINvk PIigSovhNIqx r$Q2kOJ3ruMH D2gu8WhvD2Uf qti0mML1psOF mxi4qQH5lwSJ 84WwCoLy9Ymh ptT1qtK0mcXE f6xThLY5grpC TmYeMmdqTnMv FSIaEStbIDQr EPkhGz5bFzNq @aRr@q4y8a0j kQi8axjJXBf4 TIznPkGcSkKt 6Bam2c@l7dDU $rIxOq4x9b1i 7xbp0RSp7wDY 1PJn6@Uy7@1W OKkiNKljPaYz $5zx8JKb95Dg fYzRcpq7dZjA MmMeRHRhS28v 7eAm0eZn6@AX urgCwKNDurkT HxDZLxmYERzo Ht@VHtqUINvk nse0mML1psOF zlj9vFS8wlHM zki8uET9xkGN TmAeMmxfTGkv DpPxDZew85bg zoeAyIPBtoKR vpjBzJOAspLQ voiAyIPBtoKR frxTh78Seb5C Pl@dPlycQFns Pk$cOkzdREmt jVjNf1CMgV38 jXhPd3AOiX1$ TZPhTpugMJrw TYOgSovhNIqx fYiQi4@RdY6B @aAyAadz@6Yj LNjVHtqUINvk rOA2kOJ3ruMH rR@5rRG4kxTI 3Swq4iVr3Cgb HSgaIylbHSwr nM$0mML1psOF 79jl3dak49fU 7$gm0eZn7$cX b$AGU$5Hbe83 3Bjp7hWo0BjY XBDJbB2IUhD4 XCAKYC1LXiA7 H8uUGsrVJMul T2weMmxfTGkv j2AOc2BPjW0@ P7xjRrsiOLpy P6wiQqtjPKoz f1@Nf1CMgV38 P0ucOkzdREmt KtCUGsrVJMul qsj1nNK0otPE auxHV@4Gaf92 KuBXFvoWKPtm 6xOo6gXp1AiZ yng$sGR@zmEP yrcCwKNDvqIT upiAyIPBtoKR eluMe0DNhU29 adiEW87FZc$1 6eBn1fYm6@dW KhOYKwnZFQyp 2hCo6gXp1AiZ mix7pTE6mzRK GiRbJzkaGTxq GdOUGsrVJMul 2d$k2cbl58eV mcv1nNK0otPE eZiQi4@RdY6B $ZCwCYfx94ah $bAyAadz@6Yj $aBzBbcy$7Zi uVu8uET9xkGN eUfNf1CMgV38 rtb1qtm@qtTE qxj5pwW4qhDI znc$unh@tW0P HcTVFM6UG9fk 2yEq4Sxk4jEb jXUOcmFQc3w@ TXEeM2VgMnAv ETEaJypgHigr d5nLyYiAsImR y03$h1f5xEqN dXd6zHtLj2Y@ tqNJf6pCxqkT v1P9vFS8xkrM 79jl3dak58DU HzBbJzkaGTxq 7Aeo6gXp1AiZ mgz5rRG4kxTI hX5PfWwIdHx$ j3VPd2sIdn1$ Kw6YKAjWKx2p TmAeMmxfSHIv 3Bjp7hWo1A@Y 6P1n1fIc1P9W XAnJbBmCbxj4 b8nFX9q$Xtf0 2Aeo4BHp7QyZ 6gWo0hHv0RKZ FflXHO4gFf5m PaBjNaIlRbJy kzV7mzg1kiVK m8G0ktbOosOF REXiBU@oOkat VihLUjYLVjN6 CoPv9Eft9oqh TZzhTp6aO5bw uofBuoe$zYjQ A3gWB3Zv9nle TEvUMImgSYax eZvEcY2QiIKB TmZkN38eMWUv KNHVINOUGcel XRSLkQqGbRT4 NaNZOWtXN6cz 4dHl5MCn79@U 8mcuB2hxDGgf Brhz9q4VO7Ri A3Nv9XYoCmRe R7AiQqlVPb8z Rl3ZCU$sCkud Pq4y96Bo96Uj oyc2WDgPmT5K 7wOy3tWg3BfY rTtRUi5@YeA3 TlDdPlycQFns zlj9vFS8xkrM fX83cn8Hfb1C odS$oBKRnNTE qBr@lcHGrwOJ xKc0unMGsbJS 97tz$rgs86Ri UPxNYS58U$83 jUyMg1zNf1a9 m8G0mNX@m9iF 7eYm0eli7f0X j4$Qi4@RdY6B hKdTcb8Mer9C 1BigExWy7RbY bhaIawTGaBm5 D3ZvBW0w9Xhe WCZLZDI@WCZ6 ArgqOqQYBLhi z1bDh1fHuUON dH9PgX45i3V$ R6JjRq8UO7xy iJmQi4T0d5iB Rr5jMLAUO61y RGoeRHRqRH8v D1jt@VisB07c A3hvCncw8HFe zns$yXdBzWgP tqxDsLM8vahS XhrJVhWOkhj4 HRvZERqYLhLo Ks$UEN7VH8el $kSs81Tt@Emd ynd@vmg$sX1O mS86qzV7piAL OLdjTK8iQ7Zy jpyQgZvRjoKB 7c@l18Kk2NvU rgz5pwW4qhDI 7gDp5AGo6RzY 7hCo4BHp7QyZ DU@t90Ss$Fnc jVeMcVzNfkG9 DVHtBFOZD13c TY6gM4DkOIKx Lh7ZFB6tHRTo D2kuCGNaA20f ipaQcoz0eYaB bcvFW92FZtn0 gnBPgXAQinB$ MHsoNrJjOq9y MHsqOqJjOq9y 97Nz9rIX97Ri It3VHd$XItjk cYWQiIrSd4GB 8Z3xDpKz8Zng IAqYKgDaFwep Fx2YKgDaFwep xboCw6ZAvK0T Bb4yAqJw@akj ViVLZTwJWDN6 5i5r5zQp2jta OpCgSovhNIqx gbdTcbgTdbFC v5TBzJOAspLQ 6$cm2@Jn1OsX TYmgSojkNpSx ca0Sg7tFfqAD x1m8uFj7xUON J9GUGt7jJ8ml gYuQfZfPcoyB g7NTcacMfrxC IMyULNLbI82l WBWIaBrPawC5 avlHVvMJV$p2 eZXRjpKPj4vA OIWgS4beSp$x KNXVH9ebHs7k $4WwCIruCZOh nyV7pjA1pSVK Hz0aIChUIz0r bulHVvMJV$p2 7uFn1Psp1eJW qM60rsXDmdWF fFnNf1O5g0f8 LM6UGsXwJtGl rMa0mM3QpNmF rOY2kO1SrPkH X9XFX9GBY8X0 n8m0mM3QpNmF jX5Pd3M7i2d$ fY6Qi4T0d5iB KP4WEuVyLvEn WQrJbBK9UAb4 WToKYCJ$XDY7 $65zBbwX$aBi u6pDxLAHuKxS u0n9vF$JwEvM 2C5r5jof2i5a B1TtC1iyAEnc 2MHl79ur3NXU 9oqw@5Tb9Ymh GMDVHt$eK9fk NK1jPq8UR6By JM3VJs6iH83k Zf9HV@kGYvd2 KT4aFS5dFj0r rOM2pOJ5lfUH IMOUG8PfIsOl 3SIq4j9q2DEb TlHdR1$pTFHs D16sD0Pr$l6d OELdPlCmP0@s $b4y96Bo96Uj qOwMa$Y9ketG Z$lHa$wFZeZ2 3g@n6wGo2h2Z oCV1neF9mzkL 4zBr5zAh2zBa ZfUGUOtEb$Q3 XBbJbBK9UAb4 OJ6gSojkNpSx mMX1nN2RoMnE z20$yWdKwmkP",
 showIE6EndOfLife: false
},

// childOptions are preferences to be applied only to secondary floatboxes.
// Additional options can be added to this list,
// or remove an option to have it revert to the default or global setting.

childOptions: {
  cornerRadius: 8,
  shadowSize: 8,
  padding: 16,
  overlayOpacity: 45,
  resizeDuration: 3,
  imageFadeDuration: 3,
  overlayFadeDuration: 0
},

// Option settings can be assigned based on floatbox content type.
// The syntax of typeOptions is different than the object code above.
// Each type is assigned an option string formatted the same as
// the options placed in a link's data-fb-options (or rev) attribute.
// example - iframe: "color:blue showNewWindow:true",
// Javascript Object Notation can be used instead of the option string.

typeOptions: {
  image: "",
  // html settings apply to all 4 html sub-types that follow
  html: "",
    iframe: "",
    inline: "",
    ajax: "",
    direct: "",
  // media settings apply to all 6 media sub-types that follow
  media: "",
    video: "",
    flash: "",
    quicktime: "",
    wmp: "",
    silverlight: "",
    pdf: "width:80% height:95%",
  // tooltip and context settings can consist of task-specific options in addition to the standard floatbox options
  tooltip: "",
  context: ""
},

// You can propogate settings to groups of anchors by assigning them a class
// and associating that class name with a string or object of options here.
// The syntax is the same as for typeOptions above.

classOptions: {
  alt: {
    cornerRadius: 8,
    shadowType: "hybrid",
    showClose: false,
    showOuterClose: true,
    captionPos: "bc",
    caption2Pos: "tl",
    infoLinkPos: "tl",
    printLinkPos: "tl",
    newWindowLinkPos: "tl",
    controlsPos: "tr",
    centerNav: true
  },
  transparent: {
    boxColor: "transparent",
    contentBackgroundColor: "transparent",
    roundCorners: "none",
    shadowType: "none",
    showOuterClose: true,
    showClose: false,
    overlayOpacity: 75,
    outerBorder: 2,
    innerBorder: 0,
    zoomBorder: 0
  },
  naked: {
    roundCorners: "none",
    showOuterClose: true,
    showClose: false,
    inFrameResize: false,
    showItemNumber: false,
    navType: "overlay",
    showNavOverlay: "yes",
    caption: null,
    outerBorder: 0,
    innerBorder: 0,
    padding: 0,
    panelPadding: 0,
    zoomBorder: 0
  }
},

// END OF EDITABLE CONTENT
// ***********************
ready: true};
