/*
 * The contents of this file are subject to the license and copyright
 * detailed in the LICENSE and NOTICE files at the root of the source
 * tree and available online at
 *
 * http://www.dspace.org/license/
 */

// Collapsible menu code for the Mirage theme.
//

$(document).ready(function(){
//community/collection hierarchy 

    //expansion with the plus sign (or horizontal arrow) 
    $("span.ListPlus").click(function(){ 
        $(this).hide();
        $(this).next("span.ListMinus").show(); 
        $(this).parent().children("ul").slideDown("fast"); 
    });

    //contraction with the minus sign (or vertical arrow) 
    $("span.ListMinus").click(function(){ 
        $(this).hide();
        $(this).prev("span.ListPlus").show();
        $(this).parent().children("ul").slideUp("fast"); 
    });

//hides list items initially
$("span.ListMinus").parent().children("ul").hide();

});
