﻿// browser selector 
function css_browser_selector(u)
{ var ua = u.toLowerCase(), is = function(t) { return ua.indexOf(t) > -1; }, g = 'gecko', w = 'webkit', s = 'safari', h = document.getElementsByTagName('html')[0], b = [(!(/opera|webtv/i.test(ua)) && /msie\s(\d)/.test(ua)) ? ('ie ie' + RegExp.$1) : is('firefox/2') ? g + ' ff2' : is('firefox/3') ? g + ' ff3' : is('gecko/') ? g : /opera(\s|\/)(\d+)/.test(ua) ? 'opera opera' + RegExp.$2 : is('konqueror') ? 'konqueror' : is('chrome') ? w + ' chrome' : is('applewebkit/') ? w + ' ' + s + (/version\/(\d+)/.test(ua) ? ' ' + s + RegExp.$1 : '') : is('mozilla/') ? g : '', is('j2me') ? 'mobile' : is('iphone') ? 'iphone' : is('ipod') ? 'ipod' : is('mac') ? 'mac' : is('darwin') ? 'mac' : is('webtv') ? 'webtv' : is('win') ? 'win' : is('freebsd') ? 'freebsd' : (is('x11') || is('linux')) ? 'linux' : '', 'js']; c = b.join(' '); h.className += ' ' + c; return c; }; css_browser_selector(navigator.userAgent);

// *********************************************** jquery show hide
function ShowHide(caller, content) 
{
    var $callerObj = null;
    $callerObj = $("#" + caller);

    var $contentObj = null;
    $contentObj = $("#" + content);

    $callerObj.hide();
    $contentObj.css({ 'display': 'block' });
}

//more/less to complete a paragrah
function MoreLess(more, less, control)
{
    if (document.getElementById(more).className == 'more-less-hide')
    {
        document.getElementById(more).className = 'more-less-show';
        document.getElementById(less).className = 'more-less-hide';
    }
    else
    {
        document.getElementById(more).className = 'more-less-hide';
        document.getElementById(less).className = 'more-less-show';
    }

    if (document.getElementById(control).innerHTML == 'More')
    {
        document.getElementById(control).className = 'more-less-control-less';
        document.getElementById(control).innerHTML = 'Less';
    }
    else
    {
        document.getElementById(control).className = 'more-less-control-more';
        document.getElementById(control).innerHTML = 'More';
    }

}

//jquery toggle content
function ToggleContent(which, img) 
{

    var $callerObj = null;
    $callerObj = $("#" + which);

    var $imgObj = null;
    $imgObj = $("#" + img);

    //toggle
    $callerObj.toggle();

    //set header to active state
    if ($("#header-" + which).attr("class") == "collasped" ||  $("#header-" + which).attr("class") == ""){
        $("#header-" + which).removeClass('collasped');
        $("#header-" + which).addClass('expanded');
    }
    else {
        $("#header-" + which).removeClass('expanded');
        $("#header-" + which).addClass('collasped');
    }
    
    if ($imgObj.attr("class") == "header-expanded-img") 
    {
        $imgObj.removeClass('header-expanded-img');
        $imgObj.addClass('header-collapsed-img');
    }
    else 
    {
        $imgObj.removeClass('header-collapsed-img');
        $imgObj.addClass('header-expanded-img');
    }
}

//jquery toggle content design specifically for the products section
function ToggleContentProduct(which, img) 
{
    var $callerObj = null;
    $callerObj = $("#" + which);

    var $imgObj = null;
    $imgObj = $("#" + img);

    //toggle
    $callerObj.toggle();

    if ($imgObj.attr("src") == "/images/buttons/opt-collapsed-gray.png") 
        $imgObj.attr("src", "/images/buttons/opt-expanded.blue.png");
    else 
        $imgObj.attr("src", "/images/buttons/opt-collapsed-gray.png");
}

//use to hide e-mail alias from e-mail bots
function HideEmail(address, domain) 
{
    var email = address + "@" + domain;
    document.location.href = "MailTo:" + email;

}

//text counter to limit characters
function TextCounter(fieldid, cntfieldid, maxlimit) {
    var field = document.getElementById(fieldid);
    var cntfield = document.getElementById(cntfieldid);
    
    if (field.value != null && field.value.length != null) 
    {
        if (field.value.length > maxlimit) 	// if too long...trim it!
            field.value = field.value.substring(0, maxlimit);
        // otherwise, update 'characters left' counter
        else
            cntfield.value = maxlimit - field.value.length;
    } 
    else 
        cntfield.value = maxlimit;
}

//this is for the solutions areas vertical tabs style
$(document).ready(function () {

    // Make Dyanamic Webinar Banners Clickable and have the cursor change on mouseover
    $('.DynamicWebinarBanner').click(function () {
        if ($(this).attr("url").indexOf("http") != -1) {
            window.open($(this).attr("url"));
        }
        else {
            window.location = $(this).attr("url");
        }
        return false;
    });
    $('.DynamicWebinarBanner').hover(function () {
        $(this).css('cursor', 'pointer');
    }, function () {
        $(this).css('cursor', 'auto');
    });

    $('.DynamicWebinarBannerSmall').click(function () {
        if ($(this).attr("url").indexOf("http") != -1) {
            window.open($(this).attr("url"));
        }
        else {
            window.location = $(this).attr("url");
        }
        return false;
    });
    $('.DynamicWebinarBannerSmall').hover(function () {
        $(this).css('cursor', 'pointer');
    }, function () {
        $(this).css('cursor', 'auto');
    });

    var totalItems = $("ul .vTabs  h3").length; //total items
    //display the default
    var tabIndex1 = $("ul .vTabs  h3").index($(".selected"));
    var $tabContent1 = $("#content-" + tabIndex1);
    var $tabVideo1 = $("#video-" + tabIndex1);
    $tabContent1.css("display", "block");
    $tabVideo1.css("display", "block");

    //for items in second tab of solution area (space missions)
    //display the default
    var tabIndex2 = $("ul .vTabs  h3").index($(".selected2"));
    var $tabContent2 = $("#content-" + tabIndex2);
    var $tabVideo2 = $("#video-" + tabIndex2);
    $tabContent2.css("display", "block");
    $tabVideo2.css("display", "block");

    //alert(tabIndex);

    //on mouse click event
    $("ul .vTabs  h3").click(function () {
        var tabIndex = $("ul .vTabs  h3").index(this);

        var $tabContent = $("#content-" + tabIndex);
        $tabContent.css("display", "block"); //display the corresponding content

        var $tabVideo = $("#video-" + tabIndex);
        $tabVideo.css("display", "block"); //display the corresponding video


        if (tabIndex < 4) {
            $(this).addClass("selected"); //make the item selected
            //hide everything else
            for (var i = 0; i < 4; i++) {
                if (i != tabIndex) {
                    $("#content-" + i).css("display", "none");
                    $("#video-" + i).css("display", "none");
                    $("#tab-" + i).removeClass("selected");
                }
            }
        }
        else {
            $(this).addClass("selected2");
            //hide everything else
            for (var i = 4; i < 8; i++) {
                if (i != tabIndex) {
                    $("#content-" + i).css("display", "none");
                    $("#video-" + i).css("display", "none");
                    $("#tab-" + i).removeClass("selected2");
                }
            }
        }
    });

});

////for anchor text methods
//$(function(){
//  
//  // Keep a mapping of url-to-container for caching purposes.
//  var cache = {
//    // If url is '' (no fragment), display this div's content.
//    '': $('.anchor-default')
//  };
//  
//  // Bind an event to window.onhashchange that, when the history state changes,
//  // gets the url from the hash and displays either our cached content or fetches
//  // new content to be displayed.
//  $(window).bind( 'hashchange', function(e) {
//    
//    // Get the hash (fragment) as a string, with any leading # removed. Note that
//    // in jQuery 1.4, you should use e.fragment instead of $.param.fragment().
//    var url = $.param.fragment();
//    
//    // Remove .anchor-current class from any previously "current" link(s).
//    $( 'a.anchor-current' ).removeClass( 'anchor-current' );
//    
//    // Hide any visible ajax content.
//    $( '.anchor-content' ).children( ':visible' ).hide();
//    
//    // Add .anchor-current class to "current" nav link(s), only if url isn't empty.
//    url && $( 'a[href="#' + url + '"]' ).addClass( 'anchor-current' );
//    
//    if ( cache[ url ] ) {
//      // Since the element is already in the cache, it doesn't need to be
//      // created, so instead of creating it again, let's just show it!
//      cache[ url ].show();
//      
//    } else {
//      // Show "loading" content while AJAX content loads.
//      $( '.anchor-loading' ).show();
//      
//      // Create container for this url's content and store a reference to it in
//      // the cache.
//      cache[ url ] = $( '<div class="anchor-item"/>' )
//        
//        // Append the content container to the parent container.
//        .appendTo( '.anchor-content' )
//        
//        // Load external content via AJAX. Note that in order to keep this
//        // example streamlined, only the content in .infobox is shown. You'll
//        // want to change this based on your needs.
//        .load( url, function(){
//          // Content loaded, hide "loading" content.
//          $( '.anchor-loading' ).hide();
//        });
//    }
//  })
//  
//  // Since the event is only triggered when the hash changes, we need to trigger
//  // the event now, to handle the hash the page may have loaded with.
//  $(window).trigger( 'hashchange' );

//});

//jquery toggle content design specifically for the products section
function ToggleUCTicker() {
   
    $tickerObj = $("#uc-ticker");
	$tickerImg = $("#ucTickerImg");
	
	 if ($tickerImg.attr("src") == "/images/products/by-capability/uc-ticker-bg_03.jpg") 
        $tickerImg.attr("src", "/images/products/by-capability/uc-ticker-arrow.png");
    else 
        $tickerImg.attr("src", "/images/products/by-capability/uc-ticker-bg_03.jpg");
		
    //toggle
    $tickerObj.toggle();
}

//jquery to get url paramter values
function getUrlVars() {
    var vars = [], hash;
    var hashes = window.location.href.slice(window.location.href.indexOf('?') + 1).split('&');

    for (var i = 0; i < hashes.length; i++) {
        hash = hashes[i].split('='); vars.push(hash[0]); vars[hash[0]] = hash[1];
    }

    return vars;
}

//check if on mobile devicce
function mobilecheck() {
    var check = false;
    (function (a) { if (/(android|bb\d+|meego).+mobile|avantgo|bada\/|blackberry|blazer|compal|elaine|fennec|hiptop|iemobile|ip(hone|od)|iris|kindle|lge |maemo|midp|mmp|mobile.+firefox|netfront|opera m(ob|in)i|palm( os)?|phone|p(ixi|re)\/|plucker|pocket|psp|series(4|6)0|symbian|treo|up\.(browser|link)|vodafone|wap|windows (ce|phone)|xda|xiino/i.test(a) || /1207|6310|6590|3gso|4thp|50[1-6]i|770s|802s|a wa|abac|ac(er|oo|s\-)|ai(ko|rn)|al(av|ca|co)|amoi|an(ex|ny|yw)|aptu|ar(ch|go)|as(te|us)|attw|au(di|\-m|r |s )|avan|be(ck|ll|nq)|bi(lb|rd)|bl(ac|az)|br(e|v)w|bumb|bw\-(n|u)|c55\/|capi|ccwa|cdm\-|cell|chtm|cldc|cmd\-|co(mp|nd)|craw|da(it|ll|ng)|dbte|dc\-s|devi|dica|dmob|do(c|p)o|ds(12|\-d)|el(49|ai)|em(l2|ul)|er(ic|k0)|esl8|ez([4-7]0|os|wa|ze)|fetc|fly(\-|_)|g1 u|g560|gene|gf\-5|g\-mo|go(\.w|od)|gr(ad|un)|haie|hcit|hd\-(m|p|t)|hei\-|hi(pt|ta)|hp( i|ip)|hs\-c|ht(c(\-| |_|a|g|p|s|t)|tp)|hu(aw|tc)|i\-(20|go|ma)|i230|iac( |\-|\/)|ibro|idea|ig01|ikom|im1k|inno|ipaq|iris|ja(t|v)a|jbro|jemu|jigs|kddi|keji|kgt( |\/)|klon|kpt |kwc\-|kyo(c|k)|le(no|xi)|lg( g|\/(k|l|u)|50|54|\-[a-w])|libw|lynx|m1\-w|m3ga|m50\/|ma(te|ui|xo)|mc(01|21|ca)|m\-cr|me(rc|ri)|mi(o8|oa|ts)|mmef|mo(01|02|bi|de|do|t(\-| |o|v)|zz)|mt(50|p1|v )|mwbp|mywa|n10[0-2]|n20[2-3]|n30(0|2)|n50(0|2|5)|n7(0(0|1)|10)|ne((c|m)\-|on|tf|wf|wg|wt)|nok(6|i)|nzph|o2im|op(ti|wv)|oran|owg1|p800|pan(a|d|t)|pdxg|pg(13|\-([1-8]|c))|phil|pire|pl(ay|uc)|pn\-2|po(ck|rt|se)|prox|psio|pt\-g|qa\-a|qc(07|12|21|32|60|\-[2-7]|i\-)|qtek|r380|r600|raks|rim9|ro(ve|zo)|s55\/|sa(ge|ma|mm|ms|ny|va)|sc(01|h\-|oo|p\-)|sdk\/|se(c(\-|0|1)|47|mc|nd|ri)|sgh\-|shar|sie(\-|m)|sk\-0|sl(45|id)|sm(al|ar|b3|it|t5)|so(ft|ny)|sp(01|h\-|v\-|v )|sy(01|mb)|t2(18|50)|t6(00|10|18)|ta(gt|lk)|tcl\-|tdg\-|tel(i|m)|tim\-|t\-mo|to(pl|sh)|ts(70|m\-|m3|m5)|tx\-9|up(\.b|g1|si)|utst|v400|v750|veri|vi(rg|te)|vk(40|5[0-3]|\-v)|vm40|voda|vulc|vx(52|53|60|61|70|80|81|83|85|98)|w3c(\-| )|webc|whit|wi(g |nc|nw)|wmlb|wonu|x700|yas\-|your|zeto|zte\-/i.test(a.substr(0, 4))) check = true })(navigator.userAgent || navigator.vendor || window.opera);
    return check;
}