﻿var link;
var resourceId;
var process;
var buttonType;
var trackingId = 0;
var actionId = 0;

$(document).ready(function () {
    //country dropdown onchange
    $("#drpGuestCountries").change(function () {
        $("#pnlGuestOther").hide();
        var selectedValue = $("#drpGuestCountries").val();

        //get states
        if (selectedValue != "") {
            GetStates(selectedValue);
            GetOrganizations(selectedValue);
        }
    }); //end country dropdown onchange

    //organization dropdown onchange
    $("#drpGuestOrganizations").change(function () {
        var selectedValue = $("#drpGuestOrganizations").val();

        if (selectedValue == "Other")
            $("#pnlGuestOther").show();
        else
            $("#pnlGuestOther").hide();
    }); //end organization dropdown onchange

    //close button click
    $("#btnGuestLoginClose").click(function () {
        $("#guestLogin").slideUp("slow");
    }); //end close button click

    //stk sla
//    $('#stkSLAAgreeBtn').click(function () {
//        location.href = '/products/stk/license/?install=1&referral=' + location.href;
//    });
//    $('#stkSLADisagreeBtn').click(function () {
//        $('#mask').hide();
//        $('.window').hide();
//    });

    $('#txtGuestSignInEmail').keypress(function (e) {
        if (e.keyCode == 13) { // enter key was pressed
            code = e.keyCode ? e.keyCode : e.which;
            if (code.toString() == 13) {
                //SignIn();
                return false;
            }

        }

    });

    $('#txtGuestSignInPassword').keypress(function (e) {
        if (e.keyCode == 13) { // enter key was pressed
            code = e.keyCode ? e.keyCode : e.which;
            if (code.toString() == 13) {
                e.preventDefault();
               // SignIn();
                return false;
            }
        }
    });
});  

//checks to see if user is logged in, called on product explorer pages
function IsUserLoggedIn(_link, _resourceId, _process, _buttonType) {
    link = _link;
    resourceId = _resourceId;
    process = _process;
    buttonType = _buttonType;
    
    accountServices.IsUserLoggedIn(IsUserLoggedInSuccess, OnFailure);
}//end IsUserLoggedIn

//callback for IsUserLoggedIn
function IsUserLoggedInSuccess(result) {
    //if user is not logged in then show guest login panel
    if (!result) {
        GetAllCountries();
        $("html,body").scrollTop(0);
        $("#guestLogin").slideDown("slow");
        ClearLoginForms();
    }
    else {
        //user is already logged in
        
        ProcessRequest();
    }
}//end IsUserLoggedInSuccess

//checks if user is an agier
function IsUserAGIer() {
    //check see if user is logged in
    accountServices.IsAGIer(IsUserAGIerSuccess, OnFailure);
}

//callback for IsUserAGIer
function IsUserAGIerSuccess(result) {
    //if user is not logged in then show item delete option
    if (result) {
        $(".r-remove").show();
        if ($('#addCodeSnippetsSamples') != null) {
            $('#addCodeSnippetsSamples').show();
            $('.AGIUSER').show();
        }  
    }
    else {
        $(".r-remove").hide();
        if ($('#addCodeSnippetsSamples') != null) {
            $('#addCodeSnippetsSamples').hide();
            $('.AGIUSER').hide();
        }  
    }
} //end IsUserAGIierSuccess

//get all countries
function GetAllCountries() {
    accountServices.GetCountries(GetCountriesSuccess, OnFailure);
}//end get all countries

//callback for GetAllCountries
function GetCountriesSuccess(results) {
    //set the countries dropdown
    $('#drpGuestCountries').empty();
    $('#drpGuestCountries').append('<option value="">- Select a country -</option>');

    for (var i = 0; i < results.length; i++) {
        
            $('#drpGuestCountries').append('<option value="' + results[i]["chCountryCode"] + '">' + results[i]["chCountryDesc"] + '</option>');
        
    }
  
}//end GetCountriesSuccess

//get states
function GetStates(countryCode) {
    accountServices.GetStates(countryCode,GetStatesSuccess, OnFailure);
}//end get states

//get states callback
function GetStatesSuccess(results) {
    var selectedValue = $("#drpGuestCountries").val();
    $('#drpGuestStates').empty();

    //set the states dropdown
    if (results != null) {
        $('#drpGuestStates').show();
        $('#txtGuestTerritory').hide();

        //if country is US
        if (selectedValue == "US") {

            $('#drpGuestStates').append('<option value="">- Select a state -</option>');

            for (var i = 0; i < results.length; i++) {
                $('#drpGuestStates').append('<option value="' + results[i]["StateCode"] + '">' + results[i]["StateName"] + '</option>');
            }
            //change lable
            $("#lblGuestStates").html("State:");
        }
        else if (selectedValue == "CA") {
            $('#drpGuestStates').append('<option value="">- Select a province -</option>');

            for (var i = 0; i < results.length; i++) {
                $('#drpGuestStates').append('<option value="' + results[i]["ProvinceCode"] + '">' + results[i]["ProvinceName"] + '</option>');
            }
            //change lable
            $("#lblGuestStates").html("Province:");
        }
    }
    else {
        $('#drpGuestStates').hide();
        $('#txtGuestTerritory').show();

        
        //change lable
        $("#lblGuestStates").html("City:");
    }

}//end GetStatesSuccess

//get organizations
function GetOrganizations(countryCode) {
    accountServices.GetOrganizations(countryCode, GetOrganizationSuccess, OnFailure);
} //end GetOrganizations

//get organization callback
function GetOrganizationSuccess(results) {
    $('#drpGuestOrganizations').empty();
    $('#drpGuestOrganizations').append('<option value="">- Select an organization -</option>');

    for (var i = 0; i < results.length; i++) {
        $('#drpGuestOrganizations').append('<option value="' + results[i]["Name"] + '">' + results[i]["Name"] + '</option>');
    }
}//end GetOrganizationSuccess

//verify user
function SignIn() {
    //verify user
    $("body").css("cursor", "progress")
    if ($("#txtGuestSignInEmail").val() != "" && $("#txtGuestSignInPassword").val() != "") {
        accountServices.VerifyUser($("#txtGuestSignInEmail").val(), $("#txtGuestSignInPassword").val(), VerifyUserSuccess, OnFailure);
    }
    else {
        alert("Incorrect e-mail/password.");
        $("body").css("cursor", "pointer")
    }

} //end verify user

//verify user callback
function VerifyUserSuccess(results) {
    //login successfull
    if (results == 0) {
        $("#guestLogin").slideUp("slow");
        UpdateLoginButtons();

        if (trackingId > 0 && actionId > 0)
            ProcessInsertCampaign()
        else
            ProcessRequest();
    }
    else if (results == 4 || results == 5) {
        location.href = '/includes/restrictions.aspx';
    }
    else {
        alert("Incorrect e-mail/password.");
    }

    //set cursor back to default
    $("body").css("cursor", "default")
}//end VerifyUserSuccess

//on failure callback
function OnFailure(results) {
    //alert("An error has occurred and your request could not be processed at this time." + results);
    //alert(results);
    //set cursor back to default
    $("body").css("cursor", "pointer")
}

//update login buttons
function UpdateLoginButtons() {
    //update login buttons
    $("#ctl00_headerControl_lnkEdit").attr("class", "edit");
    $("#ctl00_headerControl_lnkLogin").attr("class", "logout");
    $("#ctl00_headerControl_lnkLogin").attr("href", "/my-account/logout.aspx");
}//end UpdateLoginButtons

//process request
function ProcessRequest() {
    //set cursor back to default
    $("body").css("cursor", "progress")
    if (typeof (link) == "function") {
        link();
    }
    else if (link != "" && buttonType != "limelight") {
        location.href = link; //there is a referall link, do redirect
    }
    else {
        //there is no referal link, determin process and get resource for download
        
        switch (process) {
        
            case "productExplorerProductItem": GetProductExplorerItem("productItem"); break;
            case "productExplorerRelatedItem": GetProductExplorerItem("relatedItem"); break;
            case "downloadItem": GetProductExplorerItem("downloadItem"); break;
            case "downloadSTKDIYVideos": GetProductExplorerItem("downloadSTKDIYVideos"); break;
            case "tiremLibRequestJava": TiremRequest("Java"); break;
            case "tiremLibRequestNET": TiremRequest(".NET"); break;
            case "generateLicense": GenerateLicense(); break;
            case "codeSnippetSampleItems": GetProductExplorerItem("codeSnippetSampleItems"); break;
            case "stkStarkeyInstall": GetSTKStarkeyInstall(); break;
            case "stkStarkeyScenario": GetSTKStarkeyScenario(); break;
        }
    }
}//end process request

//get product explorer related item
function GetProductExplorerItem(itemType) {
    if (itemType == "relatedItem")
        productExplorerServices.GetProductExplorerRelatedItem(resourceId, GetProductExplorerItemSuccess, OnFailure);
    else if (itemType == "downloadItem")
        accountServices.DownloadFile("http://downloads.agi.com/s/products/", link, DownloadFileSuccess, OnFailure);
    else if (itemType == "downloadSTKDIYVideos")
        accountServices.DownloadFile("http://downloads.agi.com/s/video/", link, DownloadFileSuccess, OnFailure);
    else if (itemType == "codeSnippetSampleItems") {
        accountServices.DownloadFile("http://agi.vo.llnwd.net/v1/s/resources/code-snippets-samples/", resourceId, DownloadFileSuccess, OnFailure);

    } else
        productExplorerServices.GetProductExplorerProduct(resourceId, GetProductExplorerItemSuccess, OnFailure);
} //end GetProductExplorerRelatedItem()

//GetProductExplorerItem callback
function GetProductExplorerItemSuccess(results) {
    GetItemLink(results);
}//end GetProductExplorerItemSuccess


function GetSTKStarkeyInstall() {
    $("body").css("cursor", "default")
    location.href = '/products/stk/license/?install=2&referral=' + location.href;
}

function GetSTKStarkeyScenario() {
    accountServices.DownloadFile("https://sdf10.agi.com/content/files/Public/STK%20Scenarios/Example%20Scenarios/", 'Starkey_The_Elf.vdf', DownloadFileSuccess, OnFailure);
}

//get related item link
function GetItemLink(item) {
    var link = "";
    var filePath = "http://downloads.agi.com/s/products/";
    var productCode = 0;

    //determine which button was click
    switch (buttonType) {
        case "try": link = item["_tryLink"]; productCode = item["_tryCode"]; break;
        case "download": link = item["downloadLink"]; productCode = item["downloadCode"]; break;
    }

    if (link != null && link != "" ) {
        if (link.indexOf("http:") > -1 || link.indexOf("https") > -1) {
            window.open(link);
        }
        else {
            if (link.indexOf("/") > -1) {
                //its a link on agi.com
                location.href = link;
            }
            else {
                //its a file on limelight
                accountServices.DownloadFile(filePath, link, DownloadFileSuccess, OnFailure);
            }
        }
        $("body").css("cursor", "default")
    }
    else {
        //if its the stk download
        if (productCode == 'stk') {
            $("body").css("cursor", "default")
            location.href = '/products/stk/license/?install=1&referral=' + location.href;
            //launch sla
            //launchWindow('#stkSLADialog');
        }
        else
        //there is a download code for tracking
        accountServices.DownloadFile(filePath, productCode, DownloadFileSuccess, OnFailure);

    }
} //end GetRelatedItemLink

function DownloadFileSuccess(results) {
    //if results contains licMsg that means its a component request and license file has been denied eval request message
    if(results.indexOf('licMsg=1') > -1)
        launchWindow('#stkComponentsLicDeniedMsg');

    location.href = results;
    $("body").css("cursor", "default")
}

//tirem request
function TiremRequest(requestType) {
    accountServices.SendTiremRequest(requestType, TiremRequestSuccess, OnFailure);
  
} //end GetProductExplorerRelatedItem()

//GetProductExplorerItem callback
function TiremRequestSuccess(results) {
    alert("Thank you for requesting the STK Components Kit with the TIREM Extension.  An AGI Systems Engineer will be contacting you soon to deliver the requested libraries.\n\nIf you have any additional questions, please feel free to contact our Technical Support team at support@agi.com or by phone: 1.800.924.7244 or 1.610.981.8888");
} //end GetProductExplorerItemSuccess

//show/hide term of use info
function TermsOfUse() {
    $("#pnlTermsOfUse").toggle();
    if ($("#aGuestTermsOfUse").html() == "Terms of Use") {
        $("#aGuestTermsOfUse").html("Hide Terms of Use");
        $("#termsOfUse").attr("class", "hideTermsOfUse");
    }
    else {
        $("#aGuestTermsOfUse").html("Terms of Use");
        $("#termsOfUse").attr("class", "termsOfUse");
    }
} //end terms of use

//dont agree click
function DontAgree() {
    $("#guestLogin").slideUp("slow");
}

//agree button clicked
function Agree() {
    $("body").css("cursor", "progress")
    var first = $("#txtGuestFirst").val();
    var last = $("#txtGuestLast").val();
    var email = $("#txtGuestEmailNew").val();
    var country = $("#drpGuestCountries").val();
    var state = "";
    var organization = $("#drpGuestOrganizations").val();
    var other = $("#txtGuestOther").val();
    var interest = $("#drpGuestInterest").val();
    
    if ($("#lblGuestStates").html() == "State:" || $("#lblGuestStates").html() == "Province:") 
        state = $("#drpGuestStates").val();
    else
        state = $("#txtGuestTerritory").val();

    //make sure form is completed
    if (first == "") {alert("Please enter the first name."); $("body").css("cursor", "default"); return;};
    if (last == "") {alert("Please enter the last name."); $("body").css("cursor", "default"); return;};
    if (email == "" || email == "youremail@example.com") {alert("Please enter the e-mail address."); $("body").css("cursor", "default"); return;};
    if (country == "") {alert("Please select a country."); $("body").css("cursor", "default"); return;};
    if (state == "") {alert("Please select a state/province or enter a territory."); $("body").css("cursor", "default"); return;};
    if (organization == "") {alert("Please select an organization."); $("body").css("cursor", "default"); return;};
    if (interest == "") {alert("Please select an area of interest."); $("body").css("cursor", "default"); return;};
    if (organization == "Other" && other == "") { alert("Please enter the other field"); $("body").css("cursor", "default"); return; };

    if (organization == "Other" && other != "") { organization = other; $("body").css("cursor", "default");};

    if (resourceId == "") resourceId = 0;

    //validate email
    if (!IsValidEmail(email)) { alert("Please provide a valid e-mail address."); $("body").css("cursor", "default"); return; };

    //create new account
    accountServices.CreateNewUser(first, last, email, country, state, organization, interest, resourceId, CreateNewUserSuccess, OnFailure);
}//end Agree

//Create new user call back
function CreateNewUserSuccess(results) {
    if (results == "") {
        $("#guestLogin").slideUp("slow");
        UpdateLoginButtons();

        if (trackingId > 0 && actionId > 0)
            ProcessInsertCampaign();
        else
            ProcessRequest();
    }
    else{
        if(results.indexOf("/") > -1)
            location.href = results;
        else
            alert(results);
    }

    $("body").css("cursor", "default")
    
}//end CreateNewUserSuccess

//validate email address format
function IsValidEmail(emailAddress) {
    var pattern = new RegExp(/^[+a-zA-Z0-9._'-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,4}$/i);
    return pattern.test(emailAddress);
};

//clear the login forms
function ClearLoginForms() {
    $("#txtGuestSignInEmail").val("");
    $("#txtGuestSignInPassword").val("");
    $("#txtGuestFirst").val("");
    $("#txtGuestLast").val("");
    $("#txtGuestEmailNew").val("");
    $("#drpGuestCountries").val("");
    $("#drpGuestStates").val("");
    $("#txtGuestTerritory").val("");
    $("#drpGuestOrganizations").val("");
    $("#txtGuestOther").val("");
    $("#drpGuestInterest").val("");


}//end ClearLoginForms

//capture enter key
function DoClick(buttonName, e) {
    //the purpose of this function is to allow the enter key to 
    //point to the correct button to click.
    if (e.which == 13 || e.keyCode == 13) {
        //Get the button the user wants to have clicked
        var btn = document.getElementById(buttonName);

        if (btn != null) { //If we find the button click it
            SignIn();
            try { e.preventDefault(); } catch (ex) { } //wrap around try/catch cause ie doesnt suppor preventdefault
        }
        
    }
} //end doClick

//insert a campaign
function InsertCampaign(_trackingId, _actionId) {
    trackingId = _trackingId;
    actionId = _actionId;

    //check to see if user is logged in
    accountServices.IsUserLoggedIn(InsertCampaignSuccess, OnFailure);
} //end InsertCampaign

//insert campaign callback
function InsertCampaignSuccess(result) {
    //if user is not logged in then show guest login panel
    if (!result) {
        GetAllCountries();
        $("html,body").scrollTop(0);
        $("#guestLogin").slideDown("slow");
        ClearLoginForms();
    }
    else {
        //user is already logged in
        ProcessInsertCampaign();
    }
} //end insert campaign call back

//insert the campaign
function ProcessInsertCampaign() {
    //insert campaign
    accountServices.InsertWebCampaign(trackingId, actionId, ProcessInsertCampaignSuccess, OnFailure);
}

//process insert campaign callback
function ProcessInsertCampaignSuccess(result) {
    if (result)
        alert("Thank you for your request.");
    else
        alert("An error has occurred and your request can not be processed at this time. Please try again later or contact support@agi.com.");
}


