\chapter{Results}
\section{Mission Control Server}
The mission control server has to fulfill a set of different jobs:
\begin{itemize}
	\item{Allowing \acp{GCC} to register with the communication network}
	\item{Allowing the \ac{MCC} to register satellites}
	\item{Scheduling passive tracking jobs automatically}
	\item{Allowing \ac{MCC} to schedule active tracking jobs}
	\item{Supplying interface for polling jobs and uploading mission data}
\end{itemize}
This functionality can be achieved by either adjusting the existing \ac{BOINC} server software or implementing a web application which supplies the clients with the aforementioned functions.

\subsection{Adjustment of BOINC infrastructure}
The Berkeley University distributes the source code for the \ac{BOINC} server, manager and client \ac{API} as an open source project on the official \ac{BOINC} website (\url{http://boinc.berkeley.edu/}). It can easily be cloned from Berkeleys git repository following the guide on \url{http://boinc.berkeley.edu/trac/wiki/SourceCodeGit}. To compile only the server applications and scientific application \ac{API} the commands in listing \ref{lstlst:configure_boinc_src} have to be executed.

\begin{lstlisting}[caption={This listing states how to configure the \ac{BOINC} source code to compile the server applications and the client \ac{API} in order to start a new \ac{BOINC} project assuming that the project was cloned into a directory called boinc-src within the home directory of the Linux server},language={bash},label={lstlst:configure_boinc_src}]
$ cd ~/boinc-src
$ ./_autosetup
$ ./configure --disable-client --disable-manager
$ make

\end{lstlisting}
Subsequently using a shell script supplied with the source code a new project can be registered on the server. The script guides the user through the installation process but only configures the most vital parts of the \ac{BOINC} service. The project administrator has to register the servers web directories with the Apache web server (\ac{BOINC} does not officially support any other web servers). A default virtual host file is supplied but users of the current Apache version (v2.4) shipped with Ubuntu~14.04~\ac{LTS} will find it insufficient as it is written for a minor version (v2.2). This can lead to authorization problems displaying web pages and also renders the \ac{BOINC} feeder application in the \ac{CGI} directory path of the server application unusable. The standard feeder, a basic validator and a trivial scheduler already ship with every \ac{BOINC} project installation. The creation of work units cannot be easily generalized because there is no standard \ac{BOINC} facility for this process even though created work units can easily be passed to the scheduler using a supplied utility script.

To supply \acp{MCC} with a functional server back-end a new validator, a work unit generator tool-chain including at least a work unit generator and a set of scripts which schedule the work generation and the deployment to the scheduler would have to be implemented. As \ac{BOINC} applications normally do not schedule specific work units for specific users real scheduling would have to be done within the work unit generator tool-chain. The standard \ac{BOINC} scheduler would only be used to keep work units in the \ac{BOINC} shared memory until they are requested by feeder. Additionally the data base and web site initialization processes would have to be extended to also include geolocation data of \acp{GCC} and \acp{MCC} while providing a more streamlined user experience. A way to make this accessible to \acp{MCC} is to package a ready project server template as a deb package (software installation format used by Debian based Linux distributions like Ubuntu) resolving the most integral project dependencies automatically.

\subsection{Implementation of web application back-end}
An alternative to the \ac{BOINC} server application is the SatNOGS Network \cite{_satnogs/satnogs-network_????} application --- a part of the sub orbital tracking solution displayed at the most recent Hackaday \cite{solomos_system_2006} --- which is provided as an open source project under the \ac{MPL-2.0}.

The application is written in python using the Django web application framework and can be ported to virtually every server machine which can supply a python interpreter. The current version is still a work in progress but already offers the possibility to add \acp{GCC} to the global network. Within the application administrators have the possibility to add targets which should be tracked --- objects which orbit earth are tracked by \ac{NORAD} and can be identified by a \ac{NORAD} catalog number --- and added to the integrated scheduler which feeds work units to clients. Being an independent project using SatNOGS would allow different types of client applications to download work units and upload mission data but it also means that the \ac{BOINC} infrastructure can be used as it operates as a monolithic set of services.

\section{Ground Control Client}
Ground control client software has to be easily installed and automatable as the \ac{GCC} should work the whole day without human supervision. To ensure that operators of \acp{GCC} can take full control over their equipment automated services should also allow the manual handling of the \ac{GCC}. This is especially important as the legal situation in some countries does not allow automated radio communication for amateur operators (e.g. Austria \cite[section~3, paragraph~10, point~5]{_bundesgesetz_2013}). Apart from automation abilities the client should implement:

\begin{itemize}
	\item{Polling the mission control server for jobs}
	\item{Receiving work units}
	\item{Scheduling work units and manage radio and rotator hardware}
	\item{Processing mission data}
	\item{Uploading mission data to the mission control server}
\end{itemize}

The ground control client can be either implemented as a single distributed \ac{BOINC} application or as an individual implementation of a client software suite with exchangeable components. There are already projects in the open source community which incorporate a lot of functionality needed for satellite communication. The \ac{HamLib} \cite{_ham_????} offer an extensive amount of radio and rotator control interfaces and service utilities. The FLDIGI \cite{_fldigi_????} project --- a program supplying a software based modem --- supplies a software based modem to de- and encode radio signal while also offering an extensive set of interfaces. Gpredict \cite{_gpredict_????} is an application which allows users to track orbiters communicating elevation and azimuth values to the rotator control daemon (a \ac{HamLib} utility) while also transferring Doppler shift parameters --- alternation of radio frequencies due to high velocity movement of orbiting objects --- to the rig control daemon (a \ac{HamLib} utility controlling the radio). Independent from the way in which the client is finally implemented many parts of the aforementioned projects should be incorporated into the project because they have an extensive community and decrease the projects complexity immensely.

\subsection{Distributed \ac{BOINC} Application}
If the server back-end of the the network is implemented using the \ac{BOINC} infrastructure the client software is distributed automatically. Clients install the \ac{BOINC} manager --- an application distributed by the University of California, Berkeley --- and register themselves as participants at the \ac{BOINC} website of the \ac{MCC}. From there on the manager application starts communication with the mission control server and (if available)will download the client software and scheduled work units. The manager gives \ac{GCC} operators the opportunity to manually poll the server, pause the application, reduce or increase the available resources (e.g. \ac{CPU} time) and remove the project altogether.

From the client side this is arguably the most convenient way to participate. Developers working on the implementation would face the problem that they have to wrap every bit of functionality in a single application including work unit scheduling, de- and encoding of communication data and hardware control. Because of this and the fact that the client software distribution process only distinguishes clients of different operating system and processor architecture and not by \ac{GCC} hardware set-ups the client application itself would have to query the system extensively in order to initialize a tracking sequence.

\subsection{Client Software Suite}
If the network is not built on the \ac{BOINC} infrastructure the distribution of the client software used by \acp{GCC} has to be done manually by the operators. Developing an independent ground control client causes either more work for the developers (having to create packages for deployment) or for \ac{GCC} operators who would have to manually install and update the client components. As mentioned in chapter \ref{sect:strategy} the initial target system is planned to be a Linux distribution. The most Linux distributions ship with a package managers which is used to install, remove, update programs and resolve software dependencies. This can be used to conveniently deploy the client software to \acp{GCC} and keep it up do date.

The SatNOGS project already offers two different approaches to a client solution. Figure \ref{fig:stack} shows the integrated satellite tracking stack. The integrated stack consists of a number of services loosely bond together to supply operators with an automated tracking solution. The components are exchangeable as long as the \ac{API} conventions enforced by the management interface are not violated. Services can easily be replaced and tested separately and if needed low level components can be accessed via an additional compatibility layer. The solution displayed in figure \ref{fig:stack} depicts the hardware set-up presented at the 2014 Hackaday \cite{solomos_system_2006}. As mentioned in chapter \ref{sect:strategy} \acp{GCC} station hardware is usually stationary, thus extinguishing the need for a GPS module and the location retrieval service. As \ac{HamLib} offers a layer of hardware abstraction roatator and radio rig specifications can be be changed without major software adjustments as long \ac{HamLib} includes the right device drivers (also called back-ends).

\begin{figure}[ht]
\includegraphics[width=300px]{img_res/stack.png}
\caption{Architecture graph of of SatNOGS integrated stack. The network layer encompasses a \ac{TLE} provider and the back-end server both accessible by web-based \ac{API} calls. The SatNOGS ground station layer is divided into software and hardware abstraction elements. The central point for software based control flow is the Management Interface communicating with all other services over generic interfaces. The hardware layer is either accessed directly via a serial port or by using a layer of hardware abstraction like \ac{HamLib}. Figure taken from \url{https://satnogs.org/documentation.html}
\label{fig:stack}} 
\end{figure}

Gpredict \cite{_gpredict_????} is a commonly used program to manually track satellites. Apart from querying \ac{TLE} (simple data format to calculate orbits) providers, tracking orbiters and calling hardware interfaces it can also be modified to transmit data to the global network. Gpredict offers operators instantaneous tracking visualization and increased manual control over tracking operations while diminishing automation capabilities of the client set-up. Figure \ref{fig:stack_gpredict} shows how Gpredict fits into the client software architecture.

\begin{figure}[ht]
\includegraphics[width=300px]{img_res/stack_gpredict.png}
\caption{Architecture graph of SatNOGS Gpredict stack. The network layer encompasses the standard Gpredict \ac{TLE} provider and the back-end server both accessible by web-based \ac{API} calls. An adjusted version of Gpredict poses as the main operator accessing the hardware layer via abstraction services. Figure taken from \url{https://satnogs.org/documentation.html} 
\label{fig:stack_gpredict}} 
\end{figure}