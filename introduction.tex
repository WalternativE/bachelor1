\chapter{Introduction}
\section{Background and Motivation}
Since the late nineties pico and micro satellites have been used both in educational and commercial projects. Especially the rise of the CubeSat program made it possible for various parties to participate in this endeavor as respectively to conventional satellite operations the costs are low and there is an existing standardized work-flow. To make the launch cost effective and in order to use affordable \ac{COTS} components those missions mainly target a low earth orbit reaching an altitude of about 120 to 1200~km above sea level. In this heights the movement speed of an object is so high that it circles earth multiple times a day being in the line of sight of an observing \ac{MCC} about 15 minutes per pass. Depending on the orbit of a satellite valuable occurrences of such communication periods occur up to twelve times a day as stated in \cite{kief_genso_????}. This leaves only a short time-frame for up and down-link in which mission control has the opportunity to download mission data or send commands to the satellite. During the periods in which no direct line of sight can be established the station remains in an idle state and can only be used to track other spacecrafts. For the majority of the time mission data is not reachable and will be lost whenever the satellite is forced to overwrite its memory because of size restrictions. It is also not possible to send vital commands and updates to the orbiter which can lead to the failure of the mission.

Apart from \acp{MCC} whose main objective is to stay in contact with their own satellites there are also numerous hobbyists and educational facilities which operate radio stations which could establish up and down-link communication. For the most part individual \acp{GCC} manually track interesting projects and upload mission data own their own. This is a very valuable service for missions to increase total coverage but unfortunately does not prove to be especially reliable.

There have been different attempts to solve the problem of poor coverage and the fact that the return of investment of both the \acp{MCC} and \acp{GCC} is very small. With \ac{GENSO} --- a project sponsored by \ac{ESA} --- it was tried to establish a network of interconnected \acp{MCC} and \acp{GCC} which share their resources automatically and propagate data reliably. Overall it improved the situation but because of its closed monolithic structure it did not prove to be suitable for a majority of the missions.

In the following sections the most important topics regarding this thesis will be described briefly.

\section{CubeSat}
\begin{figure}[ht]
\centering
\includegraphics[width=300px]{img_res/AAUSAT3-FM.jpg}
\caption{AAUSAT3-FM CubeSat. Picture taken in the Student Satlab, Aalborg University, Denmark. Wikimedia Commons \ccLogo \url{http://creativecommons.org/licenses/by-sa/3.0/}  \label{AAUSAT3-FM}} 
\end{figure}
Figure \ref{AAUSAT3-FM} depicts the standard CubeSat set-up. A CubeSat as stated by the CubeSat standard in \cite{chin_cubesat:_2008} "[\dots] should be a 10~cm cube [having] a total mass of no more than 1~kg". The satellite itself very often consists of \ac{COTS} components which makes it cost effective to plan and construct. As a result of the use of these components --- especially considering the increasing range of different development boards for mobile electronics --- CubeSats are positioned in \ac{LEO} for a number of reasons summarized in \cite{preindl_optimization_2010}
\begin{itemize}
	\item Communication range: The low power budget of CubeSats limits the range in which they can communicate with \acp{GCC} and \acp{MCC}.
	\item Radiation: Objects orbiting the globe in \ac{LEO} do not risk to cross either the inner nor the outer Van Allen belt saving them from being exposed to critical radiation.
	\item Deployment costs: Deployments closer to the globe are reasonably cheaper.
	\item Space debris: CubeSats only have a limited live span until their electronics fail. Small objects in \ac{LEO} enter the atmosphere faster than satellites deployed above that altitude and no longer pose a threat to others. The differences in the decay of space debris regarding \ac{LEO} were observed in \cite{oltrogge_evaluation_2011}.
\end{itemize}
\section{\acl{GENSO}}
\label{sec:GENSO}
\ac{GENSO} describes the infrastructure behind a network of university and amateur ground stations collaborating to improve educational spacecraft communication. It was initiated by the \ac{ISEB} which consists of the education departments of the \ac{CSA}, the \ac{ESA}, the \ac{JAXA} and the \ac{NASA}. The main objectives of the project include the following as stated in \cite{leveque_global_2007}:
\begin{itemize}
	\item Near-global access to space crafts in \ac{LEO}
	\item Remote access for operators to mission data, even from ground stations other than the \ac{MCC}
	\item Down-link error-correction through redundant data streams
	\item A global standard for educational ground segment hardware and software as well as space-segment communications hardware
\end{itemize}
The \ac{GENSO} infrastructure was built to handle both active and passive satellite passes. Passive passes --- also called bookings --- only involve down-link communication from the satellite which could in theory broadcast constantly to the ground station which can receive the transmitted data. Active passes include the up-link of mission-data to the satellite and have to be specified by the \ac{MCC}. Establishing a down-link is non critical whereas an incorrect up-link can severely impair a running mission.
\begin{figure}[ht]
\centering
\includegraphics[width=200px]{img_res/genso_framework.jpg}
\caption{Architecture graph of GENSO depicting its place in the already existing satellite communication framework. Blue components stand for already existing participants and connections while red marks parts that are implemented by GENSO. Picture taken from \url{http://www.esa.int/Education/How_GENSO_works} 
\label{genso_framework}} 
\end{figure}

As seen in figure \ref{genso_framework} \ac{GENSO} implements the idea of a mission control client, a ground station server and an authentication server run by a \ac{GENSO} administrator. Both mission control clients and ground station servers authenticate at the central authentication server or one of its slaves (a server which poses as an identical copy of the main server) in order to schedule satellite traffic and propagate both active and passive mission data.

\section{\acl{BOINC}}
The \ac{BOINC} project originated at Berkeley University to assist researchers who found themselves in the need of additional computational power to process large amounts of data. \ac{BOINC} harnesses the fact that the majority of personal computers do not use their \acp{CPU} and \acp{GPU} to their full potential. Scientist can utilize this by hosting a \ac{BOINC} server which distributes both scientific applications and data sets to participating clients who only have to install a \ac{BOINC} client application and register as project participants. The distributed data is processed using spare user resources and transmitted back to the \ac{BOINC} server where it is validated and fed into the result set. As stated in \cite[chapter 1 section 1]{ries_boinc_2012} one of the most prominent early examples for this type of \ac{PRC} was Seti@Home which --- even though it has not yet achieved its goal of proving the existence of intelligent extra terrestrial life --- is still active today.

\section{Possible Medical Application}
Current CubeSat missions mainly focus on measuring space weather phenomena, observing the planet and improving low earth orbit communication techniques. Educational space programs become more and more affordable and the amount of well documented process descriptions increases regularly. This lowers the barrier for engineers working in other fields to plan missions to enable the use of this development. Bio-medical engineering offers a variety of possible mission candidates. Using small satellites to work as communication relays even the remotest regions on the planet could transmit and receive medical data improving medical services and connectivity both in developing countries and in hardly accessible research locations. Installing ground stations to establish radio contact to orbiters in \ac{LEO} is less expensive than constructing a new internet back-bone and they are relocatable.

To increase the possibilities for specialists to use miniature satellites for telemedicine purposes it is vital to increase the total satellite coverage to an extend where data transmitted by stations not connected to the internet can be transported quickly to communication hubs within a communication network. The more participants there are and with an increasing number of spacecrafts such a network could sustain steady contact to radio dependent stations.